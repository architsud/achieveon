(function() {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('interest', {
            parent: 'entity',
            url: '/interest?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Interests'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/interest/interests.html',
                    controller: 'InterestController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('interest-detail', {
            parent: 'entity',
            url: '/interest/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Interest'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/interest/interest-detail.html',
                    controller: 'InterestDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Interest', function($stateParams, Interest) {
                    return Interest.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'interest',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('interest-detail.edit', {
            parent: 'interest-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/interest/interest-dialog.html',
                    controller: 'InterestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Interest', function(Interest) {
                            return Interest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('interest.new', {
            parent: 'interest',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/interest/interest-dialog.html',
                    controller: 'InterestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('interest', null, { reload: 'interest' });
                }, function() {
                    $state.go('interest');
                });
            }]
        })
        .state('interest.edit', {
            parent: 'interest',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/interest/interest-dialog.html',
                    controller: 'InterestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Interest', function(Interest) {
                            return Interest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('interest', null, { reload: 'interest' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('interest.delete', {
            parent: 'interest',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/interest/interest-delete-dialog.html',
                    controller: 'InterestDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Interest', function(Interest) {
                            return Interest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('interest', null, { reload: 'interest' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
