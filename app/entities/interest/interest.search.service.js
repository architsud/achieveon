(function() {
    'use strict';

    angular
        .module('leApp')
        .factory('InterestSearch', InterestSearch);

    InterestSearch.$inject = ['$resource'];

    function InterestSearch($resource) {
        var resourceUrl =  'api/_search/interests/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
