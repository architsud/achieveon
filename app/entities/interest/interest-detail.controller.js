(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('InterestDetailController', InterestDetailController);

    InterestDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Interest'];

    function InterestDetailController($scope, $rootScope, $stateParams, previousState, entity, Interest) {
        var vm = this;

        vm.interest = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('leApp:interestUpdate', function(event, result) {
            vm.interest = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
