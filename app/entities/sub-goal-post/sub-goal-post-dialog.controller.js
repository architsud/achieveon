(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('SubGoalPostDialogController', SubGoalPostDialogController);

    SubGoalPostDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SubGoalPost', 'SubGoal', 'User', 'SubPostComment'];

    function SubGoalPostDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SubGoalPost, SubGoal, User, SubPostComment) {
        var vm = this;

        vm.subGoalPost = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.subgoals = SubGoal.query();
        vm.users = User.query();
        vm.subpostcomments = SubPostComment.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.subGoalPost.id !== null) {
                SubGoalPost.update(vm.subGoalPost, onSaveSuccess, onSaveError);
            } else {
                SubGoalPost.save(vm.subGoalPost, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('leApp:subGoalPostUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createTime = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
