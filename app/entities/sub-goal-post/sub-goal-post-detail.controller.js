(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('SubGoalPostDetailController', SubGoalPostDetailController);

    SubGoalPostDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SubGoalPost', 'SubGoal', 'User', 'SubPostComment'];

    function SubGoalPostDetailController($scope, $rootScope, $stateParams, previousState, entity, SubGoalPost, SubGoal, User, SubPostComment) {
        var vm = this;

        vm.subGoalPost = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('leApp:subGoalPostUpdate', function(event, result) {
            vm.subGoalPost = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
