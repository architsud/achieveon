(function() {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('sub-goal-post', {
            parent: 'entity',
            url: '/sub-goal-post?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'SubGoalPosts'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sub-goal-post/sub-goal-posts.html',
                    controller: 'SubGoalPostController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('sub-goal-post-detail', {
            parent: 'entity',
            url: '/sub-goal-post/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'SubGoalPost'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sub-goal-post/sub-goal-post-detail.html',
                    controller: 'SubGoalPostDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SubGoalPost', function($stateParams, SubGoalPost) {
                    return SubGoalPost.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'sub-goal-post',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('sub-goal-post-detail.edit', {
            parent: 'sub-goal-post-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-goal-post/sub-goal-post-dialog.html',
                    controller: 'SubGoalPostDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SubGoalPost', function(SubGoalPost) {
                            return SubGoalPost.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sub-goal-post.new', {
            parent: 'sub-goal-post',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-goal-post/sub-goal-post-dialog.html',
                    controller: 'SubGoalPostDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                title: null,
                                linkUrl: null,
                                imagePath: null,
                                videoPath: null,
                                createTime: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('sub-goal-post', null, { reload: 'sub-goal-post' });
                }, function() {
                    $state.go('sub-goal-post');
                });
            }]
        })
        .state('sub-goal-post.edit', {
            parent: 'sub-goal-post',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-goal-post/sub-goal-post-dialog.html',
                    controller: 'SubGoalPostDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SubGoalPost', function(SubGoalPost) {
                            return SubGoalPost.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sub-goal-post', null, { reload: 'sub-goal-post' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sub-goal-post.delete', {
            parent: 'sub-goal-post',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-goal-post/sub-goal-post-delete-dialog.html',
                    controller: 'SubGoalPostDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SubGoalPost', function(SubGoalPost) {
                            return SubGoalPost.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sub-goal-post', null, { reload: 'sub-goal-post' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
