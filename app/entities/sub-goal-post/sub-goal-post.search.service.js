(function() {
    'use strict';

    angular
        .module('leApp')
        .factory('SubGoalPostSearch', SubGoalPostSearch);

    SubGoalPostSearch.$inject = ['$resource'];

    function SubGoalPostSearch($resource) {
        var resourceUrl =  'api/_search/sub-goal-posts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
