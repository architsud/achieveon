(function() {
    'use strict';
    angular
        .module('leApp')
        .factory('SubGoalPost', SubGoalPost);

    SubGoalPost.$inject = ['$resource', 'DateUtils'];

    function SubGoalPost ($resource, DateUtils) {
        var resourceUrl =  'api/sub-goal-posts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createTime = DateUtils.convertLocalDateFromServer(data.createTime);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.createTime = DateUtils.convertLocalDateToServer(copy.createTime);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.createTime = DateUtils.convertLocalDateToServer(copy.createTime);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
