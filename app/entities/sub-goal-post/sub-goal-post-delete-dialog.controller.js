(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('SubGoalPostDeleteController',SubGoalPostDeleteController);

    SubGoalPostDeleteController.$inject = ['$uibModalInstance', 'entity', 'SubGoalPost'];

    function SubGoalPostDeleteController($uibModalInstance, entity, SubGoalPost) {
        var vm = this;

        vm.subGoalPost = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SubGoalPost.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
