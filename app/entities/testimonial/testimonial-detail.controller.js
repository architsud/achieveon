(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('TestimonialDetailController', TestimonialDetailController);

    TestimonialDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Testimonial', 'User'];

    function TestimonialDetailController($scope, $rootScope, $stateParams, previousState, entity, Testimonial, User) {
        var vm = this;

        vm.testimonial = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('leApp:testimonialUpdate', function(event, result) {
            vm.testimonial = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
