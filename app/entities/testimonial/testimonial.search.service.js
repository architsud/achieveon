(function() {
    'use strict';

    angular
        .module('leApp')
        .factory('TestimonialSearch', TestimonialSearch);

    TestimonialSearch.$inject = ['$resource'];

    function TestimonialSearch($resource) {
        var resourceUrl =  'api/_search/testimonials/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
