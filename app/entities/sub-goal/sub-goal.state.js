(function() {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('sub-goal', {
            parent: 'entity',
            url: '/sub-goal',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'SubGoals'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sub-goal/sub-goals.html',
                    controller: 'SubGoalController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('sub-goal-detail', {
            parent: 'entity',
            url: '/sub-goal/:id?maingoalId&dimensionId',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'SubGoal'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sub-goal/sub-goal-detail.html',
                    controller: 'SubGoalDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SubGoal', function($stateParams, SubGoal) {
                    return SubGoal.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'sub-goal',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('sub-goal-detail.edit', {
            parent: 'sub-goal-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-goal/sub-goal-dialog.html',
                    controller: 'SubGoalDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SubGoal', function(SubGoal) {
                            return SubGoal.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sub-goal.new', {
            parent: 'sub-goal',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-goal/sub-goal-dialog.html',
                    controller: 'SubGoalDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                subgoalText: null,
                                status: null,
                                metricAmount: null,
                                metricTarget: null,
                                metricType: null,
                                metricSubType: null,
                                weightage: null,
                                creatTime: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('sub-goal', null, { reload: 'sub-goal' });
                }, function() {
                    $state.go('sub-goal');
                });
            }]
        })
        .state('sub-goal.edit', {
            parent: 'sub-goal',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-goal/sub-goal-dialog.html',
                    controller: 'SubGoalDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SubGoal', function(SubGoal) {
                            return SubGoal.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sub-goal', null, { reload: 'sub-goal' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sub-goal.delete', {
            parent: 'sub-goal',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-goal/sub-goal-delete-dialog.html',
                    controller: 'SubGoalDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SubGoal', function(SubGoal) {
                            return SubGoal.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sub-goal', null, { reload: 'sub-goal' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
