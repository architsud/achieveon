(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('SubGoalDetailController', SubGoalDetailController);

    SubGoalDetailController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', 'previousState', 'entity', 'SubGoal', 'Goal', '$location', '$uibModal', 'SubGoalPost', 'Principal', 'SubGoalPostsById', 'SubPostComment'];

    function SubGoalDetailController($scope, $rootScope, $state, $stateParams, previousState, entity, SubGoal, Goal, $location, $uibModal, SubGoalPost, Principal, SubGoalPostsById, SubPostComment) {
        var vm = this;
        vm.savePost = savePost;
        vm.saveComment = saveComment;
        vm.goal = entity;
        vm.goalId = vm.goal.id;
        vm.maingoalId = $stateParams.maingoalId;
        vm.dimensionId = $stateParams.dimensionId;
        vm.goal.startTime = new Date(vm.goal.startTime);
        vm.goal.finishTime = new Date(vm.goal.finishTime);
        vm.deleteGoal = deleteGoal;
        vm.loadSubGoal = loadSubGoal;
        Principal.identity().then(function (u) {
            vm.loginUser = u;
        });
        vm.post = {};
        vm.comment = {};
        function loadSubGoal(result) {
            // SubGoal.query({ id: entity.id }, function (result) {
            //     vm.goal = result;
            // }).$promise;
            //  vm.goal = SubGoal.get({ id: vm.goalId }).$promise;
            vm.goal = result;
            vm.goal.startTime = new Date(vm.goal.startTime);
            vm.goal.finishTime = new Date(vm.goal.finishTime);
        }
        vm.getPosts = getPosts;
        getPosts();
        function getPosts() {
            SubGoalPostsById.query({ id: vm.goalId }, function (result) {
                vm.posts = result;
            }).$promise;
        }
        vm.previousStateFun = previousStateFun;
        vm.previousState = previousState;
        vm.editSubGoalFun = editSubGoalFun;
        vm.modalInstance = "";
        function previousStateFun() {
            // alert(JSON.stringify(vm.previousState))
            $state.go(vm.previousState.name, { id: vm.previousState.params.id })
        }

        function deleteGoal(goalId) {
            vm.dimensionName = vm.goal.dimensionName;
            vm.goal.goalText = vm.goal.subgoalText;
            // vm.dimensionId = vm.dimension.id;
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/goal/goal-delete-dialog.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModalInstance', DeleteController]
            });
        }
        function DeleteController($scope, $uibModalInstance) {
            vm.clear = clear;
            vm.confirmDelete = confirmDelete;

            function clear() {
                $uibModalInstance.dismiss('cancel');
            }
            function confirmDelete(id) {
                // alert(id)
                SubGoal.delete({ id: id },
                    function () {
                        $uibModalInstance.close(true);
                        $state.go('goal-detail', { id: vm.maingoalId })
                        //$state.go('goal')
                        // $state.go(vm.previousState.name, { id: vm.previousState.params.id })
                    });
            }
        }

        //Open popup modal to edit sub goal
        function editSubGoalFun() {
            vm.subGoal = angular.copy(vm.goal);
            vm.subGoal.metricAmount = parseInt(vm.subGoal.metricAmount);
            vm.changeMetricSubType = changeMetricSubType;
            vm.subMetricValues = [];
            vm.metricTypes = [
                {
                    "name": "Currency",
                    "values": ["USD", "Euro"]
                },
                {
                    "name": "Numeric",
                    "values": [""]
                },
                {
                    "name": "Status",
                    "values": ["% Complete"]
                }
            ];

            // vm.subGoalMetricType = vm.metricTypes[0];
            // vm.subGoalMetricSubType = vm.subGoalMetricType.values[0];
            // vm.subGoalMetricSubTypeOther = '';
            // if (vm.subGoal) {
            //     vm.subGoalMetricType.name = vm.subGoal.metricType;
            //     vm.subGoalMetricSubType = vm.subGoal.metricSubType;
            // }

            function changeMetricSubType() {
                for (var i = 0; i < vm.metricTypes.length; i++) {
                    if (vm.metricTypes[i].name == vm.subGoal.metricType) {
                        vm.subMetricValues = vm.metricTypes[i].values;
                        if (vm.metricTypes[i].name != "Numeric") {
                            vm.subGoal.metricSubType = vm.metricTypes[i].values[0]
                        }
                        if (vm.metricTypes[i].name == "Status") {
                            vm.subGoal.metricTarget = 100
                        }

                    }
                }

            }

            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/goal/goal-sub-goals.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', SubGoalInstanceController]
            });
        }
        function SubGoalInstanceController($scope, $uibModalInstance) {
            vm.datePickerOpenStatus = {};
            vm.openCalendar = openCalendar;
            vm.showLabel = 'yes';
            vm.editSubGoal = editSubGoal;
            $scope.cancel = function (subGoal) {
                vm.modalInstance.close();
            };
            vm.close = function (subGoal) {
                vm.modalInstance.close();
            };
            $scope.save = function () {
                $uibModalInstance.close($scope.theThingIWantToSave);
            };

            //Update sub goal with new data
            function editSubGoal(subGoal) {
                // subGoal.metricType = vm.subGoalMetricType.name;
                // subGoal.metricSubType = vm.subGoalMetricSubType;
                SubGoal.update(subGoal, onSaveSuccess, onSaveError);
                vm.modalInstance.close();
            }
            function onSaveSuccess(result) {
                vm.loadSubGoal(result);
                vm.isSaving = false;
            }

            function onSaveError() {
                vm.isSaving = false;
            }
            function openCalendar(date) {
                vm.datePickerOpenStatus[date] = true;
            }
        }
        vm.mainGoal = (vm.goal.metricAmount / vm.goal.metricTarget) * 100 || 0;
        vm.metricStatus = 'no';
        vm.saveMetric = saveMetric;
        function saveMetric() {
            SubGoal.update(vm.goal, onMetricSaveSuccess, onMetricSaveError);
        }
        function onMetricSaveSuccess(result) {
            //  $scope.$emit('leApp:goalUpdate', result);
            vm.isSaving = false;
            //vm.getGoalDetails(result)
        }
        function onMetricSaveError() {
            vm.isSaving = false;
        }
        $scope.$watchCollection('[vm.goal.metricTarget, vm.goal.metricAmount]', function (newVal, oldVal) {
            if (newVal) {
                if (vm.goal.metricTarget && vm.goal.metricAmount > vm.goal.metricTarget) {
                    // vm.goal.metricTarget = vm.goal.metricAmount;
                    vm.goal.metricAmount = 0;
                } else {
                    vm.mainGoal = parseInt((vm.goal.metricAmount / vm.goal.metricTarget) * 100) || 0;
                    if (vm.mainGoal) {
                        if (vm.metricStatus == 'yes') {
                            vm.metricStatus = 'no'
                        } else {
                            vm.metricStatus = 'yes'
                        }
                    }
                    else {
                        if (vm.metricStatus == 'yes') {
                            vm.metricStatus = 'no'
                        } else {
                            vm.metricStatus = 'yes'
                        }
                    }
                }
            }
        });
        var unsubscribe = $rootScope.$on('leApp:subGoalUpdate', function (event, result) {
            vm.goal = result;
        });
        $scope.$on('$destroy', unsubscribe);
        function savePost(type) {
            vm.isSaving = true;
            if (type == 'update') {
                SubGoalPost.update(vm.post, onPostSaveSuccess, onPostSaveError);
            } else {
                vm.post.creatTime = new Date();
                vm.post.subGoalId = vm.goal.id;
                vm.post.userId = vm.loginUser.id;
                // alert("save: " + JSON.stringify(vm.post))
                SubGoalPost.save(vm.post, onPostSaveSuccess, onPostSaveError);
            }
        }
        function onPostSaveSuccess(result) {
            vm.post = {};
            getPosts();
        }

        function onPostSaveError() {
            //vm.isSaving = false;
        }
        function saveComment(type, postId, comment) {
            if (type == 'update') {
                if (vm.comment.comment !== null) {
                    vm.comment.id = comment.id;
                    vm.comment.creatTime = new Date();
                    vm.comment.comment = comment.comment;
                    vm.comment.subGoalPostId = postId;
                    vm.comment.userId = vm.loginUser.id;
                    // alert("save: " + JSON.stringify(vm.comment))
                    SubPostComment.update(vm.comment, onCommentSaveSuccess, onCommentSaveError);
                }
            } else {
                if (vm.comment.comment !== null) {
                    // vm.comment.creatTime = new Date();
                    vm.comment.comment = comment;
                    vm.comment.subGoalPostId = postId;
                    vm.comment.userId = vm.loginUser.id;
                    // alert("save: " + JSON.stringify(vm.comment))
                    SubPostComment.save(vm.comment, onCommentSaveSuccess, onCommentSaveError);
                }
            }
        }
        function onCommentSaveSuccess(result) {
            vm.isSaving = false;
            vm.comment = {};
            getPosts();
        }

        function onCommentSaveError() {
            vm.isSaving = false;
        }
        vm.deleteComment = deleteComment;
        function deleteComment(id) {
            SubPostComment.delete({ id: id },
                function () {
                    //$uibModalInstance.close(true);
                    getPosts();
                });
        }
    }
})();
