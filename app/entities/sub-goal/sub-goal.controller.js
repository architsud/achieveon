(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('SubGoalController', SubGoalController);

    SubGoalController.$inject = ['$scope', '$state', 'SubGoal', 'SubGoalSearch'];

    function SubGoalController ($scope, $state, SubGoal, SubGoalSearch) {
        var vm = this;
        
        vm.subGoals = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            SubGoal.query(function(result) {
                vm.subGoals = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            SubGoalSearch.query({query: vm.searchQuery}, function(result) {
                vm.subGoals = result;
            });
        }    }
})();
