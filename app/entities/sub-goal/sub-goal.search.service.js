(function() {
    'use strict';

    angular
        .module('leApp')
        .factory('SubGoalSearch', SubGoalSearch);

    SubGoalSearch.$inject = ['$resource'];

    function SubGoalSearch($resource) {
        var resourceUrl =  'api/_search/sub-goals/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
