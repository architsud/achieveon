(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('SubGoalDialogController', SubGoalDialogController);

    SubGoalDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SubGoal', 'Goal'];

    function SubGoalDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SubGoal, Goal) {
        var vm = this;

        vm.subGoal = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.goals = Goal.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.subGoal.id !== null) {
                SubGoal.update(vm.subGoal, onSaveSuccess, onSaveError);
            } else {
                SubGoal.save(vm.subGoal, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('leApp:subGoalUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creatTime = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
