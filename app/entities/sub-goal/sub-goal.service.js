(function() {
    'use strict';
    angular
        .module('leApp')
        .factory('SubGoal', SubGoal)
        .factory('SubGoalPostsById', SubGoalPostsById)
    SubGoal.$inject = ['$resource', 'DateUtils'];
    SubGoalPostsById.$inject = ['$resource', 'DateUtils'];
    function SubGoal ($resource, DateUtils) {
        var resourceUrl =  'api/sub-goals/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.creatTime = DateUtils.convertLocalDateToServer(copy.creatTime);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.creatTime = DateUtils.convertLocalDateToServer(copy.creatTime);
                    return angular.toJson(copy);
                }
            }
        });
    }
    function SubGoalPostsById($resource, DateUtils) { 
        var resourceUrl = '/api/postsbysubgoal/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                    }
                    return data;
                }
            }
        });
    }
})();
