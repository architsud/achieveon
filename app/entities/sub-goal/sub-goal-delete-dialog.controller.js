(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('SubGoalDeleteController',SubGoalDeleteController);

    SubGoalDeleteController.$inject = ['$uibModalInstance', 'entity', 'SubGoal'];

    function SubGoalDeleteController($uibModalInstance, entity, SubGoal) {
        var vm = this;

        vm.subGoal = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SubGoal.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
