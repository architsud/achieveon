(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserNetworkDetailController', UserNetworkDetailController);

    UserNetworkDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserNetwork', 'User'];

    function UserNetworkDetailController($scope, $rootScope, $stateParams, previousState, entity, UserNetwork, User) {
        var vm = this;

        vm.userNetwork = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('leApp:userNetworkUpdate', function(event, result) {
            vm.userNetwork = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
