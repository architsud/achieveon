(function() {
    'use strict';

    angular
        .module('leApp')
        .factory('UserNetworkSearch', UserNetworkSearch);

    UserNetworkSearch.$inject = ['$resource'];

    function UserNetworkSearch($resource) {
        var resourceUrl =  'api/_search/user-networks/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
