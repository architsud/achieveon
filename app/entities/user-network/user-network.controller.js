(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserNetworkController', UserNetworkController);

    UserNetworkController.$inject = ['$scope', '$state', 'UserNetwork', 'UserNetworkSearch'];

    function UserNetworkController ($scope, $state, UserNetwork, UserNetworkSearch) {
        var vm = this;
        
        vm.userNetworks = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            UserNetwork.query(function(result) {
                vm.userNetworks = result;
               // alert(JSON.stringify(vm.userNetworks))
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            UserNetworkSearch.query({query: vm.searchQuery}, function(result) {
                vm.userNetworks = result;
            });
        }    }
})();
