(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserNetworkDialogController', UserNetworkDialogController);

    UserNetworkDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'UserNetwork', 'User'];

    function UserNetworkDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, UserNetwork, User) {
        var vm = this;

        vm.userNetwork = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userNetwork.id !== null) {
                UserNetwork.update(vm.userNetwork, onSaveSuccess, onSaveError);
            } else {
                UserNetwork.save(vm.userNetwork, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('leApp:userNetworkUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
