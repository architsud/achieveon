(function () {
    'use strict';
    angular
        .module('leApp')
        .factory('UserNetwork', UserNetwork)
        .factory('UserNetworkStatus', UserNetworkStatus);

    UserNetwork.$inject = ['$resource'];

    function UserNetwork($resource) {
        var resourceUrl = 'api/user-networks/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();
UserNetworkStatus.$inject = ['$resource', 'DateUtils'];

function UserNetworkStatus($resource) {
    var resourceUrl = 'api/user-networks-user';

    return $resource(resourceUrl, {}, {
        'query': { method: 'POST', isArray: true },
        'post': {
            method: 'POST',
            transformResponse: function (data) {
                if (data) {
                    data = angular.fromJson(data);
                }
                return data;
            }
        },

    });
}
