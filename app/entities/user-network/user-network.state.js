(function() {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-network', {
            parent: 'entity',
            url: '/user-network',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'UserNetworks'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-network/user-networks.html',
                    controller: 'UserNetworkController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('user-network-detail', {
            parent: 'entity',
            url: '/user-network/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'UserNetwork'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-network/user-network-detail.html',
                    controller: 'UserNetworkDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'UserNetwork', function($stateParams, UserNetwork) {
                    return UserNetwork.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-network',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-network-detail.edit', {
            parent: 'user-network-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-network/user-network-dialog.html',
                    controller: 'UserNetworkDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserNetwork', function(UserNetwork) {
                            return UserNetwork.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-network.new', {
            parent: 'user-network',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-network/user-network-dialog.html',
                    controller: 'UserNetworkDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                networkRelation: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-network', null, { reload: 'user-network' });
                }, function() {
                    $state.go('user-network');
                });
            }]
        })
        .state('user-network.edit', {
            parent: 'user-network',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-network/user-network-dialog.html',
                    controller: 'UserNetworkDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserNetwork', function(UserNetwork) {
                            return UserNetwork.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-network', null, { reload: 'user-network' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-network.delete', {
            parent: 'user-network',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-network/user-network-delete-dialog.html',
                    controller: 'UserNetworkDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserNetwork', function(UserNetwork) {
                            return UserNetwork.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-network', null, { reload: 'user-network' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
