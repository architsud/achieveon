(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserNetworkDeleteController',UserNetworkDeleteController);

    UserNetworkDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserNetwork'];

    function UserNetworkDeleteController($uibModalInstance, entity, UserNetwork) {
        var vm = this;

        vm.userNetwork = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserNetwork.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
