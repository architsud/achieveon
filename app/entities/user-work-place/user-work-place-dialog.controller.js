(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserWorkPlaceDialogController', UserWorkPlaceDialogController);

    UserWorkPlaceDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserWorkPlace'];

    function UserWorkPlaceDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, UserWorkPlace) {
        var vm = this;

        vm.userWorkPlace = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userWorkPlace.id !== null) {
                UserWorkPlace.update(vm.userWorkPlace, onSaveSuccess, onSaveError);
            } else {
                UserWorkPlace.save(vm.userWorkPlace, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('leApp:userWorkPlaceUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.fromDate = false;
        vm.datePickerOpenStatus.endDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
