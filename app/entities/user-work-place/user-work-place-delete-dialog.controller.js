(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserWorkPlaceDeleteController',UserWorkPlaceDeleteController);

    UserWorkPlaceDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserWorkPlace'];

    function UserWorkPlaceDeleteController($uibModalInstance, entity, UserWorkPlace) {
        var vm = this;

        vm.userWorkPlace = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserWorkPlace.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
