(function() {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-work-place', {
            parent: 'entity',
            url: '/user-work-place?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'UserWorkPlaces'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-work-place/user-work-places.html',
                    controller: 'UserWorkPlaceController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('user-work-place-detail', {
            parent: 'entity',
            url: '/user-work-place/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'UserWorkPlace'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-work-place/user-work-place-detail.html',
                    controller: 'UserWorkPlaceDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'UserWorkPlace', function($stateParams, UserWorkPlace) {
                    return UserWorkPlace.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-work-place',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-work-place-detail.edit', {
            parent: 'user-work-place-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-work-place/user-work-place-dialog.html',
                    controller: 'UserWorkPlaceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserWorkPlace', function(UserWorkPlace) {
                            return UserWorkPlace.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-work-place.new', {
            parent: 'user-work-place',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-work-place/user-work-place-dialog.html',
                    controller: 'UserWorkPlaceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                companyName: null,
                                title: null,
                                role: null,
                                fromDate: null,
                                endDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-work-place', null, { reload: 'user-work-place' });
                }, function() {
                    $state.go('user-work-place');
                });
            }]
        })
        .state('user-work-place.edit', {
            parent: 'user-work-place',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-work-place/user-work-place-dialog.html',
                    controller: 'UserWorkPlaceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserWorkPlace', function(UserWorkPlace) {
                            return UserWorkPlace.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-work-place', null, { reload: 'user-work-place' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-work-place.delete', {
            parent: 'user-work-place',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-work-place/user-work-place-delete-dialog.html',
                    controller: 'UserWorkPlaceDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserWorkPlace', function(UserWorkPlace) {
                            return UserWorkPlace.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-work-place', null, { reload: 'user-work-place' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
