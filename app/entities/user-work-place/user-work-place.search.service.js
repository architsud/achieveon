(function() {
    'use strict';

    angular
        .module('leApp')
        .factory('UserWorkPlaceSearch', UserWorkPlaceSearch);

    UserWorkPlaceSearch.$inject = ['$resource'];

    function UserWorkPlaceSearch($resource) {
        var resourceUrl =  'api/_search/user-work-places/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
