(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserWorkPlaceDetailController', UserWorkPlaceDetailController);

    UserWorkPlaceDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserWorkPlace'];

    function UserWorkPlaceDetailController($scope, $rootScope, $stateParams, previousState, entity, UserWorkPlace) {
        var vm = this;

        vm.userWorkPlace = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('leApp:userWorkPlaceUpdate', function(event, result) {
            vm.userWorkPlace = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
