(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('SubCommentDetailController', SubCommentDetailController);

    SubCommentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SubComment', 'Comment', 'User'];

    function SubCommentDetailController($scope, $rootScope, $stateParams, previousState, entity, SubComment, Comment, User) {
        var vm = this;

        vm.subComment = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('leApp:subCommentUpdate', function(event, result) {
            vm.subComment = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
