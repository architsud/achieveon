(function() {
    'use strict';

    angular
        .module('leApp')
        .factory('SubCommentSearch', SubCommentSearch);

    SubCommentSearch.$inject = ['$resource'];

    function SubCommentSearch($resource) {
        var resourceUrl =  'api/_search/sub-comments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
