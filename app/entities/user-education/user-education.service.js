(function() {
    'use strict';
    angular
        .module('leApp')
        .factory('UserEducation', UserEducation);

    UserEducation.$inject = ['$resource', 'DateUtils'];

    function UserEducation ($resource, DateUtils) {
        var resourceUrl =  'api/user-educations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.completion = DateUtils.convertLocalDateFromServer(data.completion);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.completion = DateUtils.convertLocalDateToServer(copy.completion);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.completion = DateUtils.convertLocalDateToServer(copy.completion);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
