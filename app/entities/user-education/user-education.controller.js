(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserEducationController', UserEducationController);

    UserEducationController.$inject = ['$scope', '$state', 'UserEducation', 'UserEducationSearch'];

    function UserEducationController ($scope, $state, UserEducation, UserEducationSearch) {
        var vm = this;
        
        vm.userEducations = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            UserEducation.query(function(result) {
                vm.userEducations = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            UserEducationSearch.query({query: vm.searchQuery}, function(result) {
                vm.userEducations = result;
            });
        }    }
})();
