(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserEducationDetailController', UserEducationDetailController);

    UserEducationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserEducation', 'User'];

    function UserEducationDetailController($scope, $rootScope, $stateParams, previousState, entity, UserEducation, User) {
        var vm = this;

        vm.userEducation = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('leApp:userEducationUpdate', function(event, result) {
            vm.userEducation = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
