(function() {
    'use strict';

    angular
        .module('leApp')
        .factory('UserEducationSearch', UserEducationSearch);

    UserEducationSearch.$inject = ['$resource'];

    function UserEducationSearch($resource) {
        var resourceUrl =  'api/_search/user-educations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
