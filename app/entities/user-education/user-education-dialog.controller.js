(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserEducationDialogController', UserEducationDialogController);

    UserEducationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserEducation', 'User'];

    function UserEducationDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, UserEducation, User) {
        var vm = this;

        vm.userEducation = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userEducation.id !== null) {
                UserEducation.update(vm.userEducation, onSaveSuccess, onSaveError);
            } else {
                UserEducation.save(vm.userEducation, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('leApp:userEducationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.completion = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
