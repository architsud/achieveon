(function() {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-education', {
            parent: 'entity',
            url: '/user-education',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'UserEducations'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-education/user-educations.html',
                    controller: 'UserEducationController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('user-education-detail', {
            parent: 'entity',
            url: '/user-education/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'UserEducation'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-education/user-education-detail.html',
                    controller: 'UserEducationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'UserEducation', function($stateParams, UserEducation) {
                    return UserEducation.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-education',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-education-detail.edit', {
            parent: 'user-education-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-education/user-education-dialog.html',
                    controller: 'UserEducationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserEducation', function(UserEducation) {
                            return UserEducation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-education.new', {
            parent: 'user-education',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-education/user-education-dialog.html',
                    controller: 'UserEducationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                institution: null,
                                name: null,
                                degree: null,
                                completion: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-education', null, { reload: 'user-education' });
                }, function() {
                    $state.go('user-education');
                });
            }]
        })
        .state('user-education.edit', {
            parent: 'user-education',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-education/user-education-dialog.html',
                    controller: 'UserEducationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserEducation', function(UserEducation) {
                            return UserEducation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-education', null, { reload: 'user-education' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-education.delete', {
            parent: 'user-education',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-education/user-education-delete-dialog.html',
                    controller: 'UserEducationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserEducation', function(UserEducation) {
                            return UserEducation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-education', null, { reload: 'user-education' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
