(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserEducationDeleteController',UserEducationDeleteController);

    UserEducationDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserEducation'];

    function UserEducationDeleteController($uibModalInstance, entity, UserEducation) {
        var vm = this;

        vm.userEducation = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserEducation.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
