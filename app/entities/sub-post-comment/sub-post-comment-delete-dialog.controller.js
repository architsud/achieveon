(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('SubPostCommentDeleteController',SubPostCommentDeleteController);

    SubPostCommentDeleteController.$inject = ['$uibModalInstance', 'entity', 'SubPostComment'];

    function SubPostCommentDeleteController($uibModalInstance, entity, SubPostComment) {
        var vm = this;

        vm.subPostComment = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SubPostComment.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
