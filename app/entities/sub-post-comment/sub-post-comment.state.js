(function() {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('sub-post-comment', {
            parent: 'entity',
            url: '/sub-post-comment?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'SubPostComments'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sub-post-comment/sub-post-comments.html',
                    controller: 'SubPostCommentController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('sub-post-comment-detail', {
            parent: 'entity',
            url: '/sub-post-comment/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'SubPostComment'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sub-post-comment/sub-post-comment-detail.html',
                    controller: 'SubPostCommentDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SubPostComment', function($stateParams, SubPostComment) {
                    return SubPostComment.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'sub-post-comment',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('sub-post-comment-detail.edit', {
            parent: 'sub-post-comment-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-post-comment/sub-post-comment-dialog.html',
                    controller: 'SubPostCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SubPostComment', function(SubPostComment) {
                            return SubPostComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sub-post-comment.new', {
            parent: 'sub-post-comment',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-post-comment/sub-post-comment-dialog.html',
                    controller: 'SubPostCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                comment: null,
                                linkUrl: null,
                                imagePath: null,
                                videoPath: null,
                                creatTime: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('sub-post-comment', null, { reload: 'sub-post-comment' });
                }, function() {
                    $state.go('sub-post-comment');
                });
            }]
        })
        .state('sub-post-comment.edit', {
            parent: 'sub-post-comment',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-post-comment/sub-post-comment-dialog.html',
                    controller: 'SubPostCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SubPostComment', function(SubPostComment) {
                            return SubPostComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sub-post-comment', null, { reload: 'sub-post-comment' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sub-post-comment.delete', {
            parent: 'sub-post-comment',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-post-comment/sub-post-comment-delete-dialog.html',
                    controller: 'SubPostCommentDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SubPostComment', function(SubPostComment) {
                            return SubPostComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sub-post-comment', null, { reload: 'sub-post-comment' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
