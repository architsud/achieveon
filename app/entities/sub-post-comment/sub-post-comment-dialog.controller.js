(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('SubPostCommentDialogController', SubPostCommentDialogController);

    SubPostCommentDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SubPostComment', 'User', 'SubGoalPost'];

    function SubPostCommentDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SubPostComment, User, SubGoalPost) {
        var vm = this;

        vm.subPostComment = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();
        vm.subgoalposts = SubGoalPost.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.subPostComment.id !== null) {
                SubPostComment.update(vm.subPostComment, onSaveSuccess, onSaveError);
            } else {
                SubPostComment.save(vm.subPostComment, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('leApp:subPostCommentUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creatTime = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
