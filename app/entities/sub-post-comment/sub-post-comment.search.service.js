(function() {
    'use strict';

    angular
        .module('leApp')
        .factory('SubPostCommentSearch', SubPostCommentSearch);

    SubPostCommentSearch.$inject = ['$resource'];

    function SubPostCommentSearch($resource) {
        var resourceUrl =  'api/_search/sub-post-comments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
