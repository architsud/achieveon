(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('SubPostCommentDetailController', SubPostCommentDetailController);

    SubPostCommentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SubPostComment', 'User', 'SubGoalPost'];

    function SubPostCommentDetailController($scope, $rootScope, $stateParams, previousState, entity, SubPostComment, User, SubGoalPost) {
        var vm = this;

        vm.subPostComment = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('leApp:subPostCommentUpdate', function(event, result) {
            vm.subPostComment = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
