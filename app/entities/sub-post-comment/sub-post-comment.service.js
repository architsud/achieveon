(function() {
    'use strict';
    angular
        .module('leApp')
        .factory('SubPostComment', SubPostComment);

    SubPostComment.$inject = ['$resource', 'DateUtils'];

    function SubPostComment ($resource, DateUtils) {
        var resourceUrl =  'api/sub-post-comments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.creatTime = DateUtils.convertLocalDateToServer(copy.creatTime);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.creatTime = DateUtils.convertLocalDateToServer(copy.creatTime);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
