(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserCollaborateController', UserCollaborateController);

    UserCollaborateController.$inject = ['$scope', '$state', 'UserCollaborate', 'UserCollaborateSearch'];

    function UserCollaborateController ($scope, $state, UserCollaborate, UserCollaborateSearch) {
        var vm = this;
        
        vm.userCollaborates = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            UserCollaborate.query(function(result) {
                vm.userCollaborates = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            UserCollaborateSearch.query({query: vm.searchQuery}, function(result) {
                vm.userCollaborates = result;
            });
        }    }
})();
