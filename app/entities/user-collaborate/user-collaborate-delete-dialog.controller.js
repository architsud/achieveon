(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserCollaborateDeleteController',UserCollaborateDeleteController);

    UserCollaborateDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserCollaborate'];

    function UserCollaborateDeleteController($uibModalInstance, entity, UserCollaborate) {
        var vm = this;

        vm.userCollaborate = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserCollaborate.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
