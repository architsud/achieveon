(function () {
    'use strict';
    angular
        .module('leApp')
        .factory('UserCollaborate', UserCollaborate)
        .factory('DeleteCollaboraterOrSuppoter', DeleteCollaboraterOrSuppoter)
        .factory('RequestCollaboraterOrSuppoter', RequestCollaboraterOrSuppoter)
        .factory('UnCollaboraterOrSuppoter',UnCollaboraterOrSuppoter)
    UserCollaborate.$inject = ['$resource'];

    function UserCollaborate($resource) {
        var resourceUrl = 'api/user-collaborates/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();
DeleteCollaboraterOrSuppoter.$inject = ['$resource', 'DateUtils'];
function DeleteCollaboraterOrSuppoter($resource, DateUtils) {
    var resourceUrl = '/api/user-collaborates/:id';
    return $resource(resourceUrl, {}, {
        'query': { method: 'DELETE', isArray: true },
        'delete': {
            method: 'DELETE',
            transformResponse: function (data) {

                if (data) {
                    data = angular.fromJson(data);
                }
                return data;
            }
        }
    });
}
RequestCollaboraterOrSuppoter.$inject = ['$resource', 'DateUtils'];
function RequestCollaboraterOrSuppoter($resource, DateUtils) {
    var resourceUrl = '/api/request-supporter-collaborator/:id';
    return $resource(resourceUrl, {}, {
        'query': { method: 'POST', isArray: false },
        'post': {
            method: 'POST',
            transformResponse: function (data) {
                if (data) {
                    data = angular.fromJson(data);
                    // data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                }
                console.log(" RequestCollaboraterOrSuppoter Serv: "+data)
                return data;
            }
        }
    });
}

UnCollaboraterOrSuppoter.$inject = ['$resource', 'DateUtils'];
function UnCollaboraterOrSuppoter($resource, DateUtils) {
    var resourceUrl = '/api/unsupport-uncollaborate/:id';
    return $resource(resourceUrl, {}, {
        'query': { method: 'POST', isArray: false },
        'post': {
            method: 'POST',
            transformResponse: function (data) {
                if (data) {
                    data = angular.fromJson(data);
                    // data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                }
                console.log(" UnCollaboraterOrSuppoter Serv: "+data)
                return data;
            }
        }
    });
}
