(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserCollaborateDialogController', UserCollaborateDialogController);

    UserCollaborateDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'UserCollaborate', 'User', 'Goal'];

    function UserCollaborateDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, UserCollaborate, User, Goal) {
        var vm = this;

        vm.userCollaborate = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();
        vm.goals = Goal.query({filter: 'usercollaborate-is-null'});
        //alert(JSON.stringify(vm.goals))
        $q.all([vm.userCollaborate.$promise, vm.goals.$promise]).then(function() {
            if (!vm.userCollaborate.goalId) {
                return $q.reject();
            }
            return Goal.get({id : vm.userCollaborate.goalId}).$promise;
        }).then(function(goal) {
            vm.goals.push(goal);
           // alert(JSON.stringify(vm.goals))
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userCollaborate.id !== null) {
                UserCollaborate.update(vm.userCollaborate, onSaveSuccess, onSaveError);
            } else {
                UserCollaborate.save(vm.userCollaborate, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('leApp:userCollaborateUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
