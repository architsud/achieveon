(function() {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-collaborate', {
            parent: 'entity',
            url: '/user-collaborate',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'UserCollaborates'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-collaborate/user-collaborates.html',
                    controller: 'UserCollaborateController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('user-collaborate-detail', {
            parent: 'entity',
            url: '/user-collaborate/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'UserCollaborate'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-collaborate/user-collaborate-detail.html',
                    controller: 'UserCollaborateDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'UserCollaborate', function($stateParams, UserCollaborate) {
                    return UserCollaborate.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-collaborate',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-collaborate-detail.edit', {
            parent: 'user-collaborate-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-collaborate/user-collaborate-dialog.html',
                    controller: 'UserCollaborateDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserCollaborate', function(UserCollaborate) {
                            return UserCollaborate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-collaborate.new', {
            parent: 'user-collaborate',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-collaborate/user-collaborate-dialog.html',
                    controller: 'UserCollaborateDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                role: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-collaborate', null, { reload: 'user-collaborate' });
                }, function() {
                    $state.go('user-collaborate');
                });
            }]
        })
        .state('user-collaborate.edit', {
            parent: 'user-collaborate',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-collaborate/user-collaborate-dialog.html',
                    controller: 'UserCollaborateDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserCollaborate', function(UserCollaborate) {
                            return UserCollaborate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-collaborate', null, { reload: 'user-collaborate' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-collaborate.delete', {
            parent: 'user-collaborate',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-collaborate/user-collaborate-delete-dialog.html',
                    controller: 'UserCollaborateDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserCollaborate', function(UserCollaborate) {
                            return UserCollaborate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-collaborate', null, { reload: 'user-collaborate' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
