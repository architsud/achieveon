(function() {
    'use strict';

    angular
        .module('leApp')
        .factory('UserCollaborateSearch', UserCollaborateSearch);

    UserCollaborateSearch.$inject = ['$resource'];

    function UserCollaborateSearch($resource) {
        var resourceUrl =  'api/_search/user-collaborates/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
