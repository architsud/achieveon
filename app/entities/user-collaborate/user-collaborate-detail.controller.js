(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserCollaborateDetailController', UserCollaborateDetailController);

    UserCollaborateDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserCollaborate', 'User', 'Goal'];

    function UserCollaborateDetailController($scope, $rootScope, $stateParams, previousState, entity, UserCollaborate, User, Goal) {
        var vm = this;

        vm.userCollaborate = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('leApp:userCollaborateUpdate', function(event, result) {
            vm.userCollaborate = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
