(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('GoalDeleteController', GoalDeleteController);

    GoalDeleteController.$inject = ['$uibModalInstance', '$state', 'entity', 'Goal'];

    function GoalDeleteController($uibModalInstance, $state, entity, Goal) {
        var vm = this;

        vm.goal = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        vm.previousState = previousState.name;
        function clear() {
            $uibModalInstance.dismiss('cancel');
           // $state.go('vm.previousState')
        }

        function confirmDelete(id) {
            Goal.delete({ id: id },
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
