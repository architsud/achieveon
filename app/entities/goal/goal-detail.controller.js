(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('GoalDetailController', GoalDetailController);

    GoalDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Goal', 'Dimension', 'User', 's3client', '$uibModal', '$state', 'SubGoal', 'Post', 'Principal', 'CustomGoal', 'Comment', 'CollaboratorsByGoal', 'InspiredGoal'];

    function GoalDetailController($scope, $rootScope, $stateParams, previousState, entity, Goal, Dimension, User, s3client, $uibModal, $state, SubGoal, Post, Principal, CustomGoal, Comment, CollaboratorsByGoal, InspiredGoal) {
        var vm = this;
        //vm.getGoalDetails = getGoalDetails;
        vm.goalId = $stateParams.id;
        vm.goal = entity;
        // function getGoalDetails(result) {
        //     vm.goal = result;
        // }
        vm.previousStateFun = previousStateFun;
        vm.previousState = previousState;
        vm.InspiredGoalFun = InspiredGoalFun;
        function previousStateFun() {
            // alert(JSON.stringify(vm.previousState))
            // $state.go(vm.previousState.name)
            // $state.go('goal')
            $state.go(vm.previousState.name, { id: vm.previousState.params.id })
        }
        //  Dimension.query({ id: vm.goal.dimensionId }, function (result) {
        //         vm.dimension = result;                 
        //     }).$promise; 
        vm.dimension = Dimension.get({ id: vm.goal.dimensionId })
        Principal.identity().then(function (u) {
            vm.loginUser = u;
        });
        // for inspier goal 
        function InspiredGoalFun(Id) {
            InspiredGoal.query({ userId: vm.loginUser.id, id: Id }, onInspiredGoalSuccess, onInspiredGoalError)
        }

        function onInspiredGoalSuccess() {
            // loadUserGoals()
            alert("Your Copied Goal was added")
            $state.go("goal");
        }
        function onInspiredGoalError() {
            alert("An error occured while adding Your Copied Goal")
        }

        vm.goal.collections = s3client.getCollections(vm.goal.imageGuid);
        // vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('leApp:goalUpdate', function (event, result) {
            vm.goal = result;
        });
        vm.getPosts = getPosts;
        getPosts();
        function getPosts() {
            CustomGoal.query({ goalId: vm.goalId }, function (result) {
                vm.posts = result;
            }).$promise;
        }
        $scope.$on('$destroy', unsubscribe);
        $scope.isCollapsed = true;
        $scope.isCollapsedTag = true;
        //vm.addSubGoals = addSubGoals;
        vm.deleteGoal = deleteGoal;
        vm.modalInstance = "";
        vm.tags = [];
        vm.post = {};
        vm.comment = {};
        vm.openTitle = false;
        vm.openDescription = false;
        vm.allSubGoals = [];
        vm.savePost = savePost;
        vm.saveComment = saveComment;
        var m1 = (vm.goal.metricAmount / vm.goal.metricTarget) * 100;
        vm.mainGoal = isNaN(m1) ? 0 : m1;
        vm.totalOfSubGoals = 0;
        vm.eachGoalPercentage = 0;
        vm.getCollaboratorsForGoal = getCollaboratorsForGoal;
        vm.addCollaborators = [];
        vm.addSupporters = [];
        vm.createSubGoal = createSubGoal;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.datePickerOpenStatus.startTime = false;
        vm.datePickerOpenStatus.finishTime = false;
        vm.datePickerOpenStatus.creatTime = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }

        vm.selectColors = ['#ff4852', '#63e762', '#ff8c3f'];


        vm.metricTypes = [
            {
                "name": "Currency",
                "values": ["USD", "Euro"]
            },
            {
                "name": "Numeric",
                "values": [""]
            },
            {
                "name": "Status",
                "values": ["% Complete"]
            }
        ];
        getCollaboratorsForGoal();
        //Open popup modal to edit sub goal
        function createSubGoal(val1, val2, val3) {
            if (vm.allSubGoals.length >= 6 && val2 == 'no') {
                alert("max 6 sub goal")
            }
            else {
                vm.subGoal = {};
                vm.showLabel = 'no';
                vm.subGoal.metricType = 'Status'

                vm.changeMetricSubType = changeMetricSubType;
                function changeMetricSubType() {
                    for (var i = 0; i < vm.metricTypes.length; i++) {
                        if (vm.metricTypes[i].name == vm.subGoal.metricType) {
                            vm.subMetricValues = vm.metricTypes[i].values;
                            if (vm.metricTypes[i].name != "Numeric") {
                                vm.subGoal.metricSubType = vm.metricTypes[i].values[0]
                            }
                            if (vm.metricTypes[i].name == "Status") {
                                vm.subGoal.metricTarget = 100
                            }
                        }
                    }
                }
                vm.modalInstance = $uibModal.open({
                    templateUrl: 'app/entities/goal/goal-sub-goals.html'
                    , size: 'md'
                    , scope: $scope
                    , controller: ['$scope', '$uibModal', SubGoalInstanceController]
                });
            }
        }
        vm.close = close;
        function close() {
            // vm.subGoalMetricType = vm.metricTypes[0];
            // vm.subGoalMetricSubType = vm.subGoalMetricType.values[0];
            // vm.subGoalMetricSubTypeOther = '';
            vm.modalInstance.close();
        }

        function SubGoalInstanceController($scope, $uibModalInstance) {
            // vm.subGoalMetricType = vm.metricTypes[0];
            // vm.subGoalMetricSubType = vm.subGoalMetricType.values[0];
            // vm.subGoalMetricSubTypeOther = '';
            // if (vm.subGoal) {
            //   vm.subGoalMetricType.name = vm.subGoal.metricType;
            //   vm.subGoalMetricSubType = vm.subGoal.metricSubType;
            // }

            vm.addSubGoal = addSubGoal;
            //vm.editSubGoal = editSubGoal;
            $scope.cancel = function (subGoal) {
                vm.modalInstance.close();
            };

            $scope.save = function () {
                $uibModalInstance.close($scope.theThingIWantToSave);
            };

            //function to add new sub goal into sub goals array
            function addSubGoal(subGoal) {
                subGoal.id = null;
                // subGoal.metricType = vm.subGoalMetricType.name;
                // subGoal.metricSubType = vm.subGoalMetricSubType;
                //subGoal.metricSubTypeOther = vm.subGoalMetricSubTypeOther;
                if (subGoal.metricAmount == undefined || subGoal.metricTarget == undefined) {
                    subGoal.metricAmount = 0;
                    subGoal.metricTarget = 0;
                    subGoal.goalpercent = 0;
                } else {
                    var val = parseInt((subGoal.metricAmount / subGoal.metricTarget) * 100);
                    subGoal.goalpercent = isNaN(val) ? 0 : val;
                }
                subGoal.color = vm.selectColors[1];
                subGoal.startTime = new Date();
                subGoal.goalId = vm.goalId;
                vm.eachGoalPercentage += subGoal.goalpercent;
                vm.allSubGoals.push(subGoal);
                vm.totalOfSubGoals = parseInt(vm.eachGoalPercentage / vm.allSubGoals.length);
                vm.subGoalMetricType = vm.metricTypes[0];
                vm.subGoalMetricSubType = vm.subGoalMetricType.values[0];
                vm.subGoalMetricSubTypeOther = '';
                vm.modalInstance.close();
                vm.goal.subGoals = vm.allSubGoals;
                Goal.update(vm.goal, onMetricSaveSuccess, onMetricSaveError);

            }
            //Update sub goal with new data
            // function editSubGoal(subGoal) {
            //     vm.eachGoalPercentage -= subGoal.editPercentage;
            //     // subGoal.metricType = vm.subGoalMetricType.name;
            //     // subGoal.metricSubType = vm.subGoalMetricSubType;
            //     var val = parseInt((subGoal.metricAmount / subGoal.metricTarget) * 100);
            //     subGoal.goalpercent = isNaN(val) ? 0 : val;
            //     vm.eachGoalPercentage += subGoal.goalpercent;
            //     vm.allSubGoals[subGoal.index] = subGoal;
            //     var val2 = parseInt(vm.eachGoalPercentage / vm.allSubGoals.length);
            //     vm.totalOfSubGoals = isNaN(val2) ? 0 : val2;
            //     vm.subGoal = {};
            //     vm.modalInstance.close();
            // }

        }
        $scope.$watchCollection('[vm.goal.metricTarget, vm.goal.metricAmount, vm.totalOfSubGoals]', function (newVal, oldVal) {
            if (newVal) {
                if (vm.goal.metricTarget && vm.goal.metricAmount > vm.goal.metricTarget) {
                    //alert('Current value should be less than the Target value')
                    // vm.goal.metricAmount = 0;
                    vm.goal.metricTarget = vm.goal.metricAmount;
                } else {
                    var m4 = parseInt((vm.goal.metricAmount / vm.goal.metricTarget) * 100) || 0;
                    vm.mainGoal = isNaN(m4) ? 0 : m4;
                    if (vm.mainGoal) {
                        if (vm.metricStatus == 'yes') {
                            vm.metricStatus = 'no'
                        } else {
                            vm.metricStatus = 'yes'
                        }
                    }
                    else {
                        if (vm.metricStatus == 'yes') {
                            vm.metricStatus = 'no'
                        } else {
                            vm.metricStatus = 'yes'
                        }
                    }
                }
            }
        });

        function getCollaboratorsForGoal() {

            CollaboratorsByGoal.query({ goalId: vm.goalId }, function (result) {
                getCollaboratorsForGoal();
                function getCollaboratorsForGoal() {
                    CollaboratorsByGoal.query({ goalId: vm.goalId }, function (result) {
                        // vm.addCollaborators = result;
                        result.forEach(function (user) {
                            if (user.role == 'COLLABORATOR') {
                                vm.addCollaborators.push(user)
                            } else {
                                vm.addSupporters.push(user)
                            }
                        })
                    }).$promise;
                }
            }).$promise;
        }
        if (vm.goal.subGoals && vm.goal.subGoals.length > 0) {
            vm.goal.subGoals.map(function (subGoal) {

                var m1 = parseInt((subGoal.metricAmount / subGoal.metricTarget) * 100);
                subGoal.goalpercent = isNaN(m1) ? 0 : m1;
                vm.eachGoalPercentage += subGoal.goalpercent;
                if (subGoal.goalpercent <= 25) {
                    subGoal.color = "#ff4852";
                }
                else if (subGoal.goalpercent <= 50) {
                    subGoal.color = "#F19447";
                }
                else if (subGoal.goalpercent <= 75) {
                    subGoal.color = "#F2E375";
                }
                else if (subGoal.goalpercent <= 100) {
                    subGoal.color = "#87E35A";
                }
                vm.allSubGoals.push(subGoal);
            })

            vm.totalOfSubGoals = parseInt(vm.eachGoalPercentage / vm.allSubGoals.length)
        }
        function deleteGoal(goalId) {
            vm.dimensionName = vm.dimension.name;
            vm.dimensionId = vm.dimension.id;
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/goal/goal-delete-dialog.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModalInstance', DeleteController]
            });
        }
        function DeleteController($scope, $uibModalInstance) {
            vm.clear = clear;
            vm.confirmDelete = confirmDelete;

            function clear() {
                $uibModalInstance.dismiss('cancel');
            }
            function confirmDelete(id) {
                Goal.delete({ id: id },
                    function () {
                        $uibModalInstance.close(true);
                        $state.go('goal')
                    });
            }
        }
        function savePost(type) {
            vm.isSaving = true;
            if (type == 'update') {
                Post.update(vm.post, onPostSaveSuccess, onPostSaveError);
            } else {
                vm.post.creatTime = new Date();
                vm.post.goalId = vm.goal.id;
                vm.post.userId = vm.loginUser.id;
                // alert("save: " + JSON.stringify(vm.post))
                Post.save(vm.post, onPostSaveSuccess, onPostSaveError);
            }
        }
        function onPostSaveSuccess(result) {
            $scope.$emit('leApp:postUpdate', result);
            //$uibModalInstance.close(result);
            // vm.isSaving = false;
            vm.post = {};
            //vm.openTitle = false;
            // vm.openDescription = false;
            getPosts();
        }

        function onPostSaveError() {
            //vm.isSaving = false;
        }
        function saveComment(type, postId, comment) {
            if (type == 'update') {
                if (vm.comment.comment !== null) {
                    vm.comment.creatTime = new Date();
                    vm.comment.id = comment.id;
                    vm.comment.comment = comment.comment;
                    vm.comment.postId = postId;
                    vm.comment.userId = vm.loginUser.id;
                    Comment.update(vm.comment, onCommentSaveSuccess, onCommentSaveError);
                }
            } else {
                if (vm.comment.comment !== null) {
                    // vm.comment.creatTime = new Date();
                    vm.comment.comment = comment;
                    vm.comment.postId = postId;
                    vm.comment.userId = vm.loginUser.id;
                    // alert("save: " + JSON.stringify(vm.comment))
                    Comment.save(vm.comment, onCommentSaveSuccess, onCommentSaveError);
                }
            }
        }
        function onCommentSaveSuccess(result) {
            $scope.$emit('leApp:commentUpdate', result);
            // $uibModalInstance.close(result);
            vm.isSaving = false;
            vm.comment = {};
            getPosts();
        }

        function onCommentSaveError() {
            vm.isSaving = false;
        }
        vm.deleteComment = deleteComment;
        function deleteComment(id) {
            Comment.delete({ id: id },
                function () {
                    //$uibModalInstance.close(true);
                    getPosts();
                });
        }
        vm.metricStatus = 'no';
        vm.saveMetric = saveMetric;
        function saveMetric() {
            Goal.update(vm.goal, onMetricSaveSuccess, onMetricSaveError);
        }
        function onMetricSaveSuccess(result) {
            //  $scope.$emit('leApp:goalUpdate', result);
            vm.isSaving = false;
            //  vm.goal= Goal.get({ id: $stateParams.id }).$promise;
            $state.reload()
            //vm.getGoalDetails(result)
        }
        function onMetricSaveError() {
            vm.isSaving = false;
        }

    }
})();
