(function () {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('goal', {
                parent: 'entity',
                url: '/goal',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Goals'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/goal/goals.html',
                        controller: 'GoalController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    previousState: ["$state", function ($state) {

                        var currentStateData = {
                            name: $state.current.name || 'goal',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('featured-goal', {
                parent: 'entity',
                url: '/featuredgoal',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Featured Goals'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/goal/featuredgoals.html',
                        controller: 'featuredGoalController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    previousState: ["$state", function ($state) {

                        var currentStateData = {
                            name: $state.current.name || 'goal',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            // .state('dimensions', {
            //     parent: 'goal',
            //     url: '/dimensions',
            //     data: {
            //         authorities: []
            //     },
            //     views: {
            //         'content@': {
            //             templateUrl: 'app/entities/goal/goal-dimensions.html',
            //             controller: 'GoalController',
            //             controllerAs: 'vm'
            //         }
            //     }
            // })
            // .state('goal-detail', {
            //     parent: 'entity',
            //     url: '/goal/{id}',
            //     data: {
            //         authorities: ['ROLE_USER'],
            //         pageTitle: 'Goal'
            //     },
            //     views: {
            //         'content@': {
            //             // templateUrl: 'app/entities/goal/goal-detail.html',
            //             templateUrl: 'app/entities/goal/goal-detail.html',
            //             controller: 'GoalDetailController',
            //             controllerAs: 'vm'
            //         }
            //     },
            //     resolve: {
            //         entity: ['$stateParams', 'Goal', function ($stateParams, Goal) {
            //             return Goal.get({ id: $stateParams.id }).$promise;
            //         }],
            //         previousState: ["$state", function ($state) {
            //             //alert(JSON.stringify($state))
            //             var currentStateData = {
            //                 name: $state.current.name || 'goal',
            //                 params: $state.params,
            //                 url: $state.href($state.current.name, $state.params)
            //             };
            //             return currentStateData;
            //         }]
            //     }
            // })
            .state('goal-detail', {
                parent: 'entity',
                url: '/goal/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Goal'
                },
                views: {
                    'content@': {
                        // templateUrl: 'app/entities/goal/goal-detail.html',
                        templateUrl: 'app/entities/goal/goal-detail-new-design.html',
                        //templateUrl: 'app/entities/goal/mobile_goal_detail.html',
                        controller: 'NewGoalDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Goal', function ($stateParams, Goal) {
                        return Goal.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        //alert(JSON.stringify($state))
                        var currentStateData = {
                            name: $state.current.name || 'goal',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('goal-detail-mobile', {
                parent: 'entity',
                url: '/goalmobile/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Goal'
                },
                views: {
                    'content@': {
                        // templateUrl: 'app/entities/goal/goal-detail.html',
                        // templateUrl: 'app/entities/goal/goal-detail-new-design.html',
                        templateUrl: 'app/entities/goal/mobile_goal_detail.html',
                        controller: 'NewGoalDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Goal', function ($stateParams, Goal) {
                        return Goal.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        //alert(JSON.stringify($state))
                        var currentStateData = {
                            name: $state.current.name || 'goal',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('goal.new', {
                parent: 'goal',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Goal'
                },
                views: {
                    'content@': {
                        //templateUrl: 'app/entities/goal/goal-dialog.html',
                        templateUrl: 'app/entities/goal/goal-detail-new-design.html',
                        controller: 'NewGoalDetailController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    dimensionId: null
                },
                resolve: {
                    entity: function () {
                        return {
                            goalText: '',//null,
                            imageGuid: UUID.generate(),
                            defaultImage: false,
                            description: null,
                            status: null,
                            startTime: new Date(),
                            finishTime: null,
                            metricAmount: 0,
                            metricTarget: 0,
                            metricType: 'Numeric',
                            metricSubType: 'Number',
                            likeCount: null,
                            commentCount: null,
                            privacy: null,
                            notebook: null,
                            creatTime: new Date(),
                            id: null,
                            isNew: true
                        };
                    },
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'goal',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('goal.newmobile', {
                parent: 'goal',
                url: '/newmobile',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Goal'
                },
                views: {
                    'content@': {
                        //templateUrl: 'app/entities/goal/goal-dialog.html',
                        templateUrl: 'app/entities/goal/mobile_goal_detail.html',
                        controller: 'NewGoalDetailController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    dimensionId: null
                },
                resolve: {
                    entity: function () {
                        return {
                            goalText: '',//null,
                            imageGuid: UUID.generate(),
                            defaultImage: false,
                            description: null,
                            status: null,
                            startTime: new Date(),
                            finishTime: null,
                            metricAmount: 0,
                            metricTarget: 0,
                            metricType: 'Numeric',
                            metricSubType: 'Number',
                            likeCount: null,
                            commentCount: null,
                            privacy: null,
                            notebook: null,
                            creatTime: new Date(),
                            id: null,
                            isNew: true
                        };
                    },
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'goal',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('goal.edit', {
                parent: 'goal',
                url: '/{id}/edit?isInspire',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Goal edit'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/goal/goal-dialog.html',
                        controller: 'GoalDialogController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Goal', function ($stateParams, Goal) {
                        return Goal.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'goal',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
                // onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                //     $uibModal.open({
                //         templateUrl: 'app/entities/goal/goal-dialog.html',
                //         controller: 'GoalDialogController',
                //         controllerAs: 'vm',
                //         backdrop: 'static',
                //         size: 'lg',
                //         resolve: {
                //             entity: ['Goal', function(Goal) {
                //                 return Goal.get({id : $stateParams.id}).$promise;
                //             }]
                //         }
                //     }).result.then(function() {
                //         $state.go('goal', null, { reload: 'goal' });
                //     }, function() {
                //         $state.go('^');
                //     });
                // }]
            })
            .state('goal.delete', {
                parent: 'goal',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/goal/goal-delete-dialog.html',
                        controller: 'GoalDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Goal', function (Goal) {
                                return Goal.get({ id: $stateParams.id }).$promise;
                            }],
                            previousState: ["$state", function ($state) {
                                var currentStateData = {
                                    name: $state.current.name || 'goal',
                                    params: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('goal-detail({id:$stateParams.id})', null, { reload: 'goal-detail({id:$stateParams.id})' });
                    }, function () {
                        $state.go('^');
                        // $state.go('goal-detail({id:{id}})', null, { reload: 'goal-detail({id:{id}})' });
                    });
                }]
            })
            .state('goal.collection', {
                parent: 'goal',
                url: '/{id}/download',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/goal/download-collections.html',
                        controller: 'DownloadCollection',
                        controllerAs: 'vm',
                        size: 'sm',
                        resolve: {
                            entity: ['Goal', function (Goal) {
                                return Goal.get({ id: $stateParams.id }).$promise;
                            }],
                            previousState: ["$state", function ($state) {
                                var currentStateData = {
                                    name: $state.current.name || 'goal',
                                    params: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }]
                        }
                    })
                }]
            })
    }

})();
