(function () {
    'use strict';
    angular
        .module('leApp')
        .factory('Goal', Goal)
        .factory('CustomGoal', CustomGoal)
        .factory('CollaboratorsByGoal', CollaboratorsByGoal)
        .factory('CollaboratorsByUser', CollaboratorsByUser)
        .factory('GoalsByUserId', GoalsByUserId)
        .factory('OwnAndCollaboratedGoals', OwnAndCollaboratedGoals)
        .factory('InspiredGoal', InspiredGoal)
        .factory('SearchGoals', SearchGoals)
        .factory('FavouriteGoal', FavouriteGoal)
        .factory('UnfavouriteGoal', UnfavouriteGoal)
        .factory('FeaturedGoal', FeaturedGoal)
        .factory('shareGoal', shareGoal);
    Goal.$inject = ['$resource', 'DateUtils'];
    function Goal($resource, DateUtils) {
        var resourceUrl = 'api/goals/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.startTime = DateUtils.convertLocalDateFromServer(data.startTime);
                        data.finishTime = DateUtils.convertLocalDateFromServer(data.finishTime);
                        data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.startTime = DateUtils.convertLocalDateToServer(copy.startTime);
                    copy.finishTime = DateUtils.convertLocalDateToServer(copy.finishTime);
                    copy.creatTime = DateUtils.convertLocalDateToServer(copy.creatTime);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.startTime = DateUtils.convertLocalDateToServer(copy.startTime);
                    copy.finishTime = DateUtils.convertLocalDateToServer(copy.finishTime);
                    copy.creatTime = DateUtils.convertLocalDateToServer(copy.creatTime);
                    return angular.toJson(copy);
                }
            }
        });
    }
    //Get posts of goal by goalId
    CustomGoal.$inject = ['$resource', 'DateUtils'];
    function CustomGoal($resource, DateUtils) {
        var resourceUrl = '/api/postsByGoal/:goalId';
        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                    }
                    return data;
                }
            }
        });
    }
    //Get collaborators by goalId
    CollaboratorsByGoal.$inject = ['$resource', 'DateUtils'];
    function CollaboratorsByGoal($resource, DateUtils) {
        var resourceUrl = '/api/user-collaborates-goal/:goalId';
        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        //data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                    }
                    return data;
                }
            }
        });
    }
    //Get collaborators by userId
    CollaboratorsByUser.$inject = ['$resource', 'DateUtils'];
    function CollaboratorsByUser($resource, DateUtils) {
        var resourceUrl = '/api/user-collaborates-user/:userId';
        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                    }
                    return data;
                }
            }
        });
    }
    //Get goals by userId
    GoalsByUserId.$inject = ['$resource', 'DateUtils'];
    function GoalsByUserId($resource, DateUtils) {
        var resourceUrl = '/api/goalsbyuser/:userId';
        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                    }
                    return data;
                }
            }
        });
    }
    //Show goals in dashboard---- OWN/COLLABORATOR/SUPPORTER
    OwnAndCollaboratedGoals.$inject = ['$resource', 'DateUtils'];
    function OwnAndCollaboratedGoals($resource, DateUtils) {
        var resourceUrl = 'api/goals/:id';
        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        // data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                    }
                    return data;
                }
            }
        });
    }
})();
InspiredGoal.$inject = ['$resource', 'DateUtils'];
function InspiredGoal($resource, DateUtils) {
    var resourceUrl = 'api/goals-inspried/:id';
    return $resource(resourceUrl, {}, {
        'query': { method: 'POST', isArray: false },
        'post': {
            method: 'POST',
            transformResponse: function (data) {
                if (data) {
                    data = angular.fromJson(data);
                    // data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                }
                return data;
            }
        }
    });
}
FeaturedGoal.$inject = ['$resource', 'DateUtils'];
function FeaturedGoal($resource, DateUtils) {
    var resourceUrl = 'api/featured-goals/:id';
    return $resource(resourceUrl, {}, {
        'query': { method: 'GET', isArray: true },
        'get': {
            method: 'GET',
            transformResponse: function (data) {
                if (data) {
                    data = angular.fromJson(data);
                    // data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                }
                return data;
            }
        }
    });
}
FavouriteGoal.$inject = ['$resource', 'DateUtils'];
function FavouriteGoal($resource, DateUtils) {
    var resourceUrl = 'api/goal/favourite/:id';
    return $resource(resourceUrl, {}, {
        'query': { method: 'POST', isArray: false },
        'post': {
            method: 'POST',
            transformResponse: function (data) {
                if (data) {
                    data = angular.fromJson(data);
                    // data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                }
                return data;
            }
        }
    });
}
UnfavouriteGoal.$inject = ['$resource', 'DateUtils'];
function UnfavouriteGoal($resource, DateUtils) {
    var resourceUrl = 'api/goal/dislike/:id';
    return $resource(resourceUrl, {}, {
        'query': { method: 'POST', isArray: false },
        'post': {
            method: 'POST',
            transformResponse: function (data) {
                if (data) {
                    data = angular.fromJson(data);
                    // data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                }
                return data;
            }
        }
    });
}
SearchGoals.$inject = ['$resource'];
function SearchGoals($resource) {
    var resourceUrl = 'api/_search/goals/:id';
    return $resource(resourceUrl, {}, {
        'query': { method: 'GET', isArray: true }
    });
}
shareGoal.$inject = ['$resource', 'DateUtils'];
function shareGoal($resource, DateUtils) {
    var resourceUrl = 'api/goals/share';
    return $resource(resourceUrl, {}, {
        'query': { method: 'POST', isArray: false },
        'post': {
            method: 'POST',
            transformResponse: function (data) {
                if (data) {
                    data = angular.fromJson(data);
                    // data.creatTime = DateUtils.convertLocalDateFromServer(data.creatTime);
                }
                return data;
            }
        }
    });
}
