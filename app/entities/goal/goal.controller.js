(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('GoalController', GoalController);

    GoalController.$inject = ['$scope', '$window', '$state', 'Goal', 'GoalSearch', 'ParseLinks', 'AlertService', 'OwnAndCollaboratedGoals', 'Principal', 'InspiredGoal', 'Dimension', '$uibModal', '$timeout', 'FavouriteGoal', 'UnfavouriteGoal'];

    function GoalController($scope, $window, $state, Goal, GoalSearch, ParseLinks, AlertService, OwnAndCollaboratedGoals, Principal, InspiredGoal, Dimension, $uibModal, $timeout, FavouriteGoal, UnfavouriteGoal) {
        var vm = this;
       
        Dimension.query(function (result) {
            vm.DIMENSIONS = result;

        }).$promise;
        Principal.identity().then(function (u) {
            vm.loginUser = u;
            vm.staticPercent = 50;
            vm.today = new Date();
            vm.goals = [];
            vm.loadPage = loadPage;
            vm.page = 0;
            vm.links = {
                last: 0
            };
            vm.predicate = 'id';
            vm.reset = reset;
            vm.reverse = true;
            vm.clear = clear;
            //vm.loadAll = loadAll;
            vm.search = search;
            //for Favourite goal
            vm.markFavourite = markFavourite;
            vm.unMarkFavourite = unMarkFavourite;
            function markFavourite(goalId) {
                FavouriteGoal.query({ "userId": vm.loginUser.id, "id": goalId, "firstName": vm.loginUser.firstName });
                $timeout(function () {
                    $state.reload();
                }, 600)

            }
            function unMarkFavourite(goalId) {
                UnfavouriteGoal.query({ "userId": vm.loginUser.id, "id": goalId, "firstName": vm.loginUser.firstName });
                $timeout(function () {
                    $state.reload();
                }, 600)
            }
            vm.InspiredGoalFun = InspiredGoalFun;
            // for inspier goal 
            function InspiredGoalFun(goal) {

                var goal = goal
                goal.parentGoalId = goal.id;
                goal.goalText = "Copy of " + goal.goalText;
                goal.id = "";
                goal.userCollaborators = [];
                for (var i = 0; i < vm.DIMENSIONS.length; i++) {
                    if (vm.DIMENSIONS[i].name == "DEFAULT") {
                        goal.dimensionId = vm.DIMENSIONS[i].id;
                        goal.dimensionName = vm.DIMENSIONS[i].name;
                    }
                }
                goal.imageGuid = "";
                for (var i = 0; i < goal.subGoals.length; i++) {
                    goal.subGoals[i].id = "";
                    // vm.goal.subGoals[i].goalId = "";
                }
                goal.userId = vm.loginUser.id;
                InspiredGoal.query(goal, onInspiredGoalSuccess, onInspiredGoalError);
            }
            //Show alert
            vm.showAlert = showAlert;
            function showAlert(message) {
                vm.message = message;
                vm.modalInstance = $uibModal.open({
                    templateUrl: 'app/entities/goal/alert-modal.html'
                    , size: 'md'
                    , scope: $scope
                    , controller: ['$scope', '$uibModalInstance', ShowAlertController]
                });
            }
            function ShowAlertController($scope, $uibModalInstance) {
                $timeout(function () {
                    $uibModalInstance.close();
                }, 1300)
            }
            function onInspiredGoalSuccess(result) {              // alert(JSON.stringify(result))
                // 

                var data = "Your Copied Goal was added successfully"
                vm.showAlert(data)
                $timeout(function () {
                    $state.go('goal-detail', { id: result.id })
                }, 600)


            }
            function onInspiredGoalError() {
                alert("An error occured while adding Your Copied Goal")
            }


            //loadAll();

            // function loadAll () {
            //     if (vm.currentSearch) {
            //         GoalSearch.query({
            //             query: vm.currentSearch,
            //             page: vm.page,
            //             size: 20,
            //             sort: sort()
            //         }, onSuccess, onError);
            //     } else {
            //         Goal.query({
            //             page: vm.page,
            //             size: 20,
            //             sort: sort()
            //         }, onSuccess, onError);
            //     }
            //     function sort() {
            //         var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
            //         if (vm.predicate !== 'id') {
            //             result.push('id');
            //         }
            //         return result;
            //     }

            //     function onSuccess(data, headers) {
            //         vm.links = ParseLinks.parse(headers('link'));
            //         vm.totalItems = headers('X-Total-Count');
            //         for (var i = 0; i < data.length; i++) {
            //             vm.goals.push(data[i]);
            //         }
            //     }

            //     function onError(error) {
            //         AlertService.error(error.data.message);
            //     }
            // }
            vm.share = share;
            function share(goal) {
                $scope.share = {
                    templateUrl: 'shareInfo.html',
                    goalURL: 'https://www.achieveon.com/#/goal/' + goal.id,
                    goal: goal,
                    goalimg: 'https://s3-us-west-1.amazonaws.com/life-edge/goal/' + goal.imageGuid + '/logo/goallogo.jpg'
                };
            }
            $scope.shareDialog = function (goal) {

                var ogUrl = 'https://www.achieveon.com/#/goal/' + goal.id;
                //'http://localhost:8080/#/goal/'+vm.goal.id;
                FB.ui({
                    method: 'share_open_graph',
                    action_type: 'og.shares',
                    action_properties: JSON.stringify({
                        object: {
                            'og:url': ogUrl,
                            'og:href': ogUrl,
                            'og:title': goal.goalText,
                            'og:description': goal.description,
                            'og:image': 'https://s3-us-west-1.amazonaws.com/life-edge/goal/' + goal.imageGuid + '/logo/goallogo.jpg'
                        }
                    })
                },
                    // callback
                    function (response) {
                        if (response && !response.error_message) {
                            // then get post content
                            //  alert('successfully posted. Status id : '+JSON.stringify(response));
                        } else {
                            // alert('Something went error.');
                        }
                    });
            }
            vm.goals = [];
            // Goal.query(function (result) {
            //     vm.goals = result.sort(function (a, b) {
            //         return a.id - b.id
            //     });
            //     loadAll();
            // }).$promise;
            loadUserGoals()
            function loadUserGoals() {

                OwnAndCollaboratedGoals.query({ userId: vm.loginUser.id }, function (result) {

                    vm.goals = result.sort(function (a, b) {
                        return a.id - b.id
                    });

                    loadAll();
                    // vm.addCollaborators = result;
                    // result.forEach(function (user) {
                    //     if (user.role == 'COLLABORATOR') {
                    //         vm.addCollaborators.push(user)
                    //     } else {
                    //         vm.addSupporters.push(user)
                    //     }
                    // })
                }).$promise;
            }
            function loadAll() {
                vm.goalsCompletedCount = 0;
                vm.goalsPendingCount = 0;
                for (var i = 0; i < vm.goals.length; i++) {
                    console.log("Goal ID: " + vm.goals[i].id)
                    vm.goals[i].userCollaborators.forEach(function (user) {
                        if (user.networkUserId == vm.loginUser.id && user.role == 'COLLABORATOR') {
                            vm.goals[i].goalStatus = 'COLLABORATOR';
                        } else if (user.networkUserId == vm.loginUser.id && user.role == 'SUPPORTER') {
                            vm.goals[i].goalStatus = 'SUPPORTER';
                        }
                    })


                    var m1 = parseInt((vm.goals[i].metricAmount / vm.goals[i].metricTarget) * 100) || 0;
                    vm.goals[i].goalPercentage = isNaN(m1) ? 0 : m1;
                    vm.goals[i].finishTime = new Date(vm.goals[i].finishTime);
                    if (vm.goals[i].metricAmount != 0 && vm.goals[i].metricAmount == vm.goals[i].metricTarget) {
                        vm.goalsCompletedCount += 1;
                    } else {
                        vm.goalsPendingCount += 1;
                    }
                }
                vm.incompletedGoals = vm.goalsPendingCount;
                vm.completedGoals = vm.goalsCompletedCount;
                vm.mainGoal = parseInt((vm.goalsCompletedCount / vm.goals.length) * 100);
            }
            function reset() {
                vm.page = 0;
                vm.goals = [];
                loadAll();
            }

            function loadPage(page) {
                vm.page = page;
                loadAll();
            }

            function clear() {
                vm.goals = [];
                vm.links = {
                    last: 0
                };
                vm.page = 0;
                vm.predicate = 'id';
                vm.reverse = true;
                vm.searchQuery = null;
                vm.currentSearch = null;
                vm.loadAll();
            }

            function search(searchQuery) {
                if (!searchQuery) {
                    return vm.clear();
                }
                vm.goals = [];
                vm.links = {
                    last: 0
                };
                vm.page = 0;
                vm.predicate = '_score';
                vm.reverse = false;
                vm.currentSearch = searchQuery;
                vm.loadAll();
            }
        });
    }
})();
