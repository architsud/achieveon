(function () {
  'use strict';
  angular.module('leApp').controller('GoalDialogController', GoalDialogController);
  GoalDialogController.$inject = ['$rootScope', '$q', '$location', '$state', '$timeout', 'previousState', '$scope', '$stateParams', '$uibModal', '$http',
    'entity', 'Goal', 'Dimension', 'User', 's3client', 'UserPersonaSearch', 'DIMENSIONS', 'SubGoal', 'Principal', 'CustomGoal', 'UserCollaborate', 'CollaboratorsByGoal', 'GetFriendsByUserId', 'UserFriendsSearch', 'DeleteCollaboraterOrSuppoter', 'InspiredGoal', 'UserNetworkStatus'];

  function GoalDialogController($rootScope, $q, $location, $state, $timeout, previousState, $scope, $stateParams, $uibModal, $http,
    entity, Goal, Dimension, User, s3client, UserPersonaSearch, DIMENSIONS, SubGoal, Principal, CustomGoal, UserCollaborate, CollaboratorsByGoal, GetFriendsByUserId, UserFriendsSearch, DeleteCollaboraterOrSuppoter, InspiredGoal, UserNetworkStatus) {


    $scope.toggled = function (open) {
      //$log.log('Dropdown is now: ', open);
    };

    var vm = this;
    Principal.identity().then(function (u) {
      vm.loginUser = u;
    });
    //vm.DIMENSIONS = DIMENSIONS; 
    Dimension.query(function (result) {
      vm.DIMENSIONS = result;
      if (vm.goal.isNew == true && $stateParams.dimensionId == null) {
        for (var i = 0; i < vm.DIMENSIONS.length; i++) {
          if (vm.DIMENSIONS[i].name == "DEFAULT") {
            vm.goal.dimensionId = vm.DIMENSIONS[i].id;
            vm.goal.dimensionName = vm.DIMENSIONS[i].name;
          }
        }
      }
    }).$promise;
    vm.goalId = $stateParams.id;
    vm.modalInstance = "";
    vm.goal = entity;
    //alert(JSON.stringify(vm.goal));
    if (vm.goal.isNew == true) {
      vm.goal.metricType = 'Status';
    }
    vm.redirectBack = redirectBack;
    function redirectBack() {
      if (vm.goal.isNew == true) {
        $state.go('goal')
      } else {
        $state.go('goal-detail', { id: $stateParams.id })
      }
    }
    vm.allSubGoals = [];
    vm.eachGoalPercentage = 0;
    vm.totalOfSubGoals = 0;
    if (vm.goal.id) {
      //vm.showsubHeader = true;
      vm.showToolTip = "Update"
    } else {
      //vm.showsubHeader = false;
      vm.showToolTip = "Save"
    }
    vm.previousStateFun = previousStateFun;
    vm.previousState = previousState;
    function previousStateFun() {
      $state.go(vm.previousState.name, { id: vm.previousState.params.id })
    }


    vm.redirectToDimension = false;

    if ($stateParams.dimensionId != null) {
     // alert($stateParams.dimensionId)
      vm.goal.dimensionId = $stateParams.dimensionId;
      vm.redirectToDimension = true;
    }

    vm.clear = clear;
    vm.datePickerOpenStatus = {};
    vm.openCalendar = openCalendar;
    vm.save = save;
    vm.showCircle = false;
    //vm.collections = s3client.getCollections(vm.goal.id);
    var collectionsPromise = s3client.getCollections(vm.goal.imageGuid);
    collectionsPromise.then(function (cols) {
      vm.collections = cols;
      if (vm.collections.logo.length != 0) {
        //vm.logoPath = vm.collections.logo[0];
        //console.debug(vm.logoPath);
        vm.logoPath='https://s3-us-west-1.amazonaws.com/life-edge/goal/' + vm.goal.imageGuid + '/logo/goallogo.jpg';
      }
    }, function (reason) {
      console.log('Failed to update collections :' + reason);
    });
  //  vm.logoPath = "https://s3-us-west-1.amazonaws.com/life-edge/dimensions/"

    vm.users = User.query();
    vm.uploadImage = uploadImage;
    vm.breadcrumbsList = [
      { 'name': 'Home', 'sref': 'home' }
    ];

    vm.getCollaboratorsForGoal = getCollaboratorsForGoal;
    vm.changeMetricSubTypeForGoal = changeMetricSubTypeForGoal;
    //COLLABORATORS
    vm.searchCollaborators = searchCollaborators;
    vm.collaboratorSearchQuery = "Search Connections";
    vm.collaboratorResults = [];
    vm.collaboratorResultsShow = false;
    vm.collaborators = [];
    vm.addCollaborators = [];
    vm.addCollaboratorsFun = addCollaboratorsFun;
    vm.removeCollaboratorsFun = removeCollaboratorsFun;
    vm.userCollaborateSave = userCollaborateSave;
    //SUPPORTERS
    vm.searchSupporters = searchSupporters;
    vm.supportersSearchQuery = "Search Connections";
    vm.supportersResults = [];
    vm.supporterResultsShow = false;
    vm.addSupporters = [];
    vm.addSupportersFun = addSupportersFun;
    vm.removeSupportersFun = removeSupportersFun;
    //Get Goal collaborators
    if ($stateParams.isInspire == "true") {
      vm.goal.userCollaborators = [];
    }
    else {
      getCollaboratorsForGoal();
    }

    function getCollaboratorsForGoal() {
      CollaboratorsByGoal.query({ goalId: vm.goalId }, function (result) {
        // vm.addCollaborators = result;
        result.forEach(function (user) {
          if (user.role == 'COLLABORATOR') {
            vm.addCollaborators.push(user)
          } else {
            vm.addSupporters.push(user)
          }
        })
      }).$promise;
    }

    //Pre defined sub goal object
    vm.subGoal = {};

    function searchCollaborators() {

      vm.collaboratorResults = []
      if (vm.collaboratorSearchQuery == "Search Connections" || vm.collaboratorSearchQuery == "") {
        vm.collaboratorResults = []
      }
      else {
        //  console.log(vm.collaboratorSearchQuery)
        UserFriendsSearch.query({ query: vm.collaboratorSearchQuery }, function (result) {
          //console.log("searchCollaborators: " + JSON.stringify(result))
          // for (var i = 0, resultLen = result.length; i < resultLen; i++) {
          //   if (result[i].status != "PENDING") {
          //     vm.collaboratorResults.push(result[i])
          //   }
          // }

          vm.collaboratorResults = result;
          //vm.collaboratorResults = removeDuplicates(vm.collaboratorResults, "networkUserId");
          vm.collaboratorResultsShow = true;
        });
      }
    }
    function removeDuplicates(originalArray, prop) {
      var newArray = [];
      var lookupObject = {};

      for (var i in originalArray) {
        lookupObject[originalArray[i][prop]] = originalArray[i];
      }

      for (i in lookupObject) {
        newArray.push(lookupObject[i]);
      }
      return newArray;
    }
    function searchSupporters() {
      vm.supportersResults = []
      if (vm.supportersSearchQuery == "Search Connections" || vm.supportersSearchQuery == "") {
        vm.supportersResults = []
      }
      else {
        // console.log(vm.supportersSearchQuery)

        UserFriendsSearch.query({ query: vm.supportersSearchQuery }, function (result) {
          // console.log("searchSupporters: " + JSON.stringify(result))
          // for (var i = 0, resultLen = result.length; i < resultLen; i++) {
          //   if (result[i].status != "PENDING") {
          //     vm.supportersResults.push(result[i])
          //   }
          // }
          vm.supportersResults = result;
          // vm.supportersResults = removeDuplicates(vm.supportersResults, "networkUserId");
          vm.supporterResultsShow = true;
        });
      }
    }


    vm.metricTypes = [
      {
        "name": "Currency",
        "values": ["USD", "Euro"]
      },
      {
        "name": "Numeric",
        "values": [""]
      },
      {
        "name": "Status",
        "values": ["% Complete"]
      }
    ];

    // vm.selectedMetricType = vm.metricTypes[0];
    // vm.selectedMetricSubType = vm.selectedMetricType.values[0];
    // vm.selectedMetricSubTypeOther = '';

    // if (vm.goal) {
    //   vm.selectedMetricType.name = vm.goal.metricType;
    //   vm.selectedMetricSubType = vm.goal.metricSubType;
    // }
    function changeMetricSubTypeForGoal(type) {
      for (var i = 0; i < vm.metricTypes.length; i++) {

        if (vm.metricTypes[i].name == vm.goal.metricType) {
          vm.subMetricValuesForGoal = vm.metricTypes[i].values;
          if (vm.metricTypes[i].name != "Numeric") {
            vm.goal.metricSubType = vm.metricTypes[i].values[0]
          }
          if (vm.metricTypes[i].name == "Status") {
            vm.goal.metricTarget = 100
          }
        }
      }
    }



    vm.manageCollections = manageCollections;
    vm.uploadCollections = uploadCollections;

    //vm.addSubGoalsPercentage = addSubGoalsPercentage;
    vm.createSubGoal = createSubGoal;
    vm.deleteSubGoal = deleteSubGoal;
    //vm.changeMetricTypes = changeMetricTypes;
    vm.confirmDelete = confirmDelete;

    //Open delete popup modal for sub goal 
    function deleteSubGoal(subgoal, index) {
      $scope.deleteSubgoal = subgoal;
      $scope.deleteSubgoal.index = index;
      $scope.deleteSubgoal.mainGoal = vm.goal.goalText;
      $scope.deleteSubgoal.editPercentage = $scope.deleteSubgoal.goalpercent;
      //  for (var i = 0; i < vm.allSubGoals.length; i++) {
      //  if (vm.allSubGoals[i].subgoalText == subgoal.subgoalText) {
      //  vm.subGoal = angular.copy(vm.allSubGoals[index]);
      //   vm.subGoal.index = index;
      // vm.subGoal = vm.allSubGoals[i];
      vm.modalInstance = $uibModal.open({
        templateUrl: 'app/entities/goal/sub-goal-delete.html'
        , size: 'md'
        , scope: $scope
        , controller: ['$scope', '$uibModal', ModalInstanceController]
      });
      //  }
      // }
    }

    //Confirm delete sub goal
    function confirmDelete(value) {
      // for (var i = 0; i < vm.allSubGoals.length; i++) {
      // if (vm.allSubGoals[i].subgoalText === value.subgoalText) {
      vm.eachGoalPercentage -= value.editPercentage;
      if (vm.allSubGoals[value.index].id != null) {
        SubGoal.delete({ id: vm.allSubGoals[value.index].id }, function () { });
      }
      vm.allSubGoals.splice(value.index, 1);
      vm.totalOfSubGoals = parseInt(vm.eachGoalPercentage / vm.allSubGoals.length);
      //  break;
      // }
      // }
      vm.modalInstance.close();
    }

    //Open popup modal to edit sub goal
    function createSubGoal(val1, val2, val3) {
      if (vm.allSubGoals.length >= 6 && val2 == 'no') {
        alert("max 6 sub goal")
      }
      else {
        vm.subGoal = {};
        vm.showLabel = 'no';
        vm.subGoal.metricType = 'Status'
        if (val2 == 'yes') {
          vm.showLabel = 'yes';
          // for (var i = 0; i < vm.allSubGoals.length; i++) {
          // vm.allSubGoals[val3].index = val3;
          // if (vm.allSubGoals[i].index == val3) {
          vm.subGoal = angular.copy(vm.allSubGoals[val3]);
          vm.subGoal.index = val3;
          vm.subGoal.editPercentage = vm.subGoal.goalpercent;
          // }
          // }
        }

        vm.changeMetricSubType = changeMetricSubType;
        function changeMetricSubType() {
          for (var i = 0; i < vm.metricTypes.length; i++) {
            if (vm.metricTypes[i].name == vm.subGoal.metricType) {
              vm.subMetricValues = vm.metricTypes[i].values;
              if (vm.metricTypes[i].name != "Numeric") {
                vm.subGoal.metricSubType = vm.metricTypes[i].values[0]
              }
              if (vm.metricTypes[i].name == "Status") {
                vm.subGoal.metricTarget = 100
              }
            }
          }
        }
        vm.modalInstance = $uibModal.open({
          templateUrl: 'app/entities/goal/goal-sub-goals.html'
          , size: 'md'
          , scope: $scope
          , controller: ['$scope', '$uibModal', SubGoalInstanceController]
        });
      }
    }
    vm.close = close;
    function close() {
      // vm.subGoalMetricType = vm.metricTypes[0];
      // vm.subGoalMetricSubType = vm.subGoalMetricType.values[0];
      // vm.subGoalMetricSubTypeOther = '';
      vm.modalInstance.close();
    }

    function SubGoalInstanceController($scope, $uibModalInstance) {
      // vm.subGoalMetricType = vm.metricTypes[0];
      // vm.subGoalMetricSubType = vm.subGoalMetricType.values[0];
      // vm.subGoalMetricSubTypeOther = '';
      // if (vm.subGoal) {
      //   vm.subGoalMetricType.name = vm.subGoal.metricType;
      //   vm.subGoalMetricSubType = vm.subGoal.metricSubType;
      // }

      vm.addSubGoal = addSubGoal;
      vm.editSubGoal = editSubGoal;
      $scope.cancel = function (subGoal) {
        vm.modalInstance.close();
      };

      $scope.save = function () {
        $uibModalInstance.close($scope.theThingIWantToSave);
      };

      //function to add new sub goal into sub goals array
      function addSubGoal(subGoal) {
        subGoal.id = null;
        // subGoal.metricType = vm.subGoalMetricType.name;
        // subGoal.metricSubType = vm.subGoalMetricSubType;
        //subGoal.metricSubTypeOther = vm.subGoalMetricSubTypeOther;
        if (subGoal.metricAmount == undefined || subGoal.metricTarget == undefined) {
          subGoal.metricAmount = 0;
          subGoal.metricTarget = 0;
          subGoal.goalpercent = 0;
        } else {
          var val = parseInt((subGoal.metricAmount / subGoal.metricTarget) * 100);
          subGoal.goalpercent = isNaN(val) ? 0 : val;
        }
        subGoal.color = vm.selectColors[1];
        subGoal.startTime = new Date();
        subGoal.goalId = vm.goalId;
        vm.eachGoalPercentage += subGoal.goalpercent;
        vm.allSubGoals.push(subGoal);
        vm.totalOfSubGoals = parseInt(vm.eachGoalPercentage / vm.allSubGoals.length);
        vm.subGoalMetricType = vm.metricTypes[0];
        vm.subGoalMetricSubType = vm.subGoalMetricType.values[0];
        vm.subGoalMetricSubTypeOther = '';
        vm.modalInstance.close();
      }
      //Update sub goal with new data
      function editSubGoal(subGoal) {
        vm.eachGoalPercentage -= subGoal.editPercentage;
        // subGoal.metricType = vm.subGoalMetricType.name;
        // subGoal.metricSubType = vm.subGoalMetricSubType;
        var val = parseInt((subGoal.metricAmount / subGoal.metricTarget) * 100);
        subGoal.goalpercent = isNaN(val) ? 0 : val;
        vm.eachGoalPercentage += subGoal.goalpercent;
        vm.allSubGoals[subGoal.index] = subGoal;
        var val2 = parseInt(vm.eachGoalPercentage / vm.allSubGoals.length);
        vm.totalOfSubGoals = isNaN(val2) ? 0 : val2;
        vm.subGoal = {};
        vm.modalInstance.close();
      }

    }

    // function changeMetricTypes(selectedMetricType) {
    //   vm.selectedMetricType = vm.metricTypes[0];
    //   vm.selectedMetricSubType = vm.selectedMetricType.values[0];
    //   vm.selectedMetricSubTypeOther = '';
    // }

    vm.date = '2017-02-10';
    vm.selectColors = ['#ff4852', '#63e762', '#ff8c3f'];
    vm.changeInMetrics = false;
    //vm.changeMetricStatus = changeMetricStatus;

    // function changeMetricStatus() {
    //   vm.changeInMetrics = true;
    //   $timeout(function () {
    //   vm.changeInMetrics = false;
    //   }, 100);
    //   $timeout(function () {
    //   vm.changeInMetrics = true;
    //   }, 200);
    // }
    vm.metricStatus = 'no';
    var m1 = parseInt((vm.goal.metricAmount / vm.goal.metricTarget) * 100) || 0;
    vm.mainGoal = isNaN(m1) ? 0 : m1;
    if (vm.goal.subGoals && vm.goal.subGoals.length > 0) {
      vm.goal.subGoals.map(function (subGoal) {
        var m2 = parseInt((subGoal.metricAmount / subGoal.metricTarget) * 100);
        subGoal.goalpercent = isNaN(m2) ? 0 : m2;
        vm.eachGoalPercentage += subGoal.goalpercent;
        subGoal.color = "#ff4852"
        vm.allSubGoals.push(subGoal);
      })
      var m3 = parseInt(vm.eachGoalPercentage / vm.allSubGoals.length);
      vm.totalOfSubGoals = isNaN(m3) ? 0 : m3;
    }

    $scope.$watchCollection('[vm.goal.metricTarget, vm.goal.metricAmount, vm.totalOfSubGoals]', function (newVal, oldVal) {
      if (newVal) {
        if (vm.goal.metricTarget && vm.goal.metricAmount > vm.goal.metricTarget) {
          //alert('Current value should be less than the Target value')
          // vm.goal.metricAmount = 0;
          vm.goal.metricTarget = vm.goal.metricAmount;
        } else {
          var m4 = parseInt((vm.goal.metricAmount / vm.goal.metricTarget) * 100) || 0;
          vm.mainGoal = isNaN(m4) ? 0 : m4;
          if (vm.mainGoal) {
            if (vm.metricStatus == 'yes') {
              vm.metricStatus = 'no'
            } else {
              vm.metricStatus = 'yes'
            }
          }
          else {
            if (vm.metricStatus == 'yes') {
              vm.metricStatus = 'no'
            } else {
              vm.metricStatus = 'yes'
            }
          }
        }
      }
    });


    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function uploadImage() {
      vm.modalInstance = $uibModal.open({
        templateUrl: 'app/entities/goal/goal-dialog-uploadLogo.html'
        , size: 'lg'
        , scope: $scope
        , controller: ['$scope', '$uibModal', ModalInstanceController]
      });
    };

    function uploadCollections() {
      vm.modalInstance = $uibModal.open({
        templateUrl: 'app/entities/goal/goal-dialog-upload.html'
        , size: 'sm'
        , scope: $scope
        , controller: ['$scope', '$uibModal', ModalInstanceController]
      });
    };

    function manageCollections() {
      vm.modalInstance = $uibModal.open({
        templateUrl: 'app/entities/goal/goal-dialog-manage.html'
        , size: 'sm'
        , scope: $scope
        , controller: ['$scope', '$uibModal', ModalInstanceController]
      });
    };

    function clear() {
      // $uibModalInstance.dismiss('cancel');
      vm.modalInstance.close();
      vm.subGoal = {};
    }
    function addCollaboratorsFun(collaboratorData) {
      var collaborator = {};
      // collaborator.id="",
      collaborator.role = 'COLLABORATOR';
      collaborator.goalId = vm.goalId;
      collaborator.userId = collaboratorData.userId;
      collaborator.userNetworkId = collaboratorData.id;
      collaborator.firstName = collaboratorData.firstName;
      collaborator.lastName = collaboratorData.lastName;
      collaborator.occupation = collaboratorData.occupation;
      collaborator.networkUserId = collaboratorData.networkUserId;
      //collaborator.userLoginName=vm.loginUser.firstName+' '+vm.loginUser.lastName;
      console.log("#################: "+JSON.stringify(collaborator))
     // vm.addCollaborators.push(collaborator); //Adding user as collaborator into collaborator db
      // var collaboratorToSave = {};
      // collaboratorToSave.goalId = vm.goalId;
      // collaboratorToSave.userId = collaborator.id;
      // collaboratorToSave.role = 'COLLABORATOR';
      // vm.userCollaborate = collaboratorToSave;

    }
    function removeCollaboratorsFun(index, Id) {
      vm.addCollaborators.splice(index, 1);
      DeleteCollaboraterOrSuppoter.query({ id: Id })
    }
    function addSupportersFun(supporterData) {
      // vm.addSupporters.push(supporter)
      var supporter = {};
      // supporter.id="",
      supporter.role = 'SUPPORTER',
        supporter.goalId = vm.goalId;
      supporter.userId = supporterData.userId;
      supporter.userNetworkId = supporterData.id;
      supporter.firstName = supporterData.firstName;
      supporter.lastName = supporterData.lastName;
      supporter.occupation = supporterData.occupation;
      supporter.networkUserId = supporterData.networkUserId;
      //supporter.userLoginName=vm.loginUser.firstName+' '+vm.loginUser.lastName;;
      // alert(JSON.stringify(supporter))
      vm.addSupporters.push(supporter)  //Adding user as supporter into collaborator db
    }
    function removeSupportersFun(index, Id) {
      vm.addSupporters.splice(index, 1);
      DeleteCollaboraterOrSuppoter.query({ id: Id })
    }


    function ModalInstanceController($scope, $uibModalInstance) {
      //$scope.vm = vm;

      $scope.keyPrefix = 'goal/' + $scope.vm.goal.imageGuid + '/';
      $scope.save = function () {
        $uibModalInstance.close($scope.theThingIWantToSave);
      };
      $scope.cancel = function () {
        vm.modalInstance.close();
      };

      $scope.uploadS3File = function (isLogo) {
        var fileChooser = document.getElementById('file-chooser');
        var file = fileChooser.files[0];
        if (file) {
          //determine extension
          var folder = s3client.getFolderName(file.name, isLogo);
          var fileName = file.Name;
          if (isLogo) fileName = 'goallogo.jpg';
          var params = {
            Key: $scope.keyPrefix + folder + '/' + fileName
            , ContentType: file.type
            , Body: file
          };
          var bucket = s3client.getS3bucket();
          bucket.upload(params, function (err, data) {
            // results.innerHTML = err ? err : file.name + ' uploaded successfully.';
            if (isLogo) {
              vm.logoPath = "https://s3-us-west-1.amazonaws.com/life-edge/" + $scope.keyPrefix + folder + "/goallogo.jpg?" + new Date().getTime();;
            }
            else {
              var collectionsPromise = s3client.getCollections(vm.goal.imageGuid);
              collectionsPromise.then(function (cols) {
                vm.collections = cols;
              }, function (reason) {
                console.log('Failed to update collections :' + reason);
              });
            }
            $scope.cancel();
          });
        }
      }

      $scope.vm.chooseAvatar = function (id, avatarName) {
        var logoKey = $scope.keyPrefix + s3client.getFolderName(avatarName, true) + '/goallogo.jpg';
        var keySource = "avatars/dimensions/" + avatarName;
        var apiUrl = "/s3/copy?source=" + keySource + "&destination=" + logoKey;
        $http.get(apiUrl)
          .then(function successCallback(response) {
            vm.logoPath = "https://s3-us-west-1.amazonaws.com/life-edge/" + logoKey;
            vm.clear();
          }, function errorCallback(response) {
            alert("An unexpected error occurred: " + response);
            $scope.cancel();
          });
      }
    }

    //Main goal save
    function save() {
      vm.goal.userLoginName = vm.loginUser.firstName + ' ' + vm.loginUser.lastName;
      vm.goal.subGoals = vm.allSubGoals;
      // console.log("vm.addCollaborators: " + JSON.stringify(vm.addCollaborators))
      // console.log("vm.addSupporters: " + JSON.stringify(vm.addSupporters))
      vm.goal.userCollaborators = vm.addCollaborators.concat(vm.addSupporters);

      //console.log("vm.goal.userCollaborators :" + JSON.stringify(vm.goal))
      if (vm.goal.tags) {
        vm.goal.tags = vm.goal.tags.map(function (tag) {
          return tag.id;
        })
      }

      vm.isSaving = true;
      if ($stateParams.isInspire == "false") {
        if (vm.goal.id) {
          Goal.update(vm.goal, onSaveSuccess, onSaveError);
          // userCollaborateSave();
        } else {
          //alert("isInspire == false else")
          vm.goal.userId = vm.loginUser.id;
          vm.goal.userLogin = vm.loginUser.userLogin;
          vm.goal.userLoginName = vm.loginUser.userLoginName;
          Goal.save(vm.goal, onSaveSuccess, onSaveError);
          // userCollaborateSave();
        }
      }
      else if ($stateParams.isInspire == "true") {
        vm.goal.parentGoalId = vm.goal.id;
        vm.goal.id = "";
        for (var i = 0; i < vm.DIMENSIONS.length; i++) {
          if (vm.DIMENSIONS[i].name == "DEFAULT") {
            vm.goal.dimensionId = vm.DIMENSIONS[i].id;
            vm.goal.dimensionName = vm.DIMENSIONS[i].name;
          }
        }
        
        for (var i = 0; i < vm.goal.subGoals.length; i++) {
          vm.goal.subGoals[i].id = "";
        }
        vm.goal.userId = vm.loginUser.id;
        InspiredGoal.query(vm.goal, onInspiredGoalSuccess, onInspiredGoalError)

      }
      else {
        //alert("else")
        vm.goal.userId = vm.loginUser.id;
        Goal.save(vm.goal, onSaveSuccess, onSaveError);
        // userCollaborateSave(); 
      }

    }
    function onInspiredGoalSuccess() {
      alert("Your Copied Goal was added")
      $state.go('goal');
    }
    function onInspiredGoalError() {
      alert("An error occured while adding Your Copied Goal")
    }
    function onSaveSuccess(result) {
      $scope.$emit('leApp:goalUpdate', result);
      //$uibModalInstance.close(result);
      vm.isSaving = false;
      if (vm.redirectToDimension) {
        $state.go('dimension.edit', { id: $stateParams.dimensionId })
      } else if (vm.goal.isNew == true) {
        $state.go('goal')
      } else if ($stateParams.isInspire == "true") {
        $state.go('goal')
      } else {
        $state.go('goal-detail', { id: $stateParams.id })
      }
    }
    function onSaveError() {
      vm.isSaving = false;
    }
    //Save collaborators and supporters
    function userCollaborateSave() {
      vm.isSaving = true;
      //  vm.userCollaborate=vm.addCollaborators//.concat(vm.addSupporters)
      //  alert(JSON.stringify(vm.userCollaborate))
      // if (vm.userCollaborate.id !== null) {
      // UserCollaborate.update(vm.userCollaborate, onUserCollaborateSaveSuccess, onUserCollaborateSaveError);
      // } else {
      UserCollaborate.save(vm.userCollaborate, onUserCollaborateSaveSuccess, onUserCollaborateSaveError);
      // }
    }

    function onUserCollaborateSaveSuccess(result) {
      $scope.$emit('leApp:userCollaborateUpdate', result);
      //$uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onUserCollaborateSaveError() {
      vm.isSaving = false;
    }


    vm.datePickerOpenStatus.startTime = false;
    vm.datePickerOpenStatus.finishTime = false;
    vm.datePickerOpenStatus.creatTime = false;

    function openCalendar(date) {
      vm.datePickerOpenStatus[date] = true;
    }
  }
})();