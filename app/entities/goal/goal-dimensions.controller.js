(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('DimensionController', DimensionController);

    DimensionController.$inject = ['$scope', '$state', 'Dimension', 'DimensionSearch'];

    function DimensionController($scope, $state, Dimension, DimensionSearch) {
        var vm = this;

        vm.dimensions = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Dimension.query(function (result) {
                vm.dimensions = result;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            DimensionSearch.query({ query: vm.searchQuery }, function (result) {
                vm.dimensions = result;
            });
        }
    }
})();
