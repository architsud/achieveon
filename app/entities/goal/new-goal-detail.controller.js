(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('NewGoalDetailController', NewGoalDetailController);
    NewGoalDetailController.$inject = ['$scope', '$window', '$rootScope', '$stateParams', 'previousState', 'entity', 'Goal', 'Dimension', 'User', 's3client', '$uibModal', '$state', 'SubGoal', 'Post', 'Principal', 'CustomGoal', 'Comment', 'CollaboratorsByGoal', 'InspiredGoal', 'SubGoalPostsById', '$http', 'UserFriendsSearch', 'UserNetworkStatus', 'SubPostComment', 'RequestCollaboraterOrSuppoter', 'DeleteCollaboraterOrSuppoter', '$timeout', 'ngMeta', 'FavouriteGoal', 'UnfavouriteGoal', 'UnCollaboraterOrSuppoter', 'UserPersona', '$interval', 'shareGoal', '$sce'];

    function NewGoalDetailController($scope, $window, $rootScope, $stateParams, previousState, entity, Goal, Dimension, User, s3client, $uibModal, $state, SubGoal, Post, Principal, CustomGoal, Comment, CollaboratorsByGoal, InspiredGoal, SubGoalPostsById, $http, UserFriendsSearch, UserNetworkStatus, SubPostComment, RequestCollaboraterOrSuppoter, DeleteCollaboraterOrSuppoter, $timeout, ngMeta, FavouriteGoal, UnfavouriteGoal, UnCollaboraterOrSuppoter, UserPersona, $interval, shareGoal, $sce) {
        var vm = this;
        $scope.trustSrc = function (src) {
            return $sce.trustAsResourceUrl(src);
        }

        vm.checkStatus = "";
        $scope.collaboratorImage = "/content/images/collaborater-icons/01-become-collaborator.png";
        // $scope.supporterImage = "/content/images/collaborater-icons/01-become-supporter.png";
        $scope.supporterImage = "/content/images/joinbutton.png";
        $scope.collaboratorToolTip = "Request To Collaborate";
        $scope.supporterToolTip = "Join as Supporter";
        $scope.dateOptions = {
            showWeeks: false,
            minDate: new Date(),
            autoclose: true,

        };
        $scope.opened = {};

        $scope.open = function ($event, elementOpened) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened[elementOpened] = !$scope.opened[elementOpened];
        };

        $scope.viewother = false;
        $scope.viewcollaborators = false;
        $scope.viewDescription = false;
        $scope.viewCollection = false;
        $scope.viewTags = false;
        vm.waitingForUpload = false;

        $scope.checktype = function (type) {
            if (type == "jpg" || type == "JPG" || type == "png" || type == "PNG" || type == "JPEG" || type == "jpeg") {
                return "photo";
            }
            else if (type == "avi" || type == "mp4" || type == "mov" || type == "MOV" || type == "mpg" || type == "wmv") {
                return "video";
            }
            else {
                return "other";
            }
        }
        $scope.changeView = function (name) {
            if (name == 'collaborators') {
                $scope.viewother = true;
                $scope.viewcollaborators = true;

                $scope.viewDescription = false;
                $scope.viewCollection = false;
                $scope.viewTags = false;
            }
            else if (name == 'Description') {
                $scope.viewother = true;
                $scope.viewDescription = true;
                $scope.viewcollaborators = false;

                $scope.viewCollection = false;
                $scope.viewTags = false;
            }
            else if (name == 'Collection') {
                $scope.viewother = true;
                $scope.viewCollection = true;
                $scope.viewcollaborators = false;
                $scope.viewDescription = false;

                $scope.viewTags = false;
            }
            else if (name == 'Tags') {
                $scope.viewother = true;
                $scope.viewTags = true;
                $scope.viewcollaborators = false;
                $scope.viewDescription = false;
                $scope.viewCollection = false;

            }
        }


        vm.goal = entity;
        $scope.windowWidth = $window.innerWidth;
        if ($scope.windowWidth < 767) {
            $state.go('goal-detail-mobile', { id: vm.goal.id })
        }
        $(window).resize(function () {
            if ($scope.windowWidth < 767) {
                $state.go('goal-detail-mobile', { id: vm.goal.id })
            }

        });
        // ngMeta.init();
        // ngMeta.setTag('og:url', 'https://www.achieveon.com/#/goal/' + vm.goal.id);
        // ngMeta.setTag('og:type', 'article');
        // ngMeta.setTag('og:title', vm.goal.goalText);
        // ngMeta.setTag('og:description', vm.goal.description);
        vm.today = new Date();
        if (vm.goal.finishTime == '' || vm.goal.finishTime == null || vm.goal.finishTime == undefined) {
            vm.goal.finishTime = vm.today;
        }
        vm.userFriends = [];

        if (vm.goal.imageGuid == null || vm.goal.imageGuid == undefined || vm.goal.imageGuid == '') {
            vm.goal.imageGuid = UUID.generate();
        }
        //Show alert
        vm.showAlert = showAlert;
        Principal.identity().then(function (u) {
            vm.loginUser = u;
            UserNetworkStatus.query({ userId: vm.loginUser.id, status: 'ACCEPTED' }, function (result) {
                // for(var i=0;i<result.length;i++){
                //     result[i].isCollaborator=true;
                //     result[i].isSupporter=false;
                // }
                vm.userFriends = result;
                // console.log("vm.userFriends " + JSON.stringify(vm.userFriends))
                // vm.userPersonas = vm.userFriends;
                if (vm.goal.isNew != true) {
                    getCollaboratorsForGoal();
                }
            })
            vm.checkFavourite();
        }).$promise;
        //vm.getGoalDetails = getGoalDetails;
        vm.goalId = $stateParams.id;


        // alert(JSON.stringify(vm.goal))
        // function getGoalDetails(result) {
        //     vm.goal = result;
        // }
        vm.previousStateFun = previousStateFun;
        vm.previousState = previousState;
        vm.postAttachment = postAttachment;
        vm.chkBox1 = true;
        vm.chkBox2 = false;
        // vm.chkBox3 = false;
        vm.newLineToBR = newLineToBR;
        function newLineToBR(input, name) {
            if (name == 'description') {
                if (!input) return 'Write your goal description';
            }
            else if (name == 'subdescription') {
                if (!input) return 'Write your sub goal description';
            }
            else {
                if (!input) return input;
            }
            var output = input
                //replace possible line breaks.
                .replace(/(\r\n|\r|\n)/g, '<br/>')
                //replace tabs
                .replace(/\t/g, '&nbsp;&nbsp;&nbsp;')
                //replace spaces.
                .replace(/ /g, '&nbsp;');
            return output;
        };
        //get s3 imges
        vm.logoPath = 'https://s3-us-west-1.amazonaws.com/life-edge/goal/' + vm.goal.imageGuid + '/logo/goallogo.jpg';
        //   ngMeta.setTag('og:image', vm.logoPath);
        getGoalCollections()
        function getGoalCollections() {
            var collectionsPromise = s3client.getCollections(vm.goal.imageGuid);
            collectionsPromise.then(function (cols) {
                vm.collections = cols;
                vm.collCount = vm.collections.images.length + vm.collections.videos.length + vm.collections.audios.length + vm.collections.docs.length + vm.collections.links.length;

                if (vm.collections.logo.length != 0) {
                    vm.logoPath = 'https://s3-us-west-1.amazonaws.com/life-edge/goal/' + vm.goal.imageGuid + '/logo/goallogo.jpg';
                }
            }, function (reason) {
                console.log('Failed to update collections :' + reason);
            });
        }

        vm.FileNamePost = '';
        $scope.$watch(function () {
            return vm.FileNamePost;
        }, function (current, original) {
            vm.FileNamePost = current;

        });
        function postAttachment() {
            vm.modalInstance2 = $uibModal.open({
                templateUrl: 'app/entities/goal/goal-detail-post-attachment.html'
                , size: 'sm'
                , scope: $scope
                , controller: ['$scope', '$uibModal', UploadDialogController]
            });
        }

        $scope.opened = {};

        $scope.open = function ($event, elementOpened) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened[elementOpened] = !$scope.opened[elementOpened];
        };
        function UploadDialogController($scope, $uibModalInstance) {
            $scope.image = "";

            vm.processPost = 0;
            vm.clear = clear;
            vm.upload = upload;

            $scope.uploadme = {};
            $scope.uploadme.src = "";

            vm.cancel = clear;

            function clear() {
                vm.modalInstance2.close();

            }

            // function save() {
            //     vm.isSaving = true;
            // }

            function onSaveSuccess(result) {
                $uibModalInstance2.close(result);
                vm.isSaving = false;
            }

            function onSaveError() {
                vm.isSaving = false;
            }

            $scope.updateGoalImage = function () {
                var logoKey = 'goal/' + vm.goal.imageGuid + '/logo/goallogo.jpg';
                var keySource = 'goal/' + $scope.previousImageGUID + '/logo/goallogo.jpg';
                var apiUrl = "/s3/copy?source=" + keySource + "&destination=" + logoKey;
                $http.get(apiUrl)
                    .then(function successCallback(response) {
                        //  $scope.$apply(function () {
                        vm.logoPath = /*"https://s3-us-west-1.amazonaws.com/life-edge/" +*/ logoKey;
                        //$scope.cancel(); //vamc 29-10-17
                        document.getElementById("imgForGoal").src = vm.logoPath;
                        //  })
                        // if (vm.goal.id != null) {
                        //     Goal.update(vm.goal);
                        // }
                        var data = "Image added successfully";
                        vm.showAlert(data)
                        // $timeout(function () {
                        //     $state.reload();
                        // }, 2000)

                    }, function errorCallback(response) {
                        alert("An unexpected error occurred: " + response);
                        $scope.cancel();
                    });
            }

            function upload() {
                if (vm.goal.defaultImage) {
                    $scope.previousImageGUID = vm.goal.imageGuid;
                    vm.goal.imageGuid = UUID.generate();
                }
                vm.waitingForUpload = true;
                var fileChooser = document.getElementById('file-chooser');
                var file = fileChooser.files[0];
                var folder = s3client.getFolderName(file.name, false);
                var fileName = file.name;
                vm.FileNamePost = file.name;

                var filePrefix = "goal/" + vm.goal.imageGuid + "/" + folder + "/" + fileName;
                var s3Prefix = "https://s3-us-west-1.amazonaws.com/life-edge/" + filePrefix;
                var apiUrl = "/s3/geturl?key=" + filePrefix;

                $http.get(apiUrl)
                    .then(function (response) {
                        var url = response.data;
                        console.debug(url);
                        $http.put(url, file, {
                            withCredentials: false,
                            headers: {
                                'Content-Type': 'binary/octet-stream'
                            },
                            transformRequest: angular.identity,
                            eventHandlers: {
                                progress: function (c) {
                                    console.log('Progress -> ' + c);
                                    console.log(c);
                                }
                            },
                            uploadEventHandlers: {
                                progress: function (e) {
                                    vm.processPost = parseInt((e.loaded * 100) / e.total);
                                }
                            },
                        }).success(function (data) {
                            vm.waitingForUpload = false;
                            vm.Upload = false;
                            vm.post.imagePath = s3Prefix;
                            if (vm.goal.defaultImage) {
                                Goal.update(vm.goal);
                                $scope.updateGoalImage();
                            }
                            var data = "Attachment added to the post.";
                            vm.showAlert(data)
                            getGoalCollections()

                            vm.cancel();

                        })
                            .error(function (data) {
                                vm.waitingForUpload = false;
                                vm.Upload = false;
                                vm.cancel();
                            });
                    });

            }
        }

        function previousStateFun() {
            // alert(JSON.stringify(vm.previousState))
            // $state.go(vm.previousState.name)
            // $state.go('goal')

            $state.go(vm.previousState.name, { id: vm.previousState.params.id })
        }
        //  Dimension.query({ id: vm.goal.dimensionId }, function (result) {
        //         vm.dimension = result;
        //     }).$promise;
        vm.dimension = Dimension.get({ id: vm.goal.dimensionId })
        Principal.identity().then(function (u) {
            vm.loginUser = u;
            if (vm.goal.isNew == true) {
                vm.goal.userLoginName = vm.loginUser.firstName + ' ' + vm.loginUser.lastName;
                vm.goal.userId = vm.loginUser.id;
                vm.goal.firstName = vm.loginUser.firstName;
                vm.goal.lastName = vm.loginUser.lastName;
                UserPersona.get({ id: vm.loginUser.id }).$promise.then(function (v) {

                    vm.goal.profileGuid = v.profileGuid;
                });

            }
        });



        vm.goal.collections = s3client.getCollections(vm.goal.imageGuid);
        // vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('leApp:goalUpdate', function (event, result) {
            vm.goal = result;
        });
        vm.getPosts = getPosts;

        function getPosts() {
            CustomGoal.query({ goalId: vm.goalId }, function (result) {
                vm.posts = result;
            }).$promise;

        }

        vm.showImg = showImg;
        function showImg(imgSrc) {
            var modal = document.getElementById('postImgModal');
            var modalImg = document.getElementById("img01");
            modal.style.display = "block";
            modalImg.src = imgSrc;
            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];
            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
                modal.style.display = "none";
            }
        }

        $scope.$on('$destroy', unsubscribe);
        $scope.isCollapsed = false;
        $scope.isCollapsed1 = false;
        $scope.isCollapsed2 = false;
        $scope.isCollapsedTag = false;
        vm.switchUsers = switchUsers;
        //vm.addSubGoals = addSubGoals;
        vm.deleteGoal = deleteGoal;
        vm.modalInstance = "";
        vm.modalInstance123 = "";
        vm.tags = [];
        vm.post = {};
        vm.comment = {};
        vm.openTitle = false;
        vm.openDescription = false;
        vm.allSubGoals = [];
        vm.savePost = savePost;
        vm.saveComment = saveComment;
        var m1 = (vm.goal.metricAmount / vm.goal.metricTarget) * 100;
        vm.mainGoal = isNaN(m1) ? 0 : m1;
        vm.totalOfSubGoals = 0;
        vm.eachGoalPercentage = 0;
        vm.getCollaboratorsForGoal = getCollaboratorsForGoal;
        // vm.addCollaborators = [];
        vm.bothCollaboratorAndSupporter = [];
        // vm.addSupporters = [];
        vm.createSubGoal = createSubGoal;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.datePickerOpenStatus.startTime = false;
        vm.datePickerOpenStatus.finishTime = false;
        vm.datePickerOpenStatus.creatTime = false;
        vm.deleteSubGoal = deleteSubGoal;
        //vm.changeMetricTypes = changeMetricTypes;
        vm.confirmDelete = confirmDelete;
        vm.changeMetricSubTypeForGoal = changeMetricSubTypeForGoal;
        vm.save = save;
        vm.uploadImage = uploadImage;
        vm.clear = clear;
        vm.markFavourite = markFavourite;
        vm.unMarkFavourite = unMarkFavourite;
        function markFavourite() {
            FavouriteGoal.query({ "userId": vm.loginUser.id, "id": vm.goal.id, "firstName": vm.loginUser.firstName });
            $timeout(function () {
                $state.reload();
            }, 600)

        }
        function unMarkFavourite() {
            UnfavouriteGoal.query({ "userId": vm.loginUser.id, "id": vm.goal.id, "firstName": vm.loginUser.firstName });
            $timeout(function () {
                $state.reload();
            }, 600)
        }
        vm.checkFavourite = checkFavourite;

        function checkFavourite() {
            if (vm.goal.favouriteUsers != null) {
                vm.goal.favouriteUsers.forEach(function (user) {
                    if (vm.loginUser.id == user) {
                        vm.goal.favourite = true;
                    }
                })
            }
        }

        Dimension.query(function (result) {
            vm.DIMENSIONS = result;
            if (vm.goal.isNew == true && $stateParams.dimensionId == null) {
                for (var i = 0; i < vm.DIMENSIONS.length; i++) {
                    if (vm.DIMENSIONS[i].name == "DEFAULT") {
                        vm.goal.dimensionId = vm.DIMENSIONS[i].id;
                        vm.goal.dimensionName = vm.DIMENSIONS[i].name;
                    }
                }
            } else if ($stateParams.dimensionId != null) {
                for (var i = 0; i < vm.DIMENSIONS.length; i++) {
                    if (vm.DIMENSIONS[i].id == $stateParams.dimensionId) {
                        vm.goal.dimensionId = vm.DIMENSIONS[i].id;
                        vm.goal.dimensionName = vm.DIMENSIONS[i].name;
                        vm.redirectToDimension = true;
                    }
                }
            }

            $scope.more = {
                Dimensions: vm.DIMENSIONS,
                templateUrl: 'moreInfo.html',
                templatedimensionUrl: 'dimensionInfo.html'
            };
        }).$promise;

        if (vm.goal.isNew == true) {
            vm.goal.metricType = 'Status';
        }
        vm.Shareemail = Shareemail;
        vm.recipientGoal = "";
        vm.contentGoal = "";
        function Shareemail() {
            if (vm.recipientGoal == null || vm.recipientGoal == undefined || vm.recipientGoal == "") {
                var data = "Please enter email address"
                vm.showAlert(data)
            }
            else {
                shareGoal.query({
                    "goalText": vm.goal.goalText,
                    "imageGuid": vm.goal.imageGuid,
                    "description": vm.goal.description,
                    "id": vm.goal.id,
                    "userLogin": vm.loginUser.email,
                    "userLoginName": vm.recipientGoal,
                    "goalShareMessaage": vm.contentGoal,
                    "firstName": vm.loginUser.firstName
                }, onInviteEmailSuccess, onInviteEmailError);
            }
        }
        function onInviteEmailSuccess() {
            var data = "Your Email sent successfully"
            vm.showAlert(data);
            vm.recipientGoal = '';
            vm.contentGoal = '';
        }
        function onInviteEmailError() {
            var data = "Some error occured. Please try again"
            vm.showAlert(data)
        }
        $scope.share = {
            templateUrl: 'shareInfo.html',
            templatedimensionUrl: 'emailInfo.html',
            goalURL: 'https://www.achieveon.com/#/goal/' + vm.goal.id,
            goalimg: vm.logoPath
        };

        $scope.shareDialog = function () {
            // FB.ui(
            //     {
            //         method: 'sharer',
            //         name: 'This is the content of the "name" field.',
            //         link: 'https://www.achieveon.com/#/goal/' + vm.goal.id,
            //         picture: 'https://s3-us-west-1.amazonaws.com/life-edge/goal/ff5ee57d-8d3d-46a9-87c7-05ee5b811f80/logo/goallogo.jpg',
            //         caption: encodeURIComponent(vm.goal.goalText),
            //         description: 'This is the content of the "description" field, below the caption.',
            //         message: ''
            //     });
            var ogUrl = 'https://www.achieveon.com/#/goal/' + vm.goal.id;
            //'http://localhost:8080/#/goal/'+vm.goal.id;
            FB.ui({
                method: 'share_open_graph',
                action_type: 'og.shares',
                action_properties: JSON.stringify({
                    object: {
                        'og:url': ogUrl,
                        'og:href': ogUrl,
                        'og:title': vm.goal.goalText,
                        'og:description': vm.goal.description,
                        'og:image': vm.logoPath
                    }
                })
            },
                // callback
                function (response) {
                    if (response && !response.error_message) {
                        // then get post content
                        //  alert('successfully posted. Status id : '+JSON.stringify(response));
                    } else {
                        // alert('Something went error.');
                    }
                });
        }
        //change Dimension
        vm.changeDimension = changeDimension;
        function changeDimension(dimension) {
            vm.goal.dimensionId = dimension.id;
            vm.goal.dimensionName = dimension.name;
        }
        //COLLABORATORS
        vm.searchCollaborators = searchCollaborators;
        vm.collaboratorSearchQuery = "Search Connections";
        vm.collaboratorResults = [];
        vm.collaboratorResultsShow = false;
        vm.collaborators = [];
        vm.addCollaborators = [];
        vm.addCollaboratorsFun = addCollaboratorsFun;
        vm.removeCollaboratorsFun = removeCollaboratorsFun;
        //vm.userCollaborateSave = userCollaborateSave;

        //SUPPORTERS
        //vm.searchSupporters = searchSupporters;
        vm.supportersSearchQuery = "Search Connections";
        vm.supportersResults = [];
        vm.supporterResultsShow = false;
        vm.addSupporters = [];
        vm.addSupportersFun = addSupportersFun;
        vm.removeSupportersFun = removeSupportersFun;

        vm.allCollaboratorsResult = [];
        //vm.checkCollaborator
        // vm.checkCollaborator=vm.checkCollaborator;



        function searchCollaborators() {
            vm.collaboratorResults = []
            if (vm.collaboratorSearchQuery == "Search Connections" || vm.collaboratorSearchQuery == "") {
                vm.collaboratorResults = []
            }
            else {
                UserFriendsSearch.query({ query: vm.collaboratorSearchQuery }, function (result) {
                    vm.collaboratorResults = result;
                    vm.collaboratorResultsShow = true;
                })
            }
        }
        function addCollaboratorsFun(collaboratorData, userRole) {
            var collaborator = {};
            // collaborator.id="",
            //collaborator.role = 'COLLABORATOR';
            collaborator.role = userRole;
            collaborator.goalId = vm.goalId;
            collaborator.userId = collaboratorData.userId;
            collaborator.userNetworkId = collaboratorData.id;
            collaborator.firstName = collaboratorData.firstName;
            collaborator.lastName = collaboratorData.lastName;
            collaborator.occupation = collaboratorData.occupation;
            collaborator.networkUserId = collaboratorData.networkUserId;
            collaborator.profileGuid = collaboratorData.profileGuid;
            vm.bothCollaboratorAndSupporter.push(collaborator);
            // vm.addCollaborators.push(collaborator); //Adding user as collaborator into collaborator db
        }

        function addSupportersFun(supporterData) {
            // vm.addSupporters.push(supporter)
            var supporter = {};
            // supporter.id="",
            supporter.role = 'SUPPORTER',
                supporter.goalId = vm.goalId;
            supporter.userId = supporterData.userId;
            supporter.userNetworkId = supporterData.id;
            supporter.firstName = supporterData.firstName;
            supporter.lastName = supporterData.lastName;
            supporter.occupation = supporterData.occupation;
            supporter.networkUserId = supporterData.networkUserId;
            supporter.profileGuid = supporterData.profileGuid;
            vm.bothCollaboratorAndSupporter.push(supporter);
            // vm.addSupporters.push(supporter)  //Adding user as supporter into collaborator db
        }
        function removeSupportersFun(index, Id) {
            vm.addSupporters.splice(index, 1);
            for (var i = 0; i < vm.bothCollaboratorAndSupporter.length; i++) {
                if (vm.bothCollaboratorAndSupporter[i].networkUserId == Id) {
                    vm.bothCollaboratorAndSupporter.splice(i, 1);
                    return;
                }
            }

            //DeleteCollaboraterOrSuppoter.query({ id: Id })
        }
        function removeCollaboratorsFun(index, Id) {
            vm.addCollaborators.splice(index, 1);
            for (var i = 0; i < vm.bothCollaboratorAndSupporter.length; i++) {
                if (vm.bothCollaboratorAndSupporter[i].networkUserId == Id) {
                    vm.bothCollaboratorAndSupporter.splice(i, 1);
                    return;
                }
            }
        }
        // upload goal image 12/7/2017
        function uploadImage() {
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/goal/goal-dialog-uploadLogo.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', ModalInstanceController]
            });
        };
        function clear() {
            // $uibModalInstance.dismiss('cancel');
            vm.modalInstance.close();
            vm.subGoal = {};
        }
        function ModalInstanceController($scope, $uibModalInstance) {
            $scope.myImage = '';
            $scope.myCroppedImage = ''; // in this variable you will have dataUrl of cropped area.
            $scope.$watch('myCroppedImage', function (newVal, oldVal) {
                $scope.myCroppedImage = newVal;

            })
            $scope.handleFileSelect = function () {
                var input = document.getElementById('file-chooser');
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        // bind new Image to Component
                        $scope.$apply(function () {
                            $scope.myImage = e.target.result;
                        });
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            //$scope.vm = vm;
            vm.process = 0;
            $scope.image = "";
            $scope.keyPrefix = 'goal/' + vm.goal.imageGuid + '/';

            $scope.save = function () {
                $uibModalInstance.close($scope.theThingIWantToSave);
            };
            $scope.cancel = function () {
                vm.modalInstance.close();
            };

            $scope.uploadS3FileVV = function (isLogo) {   //Changed by vamc 27-10-17

                var fileChooser = document.getElementById('file-chooser');
                var file = fileChooser.files[0];

                if (file) {
                    vm.waitingForUpload = true;
                    //determine extension
                    var folder = s3client.getFolderName(file.name, isLogo);
                    var fileName = file.Name;
                    if (isLogo) fileName = 'goallogo.jpg';
                    var params = {
                        Key: $scope.keyPrefix + folder + '/' + fileName
                        , ContentType: file.type
                        , Body: file
                    };
                    var bucket = s3client.getS3bucket();
                    bucket.upload(params).on('httpUploadProgress', function (evt) {
                        $scope.$apply(function () {
                            vm.process = parseInt((evt.loaded * 100) / evt.total);
                        })
                    }).send(function (err, data) {
                        // results.innerHTML = err ? err : file.name + ' uploaded successfully.';
                        if (isLogo) {
                            vm.logoPath = "https://s3-us-west-1.amazonaws.com/life-edge/" + $scope.keyPrefix + folder + "/goallogo.jpg" + new Date().getTime();

                            document.getElementById("imgForGoal").src = vm.logoPath;
                            vm.clear();

                            getGoalCollections()
                            // if (vm.goal.id != null) {
                            //     Goal.update(vm.goal);
                            // }
                            var data = "Image added successfully";
                            vm.showAlert(data)
                        }
                        else {
                            var collectionsPromise = s3client.getCollections(vm.goal.imageGuid);
                            collectionsPromise.then(function (cols) {
                                vm.collections = cols;
                            }, function (reason) {
                                console.log('Failed to update collections :' + reason);
                            });
                        }
                        $scope.cancel();
                    });
                }
            }
            $scope.blockingObject = { block: true };

            function dataURItoBlob(dataURI) {
                // convert base64/URLEncoded data component to raw binary data held in a string
                var binary = atob(dataURI.split(',')[1]);
                var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

                var array = [];
                for (var i = 0; i < binary.length; i++) {
                    array.push(binary.charCodeAt(i));
                }
                return new Blob([new Uint8Array(array)], { type: mimeString });
            }
            $scope.blockingObject.callback = function (dataURL) {

                $scope.myCroppedImage = dataURL;
            }
            $scope.uploadS3File = function (isLogo) {

                if (vm.goal.defaultImage) {
                    vm.goal.imageGuid = UUID.generate();
                }
                // var fileChooser = document.getElementById('file-chooser');
                // var file = fileChooser.files[0];
                if ($scope.myCroppedImage == '') {
                    $scope.blockingObject.render(function (dataURL) {
                        console.log('via render');
                        console.log(dataURL.length);
                        $scope.myCroppedImage = dataURL;
                    });
                }
                var blob = dataURItoBlob($scope.myCroppedImage)
                var file = new File([blob], "goallogo.jpg");
                var fileName = '';
                if (isLogo) fileName = 'goallogo.jpg';
                if (file) {
                    vm.waitingForUpload = true;
                    var keyPrefix = 'goal/' + vm.goal.imageGuid;
                    //var destination = '';
                    //	vm.registerAccount.logo = d.getTime() + ".jpg";
                    $scope.s3Prefix = "https://s3-us-west-1.amazonaws.com/life-edge/" + keyPrefix + '/logo/' + fileName;
                    var apiUrl = "/s3/geturl?key=goal/" + vm.goal.imageGuid + '/logo/' + fileName;
                    // var apiUrl = "/s3/geturl?key=" + $scope.s3Prefix;
                    console.log(apiUrl);
                    $http.get(apiUrl)
                        .then(function (response) {
                            var url = response.data;
                            console.debug("*&^%$#$#@: " + url);
                            $http.put(url, file, {
                                withCredentials: false,
                                headers: {
                                    'Content-Type': 'binary/octet-stream'
                                }, eventHandlers: {
                                    progress: function (c) {
                                        console.log('Progress -> ' + c);
                                        console.log(c);
                                    }
                                },
                                uploadEventHandlers: {
                                    progress: function (e) {
                                        vm.process = parseInt((e.loaded * 100) / e.total);
                                    }
                                },
                                transformRequest: angular.identity
                            })
                                .success(function (data) {
                                    vm.logoPath = "https://s3-us-west-1.amazonaws.com/life-edge/goal/" + vm.goal.imageGuid + '/logo/goallogo.jpg';
                                    $scope.cancel();  //vamc 29-10-17
                                    document.getElementById("imgForGoal").src = "";
                                    vm.waitingForUpload = false;
                                    $scope.setInterval(); //vamc 29-10-17
                                    getGoalCollections()
                                    // if (vm.goal.id == null) {
                                    //     Goal.update(vm.goal);
                                    // }
                                    if (vm.goal.defaultImage) {
                                        Goal.update(vm.goal);
                                    }
                                    var status = "Image added successfully";
                                    vm.showAlert(status)
                                })
                                .error(function (data) {
                                    vm.waitingForUpload = false;
                                    //vm.Upload = false; //vamc 29-10-17
                                    console.log(data);
                                    $scope.cancel();
                                });
                        });
                }


            }

            $scope.setInterval = function () {
                // $scope.timeInterval = $interval(function () {
                //     vm.logoPath = "https://s3-us-west-1.amazonaws.com/life-edge/goal/" + vm.goal.imageGuid + '/logo/goallogo.jpg';
                //     document.getElementById("imgForGoal").src = vm.logoPath;
                //     console.log(vm.logoPath)
                // }, 2000);
                $timeout(function () {
                    //  $interval.cancel($scope.timeInterval);
                    document.getElementById("imgForGoal").src = vm.logoPath;
                }, 3000)
            }

            $scope.vm.chooseAvatar = function (id, avatarName) {
                if (vm.goal.defaultImage) {
                    vm.goal.imageGuid = UUID.generate();
                }
                //var logoKey = $scope.keyPrefix + s3client.getFolderName(avatarName, true) + '/goallogo.jpg';
                var logoKey = 'goal/' + vm.goal.imageGuid + '/logo/goallogo.jpg';
                var keySource = "avatars/dimensions/" + avatarName;
                var apiUrl = "/s3/copy?source=" + keySource + "&destination=" + logoKey;


                $http.get(apiUrl).then(function successCallback(response) {
                    //  $scope.$apply(function () {
                    vm.logoPath = "https://s3-us-west-1.amazonaws.com/life-edge/" + logoKey;
                    $scope.cancel(); //vamc 29-10-17
                    document.getElementById("imgForGoal").src = vm.logoPath;
                    //  })
                    // if (vm.goal.id != null) {
                    //     Goal.update(vm.goal);
                    // }
                    if (vm.goal.defaultImage) {
                        Goal.update(vm.goal);
                    }
                    var data = "Image added successfully";
                    vm.showAlert(data)
                    // $timeout(function () {
                    //     $state.reload();
                    // }, 2000)

                }, function errorCallback(response) {
                    alert("An unexpected error occurred: " + response);
                    $scope.cancel();
                });
            }
        }

        //Check login user is already a collaborator / supporter

        //Request for Collaborater Or Suppoter
        vm.requestForCollaborater = requestForCollaborater;

        // function requestForCollaborater(role) {
        //     vm.requestCollaborater = {
        //         //"id": "",
        //         "networkUserId": vm.loginUser.id, //my
        //         // "networkRelation": "", //my
        //         "userId": vm.goal.userId, //owner
        //         "firstName": vm.goal.firstName, //owner
        //         "lastName": vm.goal.lastName, //owner
        //         "collaborate": {
        //             "firstName": vm.loginUser.firstName,   //my
        //             "lastName": vm.loginUser.lastName, //my
        //             "occupation": vm.loginUser.occupation, //my
        //             "userId": vm.loginUser.id, //my
        //             "role": role, //my
        //             "goalId": vm.goal.id,
        //             "goalName": vm.goal.goalText
        //         }
        //     }

        //     vm.userFriends.forEach(function (user) {
        //         if (vm.goal.userId == user.networkUserId) {
        //             vm.requestCollaborater = {
        //                 "id": user.id,
        //                 "networkUserId": vm.loginUser.id, //my
        //                 "networkRelation": "FRIEND", //my
        //                 "userId": vm.goal.userId, //owner
        //                 "firstName": vm.loginUser.firstName, //owner //but as per satya we kept login user
        //                 "lastName": vm.loginUser.lastName, //.owner
        //                 "collaborate": {
        //                     "firstName": vm.loginUser.firstName,   //my
        //                     "lastName": vm.loginUser.lastName, //my
        //                     "occupation": vm.loginUser.occupation, //my
        //                     "userId": vm.loginUser.id, //my
        //                     "role": role, //my
        //                     "goalId": vm.goal.id,
        //                     "goalName": vm.goal.goalText
        //                 }
        //             }
        //         }

        //     });
        //     console.log(vm.requestCollaborater)
        //    // RequestCollaboraterOrSuppoter.query(vm.requestCollaborater, onRequestGoalSuccess, onRequestGoalError);


        // }

        function requestForCollaborater(role) {
            //{"isCollaborator":"false","isCollaboratorReqSent":"true","isSupporter":"false","isSupporterReqSent":"false"}
            console.log("vm.checkStatus.isCollaborator: " + vm.checkStatus.isCollaborator)
            console.log("vm.checkStatus.isCollaboratorReqSent: " + vm.checkStatus.isCollaboratorReqSent)
            console.log("vm.checkStatus.isSupporter: " + vm.checkStatus.isSupporter)
            console.log("vm.checkStatus.isSupporterReqSent: " + vm.checkStatus.isSupporterReqSent)

            if (role == 'COLLABORATOR') {
                if (vm.checkStatus.isCollaborator == 'false' && vm.checkStatus.isCollaboratorReqSent == 'false') {
                    vm.requestCollaborater = {
                        //"id": "",
                        "networkUserId": vm.loginUser.id, //my
                        // "networkRelation": "", //my
                        "userId": vm.goal.userId, //owner
                        "firstName": vm.goal.firstName, //owner
                        "lastName": vm.goal.lastName, //owner
                        "collaborate": {
                            "firstName": vm.loginUser.firstName,   //my
                            "lastName": vm.loginUser.lastName, //my
                            "occupation": vm.loginUser.occupation, //my
                            "userId": vm.loginUser.id, //my
                            "role": role, //my
                            "goalId": vm.goal.id,
                            "goalName": vm.goal.goalText
                        }
                    }

                    vm.userFriends.forEach(function (user) {
                        if (vm.goal.userId == user.networkUserId) {
                            vm.requestCollaborater.id = user.id;
                            vm.requestCollaborater.networkRelation = "FRIEND";
                            vm.requestCollaborater.firstName = vm.loginUser.firstName;
                            vm.requestCollaborater.lastName = vm.loginUser.lastName
                        }

                    });
                    console.log("vm.requestCollaborater: " + JSON.stringify(vm.requestCollaborater))
                    RequestCollaboraterOrSuppoter.query(vm.requestCollaborater, onRequestGoalSuccess, onRequestGoalError);
                } else {

                    vm.goal.userCollaborators.forEach(function (collaborator) {
                        if (collaborator.networkUserId == vm.loginUser.id) {
                            UnCollaboraterOrSuppoter.query({ "id": collaborator.id })
                            $state.reload();
                        }
                    })
                }
            }
            if (role == 'SUPPORTER') {
                if (vm.checkStatus.isSupporter == 'false' && vm.checkStatus.isSupporterReqSent == 'false') {
                    vm.requestCollaborater = {
                        //"id": "",
                        "networkUserId": vm.loginUser.id, //my
                        // "networkRelation": "", //my
                        "userId": vm.goal.userId, //owner
                        "firstName": vm.goal.firstName, //owner
                        "lastName": vm.goal.lastName, //owner
                        "collaborate": {
                            "firstName": vm.loginUser.firstName,   //my
                            "lastName": vm.loginUser.lastName, //my
                            "occupation": vm.loginUser.occupation, //my
                            "userId": vm.loginUser.id, //my
                            "role": role, //my
                            "goalId": vm.goal.id,
                            "goalName": vm.goal.goalText
                        }
                    }

                    vm.userFriends.forEach(function (user) {
                        if (vm.goal.userId == user.networkUserId) {
                            vm.requestCollaborater.id = user.id;
                            vm.requestCollaborater.networkRelation = "FRIEND";
                            vm.requestCollaborater.firstName = vm.loginUser.firstName;
                            vm.requestCollaborater.lastName = vm.loginUser.lastName
                        }

                    });
                    console.log("vm.requestCollaborater: " + JSON.stringify(vm.requestCollaborater))
                    RequestCollaboraterOrSuppoter.query(vm.requestCollaborater, onRequestGoalSuccess, onRequestGoalError);
                } else {
                    console.log("Goal_Collaborators 2: " + JSON.stringify(vm.goal.userCollaborators))
                    vm.goal.userCollaborators.forEach(function (supporter) {
                        if (supporter.networkUserId == vm.loginUser.id) {
                            //onCancellationSuccess, onCancellationError
                            UnCollaboraterOrSuppoter.query({ "id": supporter.id })
                            $state.reload();
                        }
                    })
                }
            }


        }
        function onRequestGoalSuccess() {
            var data = "Your request has been sent successfully"
            vm.showAlert(data)
            $timeout(function () {
                $state.reload();
            }, 200)
        }
        function onRequestGoalError() {
            var data = "Some error occured while sending request"
            vm.showAlert(data)
        }

        function onCancellationSuccess() {
            // $state.reload();
        }

        function onCancellationError() {
            var data = "Some error occured while cancelling"
            vm.showAlert(data)
        }

        //To make Request to Un Request or Vice Versa

        // save Inspire
        function save(isInspire) {
            vm.isInspire = isInspire;
            vm.goal.subGoals = vm.allSubGoals;
            vm.goal.userCollaborators = vm.bothCollaboratorAndSupporter;
            if (vm.goal.tags) {
                vm.goal.tags = vm.goal.tags.map(function (tag) {
                    return tag.id;
                })
            }
            vm.goal.creatTime = "";
            if (vm.isInspire == false) {
                if (vm.goal.id) {
                    vm.goal.userLoginName = vm.goal.firstName + ' ' + vm.goal.lastName; // need to modify according to client suggestion
                    Goal.update(vm.goal, onMetricSaveSuccess, onMetricSaveError);
                } else {
                    vm.goal.userLoginName = vm.loginUser.firstName + ' ' + vm.loginUser.lastName;
                    vm.goal.userId = vm.loginUser.id;
                    Goal.save(vm.goal, onGoalSaveSuccess, onGoalSaveError);
                }
            }
            else if (vm.isInspire == true) {
                vm.goal.userLoginName = vm.loginUser.firstName + ' ' + vm.loginUser.lastName;
                vm.goal.userId = vm.loginUser.id;
                vm.goal.parentGoalId = vm.goal.id;
                vm.goal.goalText = "Copy of " + vm.goal.goalText;
                vm.goal.id = "";
                vm.goal.imageGuid = "";
                vm.goal.userCollaborators = [];
                for (var i = 0; i < vm.DIMENSIONS.length; i++) {
                    if (vm.DIMENSIONS[i].name == "DEFAULT") {
                        vm.goal.dimensionId = vm.DIMENSIONS[i].id;
                        vm.goal.dimensionName = vm.DIMENSIONS[i].name;
                    }
                }
                for (var i = 0; i < vm.goal.subGoals.length; i++) {
                    vm.goal.subGoals[i].id = "";
                }
                InspiredGoal.query(vm.goal, onInspiredGoalSuccess, onInspiredGoalError);
            }
        }
        function onInspiredGoalSuccess(result) {
            // alert(JSON.stringify(result))

            //alert("Your Copied Goal was added")
            var data = "Your Copied Goal was added"
            vm.showAlert(data)
            $timeout(function () {
                $scope.windowWidth = $window.innerWidth;
                if ($scope.windowWidth < 767) {
                    $state.go('goal-detail-mobile', { id: result.id })
                }
                else {
                    $state.go('goal-detail', { id: result.id })
                }
            }, 600)

        }
        function onInspiredGoalError() {
            //alert("An error occured while adding Your Copied Goal");
            var data = "An error occured while adding Your Copied Goal"
            vm.showAlert(data)
        }
        function onGoalSaveSuccess(result) {
            var data = "Your goal saved successfully "
            vm.showAlert(data)
            $timeout(function () {
                if (vm.redirectToDimension) {
                    $state.go('dimension.edit', { id: $stateParams.dimensionId })
                }
                else if (vm.goal.isNew == true) {
                    $scope.windowWidth = $window.innerWidth;
                    if ($scope.windowWidth < 767) {
                        $state.go('goal-detail-mobile', { id: result.id })
                    }
                    else {
                        $state.go('goal-detail', { id: result.id })
                    }
                }  //client's new changes 20/11/2017
                else if ($stateParams.isInspire == "true") {
                    $state.go('goal')
                } else {
                    $state.reload()
                }
            }, 600)


        }
        function onGoalSaveError() {
            vm.isSaving = false;
        }

        if (vm.goal.isNew != true) {
            getPosts();
        }
        function getCollaboratorsForGoal() {
            CollaboratorsByGoal.query({ goalId: vm.goalId }, function (result) {
                // vm.bothCollaboratorAndSupporter = result;
                result.forEach(function (user) {
                    if (user.status != "REJECTED") {
                        vm.bothCollaboratorAndSupporter.push(user);
                    }
                })
                checkInLoading()
                //  console.log("collabRes" + JSON.stringify(result))
            }).$promise;

        }

        function checkInLoading() {
            vm.checkStatus = $scope.checkAccepted({ networkUserId: vm.loginUser.id })

            //console.log("vm.checkStatus: " + JSON.stringify(vm.checkStatus))
            for (var i = 0; i < vm.bothCollaboratorAndSupporter.length; i++) {
                vm.bothCollaboratorAndSupporter[i].supporterRequestSent = 'false';
                vm.bothCollaboratorAndSupporter[i].collaboratorRequestSent = 'false';
                vm.bothCollaboratorAndSupporter[i].supporterAccept = 'false';
                vm.bothCollaboratorAndSupporter[i].collaboratorAccept = 'false';
                if (vm.bothCollaboratorAndSupporter[i].role == 'SUPPORTER') {
                    vm.bothCollaboratorAndSupporter[i].supporterRequestSent = 'true';
                    if (vm.bothCollaboratorAndSupporter[i].status == "ACCEPTED") {
                        vm.bothCollaboratorAndSupporter[i].supporterAccept = 'true';
                    }
                    vm.addSupporters.push(vm.bothCollaboratorAndSupporter[i]);
                }
                if (vm.bothCollaboratorAndSupporter[i].role == 'COLLABORATOR') {
                    vm.bothCollaboratorAndSupporter[i].collaboratorRequestSent = 'true';
                    if (vm.bothCollaboratorAndSupporter[i].status == "ACCEPTED") {
                        vm.bothCollaboratorAndSupporter[i].collaboratorAccept = 'true';
                    }
                    vm.addCollaborators.push(vm.bothCollaboratorAndSupporter[i]);
                }
            }
            vm.userPersonas = vm.addCollaborators;
        }
        //This following function is for showing friends supporters and collaborators. As per client suggestion not using this keeping as backup// 27-7-2017
        function checkInLoadingBackup() {
            vm.checkStatus = $scope.checkCollaborator({ networkUserId: vm.loginUser.id })
            for (var i = 0; i < vm.userFriends.length; i++) {
                console.log("LENGTH" + JSON.stringify(vm.userFriends.length))
                vm.userFriends[i].supporterRequestSent = 'false';
                vm.userFriends[i].collaboratorRequestSent = 'false';
                vm.userFriends[i].supporterAccept = 'false';
                vm.userFriends[i].collaboratorAccept = 'false';
                vm.userFriends[i].ignore = 'false';
                for (var j = 0; j < vm.bothCollaboratorAndSupporter.length; j++) {
                    var exist = false;
                    if (vm.bothCollaboratorAndSupporter[j].networkUserId == vm.userFriends[i].networkUserId) {
                        exist = true;
                    }
                    if (exist) {
                        if (vm.bothCollaboratorAndSupporter[j].role == 'SUPPORTER') {
                            vm.userFriends[i].ignore = 'true';
                            vm.userFriends[i].supporterRequestSent = 'true';
                            if (vm.bothCollaboratorAndSupporter[j].status == "ACCEPTED") {
                                vm.userFriends[i].supporterAccept = 'true';
                                vm.addSupporters.push(vm.bothCollaboratorAndSupporter[j]);
                            }
                        }
                        if (vm.bothCollaboratorAndSupporter[j].role == 'COLLABORATOR') {
                            vm.userFriends[i].ignore = 'true';
                            vm.userFriends[i].collaboratorRequestSent = 'true';
                            if (vm.bothCollaboratorAndSupporter[j].status == "ACCEPTED") {
                                vm.userFriends[i].collaboratorAccept = 'true';
                                vm.addCollaborators.push(vm.bothCollaboratorAndSupporter[j]);
                            }
                        }
                    }

                }
            }
            vm.userPersonas = vm.userFriends;
        }


        //Function to check whether passed user is collaborator or supporter
        $scope.checkCollaborator = function (Collaborator) {
            var userStatus = {};
            userStatus.isCollaborator = 'false';
            userStatus.isSupporter = 'false';
            if (vm.bothCollaboratorAndSupporter.length > 0) {
                for (var i = 0; i < vm.bothCollaboratorAndSupporter.length; i++) {
                    console.log("i: " + i)
                    var found = false;
                    if (vm.bothCollaboratorAndSupporter[i].networkUserId == Collaborator.networkUserId) {
                        found = true;
                    }
                    if (found) {
                        if (vm.bothCollaboratorAndSupporter[i].role == 'COLLABORATOR') {
                            userStatus.isCollaborator = 'true';
                            userStatus.isSupporter = 'false';
                            // vm.addSupporters.push(vm.bothCollaboratorAndSupporter[i]);
                            return userStatus;
                        }
                        if (vm.bothCollaboratorAndSupporter[i].role == 'SUPPORTER') {
                            userStatus.isSupporter = 'true';
                            userStatus.isCollaborator = 'false';
                            //vm.addCollaborators.push(vm.bothCollaboratorAndSupporter[i]);
                            return userStatus;
                        }
                    }
                    if (!found) {
                        if (i == vm.bothCollaboratorAndSupporter.length - 1) {
                            return userStatus;
                        }
                    }
                }
            } else {
                //  console.log("2222222222222222")
                return userStatus;
            }
        }

        // $scope.collaboratorImage = "/content/images/collaborater-icons/01-become-collaborator.png";
        // $scope.supporterImage = "/content/images/collaborater-icons/01-become-supporter.png";
        // $scope.collaboratorToolTip = "Request To Collaborate";
        // $scope.supporterToolTip = "Request To Support";
        $scope.checkAccepted = function (Collaborator) {
            var userStatus = {};
            userStatus.isCollaborator = 'false';
            userStatus.isCollaboratorReqSent = 'false';
            userStatus.isSupporter = 'false';
            userStatus.isSupporterReqSent = 'false';
            if (vm.bothCollaboratorAndSupporter.length > 0) {
                for (var i = 0; i < vm.bothCollaboratorAndSupporter.length; i++) {
                    var found = false;
                    if (vm.bothCollaboratorAndSupporter[i].networkUserId == Collaborator.networkUserId) {
                        found = true;
                    }
                    if (found) {
                        if (vm.bothCollaboratorAndSupporter[i].role == 'COLLABORATOR') {
                            userStatus.isCollaboratorReqSent = 'true';
                            $scope.collaboratorToolTip = "Request Pending";
                            if (vm.bothCollaboratorAndSupporter[i].status == 'ACCEPTED') {
                                $scope.collaboratorToolTip = "Request To UnCollaborate";
                                $scope.supporterToolTip = "You are already a collaborator";
                                $scope.collaboratorImage = "/content/images/collaborater-icons/02-collaborater-successful.png";
                                userStatus.isCollaborator = 'true';
                                userStatus.isSupporter = 'false';
                            }
                            return userStatus;
                        }
                        if (vm.bothCollaboratorAndSupporter[i].role == 'SUPPORTER') {
                            userStatus.isSupporterReqSent = 'true';
                            $scope.supporterToolTip = "Request Pending";
                            if (vm.bothCollaboratorAndSupporter[i].status == 'ACCEPTED') {
                                $scope.supporterToolTip = "Un-Join Goal";
                                $scope.collaboratorToolTip = "You are already a supporter";
                                $scope.supporterImage = "/content/images/collaborater-icons/02-supporter-successful.png";
                                //  $scope.supporterImage = "/content/images/unjoinbutton.png";
                                userStatus.isSupporter = 'true';
                                userStatus.isCollaborator = 'false';
                            }
                            return userStatus;
                        }
                    }
                    if (!found) {
                        if (i == vm.bothCollaboratorAndSupporter.length - 1) {
                            return userStatus;
                        }
                    }
                }
            } else {
                return userStatus;
            }
        }

        vm.changeUserRelation = changeUserRelation;

        //Switching between Collaborator and Supporter
        function changeUserRelation(user, role, index, status) {
            if (vm.bothCollaboratorAndSupporter.length > 0) {
                var found = false;
                for (var i = 0; i < vm.bothCollaboratorAndSupporter.length; i++) {
                    if (vm.bothCollaboratorAndSupporter[i].networkUserId == user.networkUserId) {
                        console.log("changeUserRelation: equal")
                        found = true;
                    }
                    if (found) {
                        vm.bothCollaboratorAndSupporter[i].role = role;
                        if (status == 'remove') {
                            vm.bothCollaboratorAndSupporter.splice(i, 1)
                        }
                    }
                }
                if (!found) {
                    addCollaboratorsFun(user, role);
                }

            } else {
                console.log("No collaborators")
                addCollaboratorsFun(user, role)
            }

        }


        function switchUsers(type) {
            if (type == 'collaborators') {
                vm.chkBox1 = true;
                vm.chkBox2 = false;
                vm.chkBox3 = false;
                vm.userPersonas = vm.addCollaborators;
            } else if (type == 'supporters') {
                vm.chkBox2 = true;
                vm.chkBox1 = false;
                vm.chkBox3 = false;
                vm.userPersonas = vm.addSupporters;
            } else {
                vm.chkBox3 = true;
                vm.chkBox2 = false;
                vm.chkBox1 = false;
                vm.userPersonas = vm.userFriends;
            }
        }
        vm.show = show;
        function show(col) {

            $scope.dynamicPopover = {
                id: col.networkUserId,
                name: col.firstName + " " + col.lastName,
                profileGuid: col.profileGuid,
                templateUrl: 'collaboratorInfo.html',
                occupation: col.occupation,
                role: col.role
            };
        }
        vm.showOwner = showOwner;
        function showOwner(col) {

            $scope.dynamicPopover = {
                id: vm.goal.userId,
                name: vm.goal.firstName + " " + vm.goal.lastName,
                templateUrl: 'collaboratorInfo.html',
                profileGuid: vm.goal.profileGuid,
                occupation: vm.goal.occupation
            };
        }
        $scope.search = {

            templateUrl: 'collaboratorSearch.html',

        };




        //Open delete popup modal for sub goal
        function deleteSubGoal(subgoal, index) {
            $scope.deleteSubgoal = subgoal;
            $scope.deleteSubgoal.index = index;
            $scope.deleteSubgoal.mainGoal = vm.goal.goalText;
            $scope.deleteSubgoal.editPercentage = $scope.deleteSubgoal.goalpercent;
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/goal/sub-goal-delete.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', ModalInstanceController]
            });
        }

        //Confirm delete sub goal
        function confirmDelete(value) {
            vm.eachGoalPercentage -= value.editPercentage;
            if (vm.allSubGoals[value.index].id != null) {
                SubGoal.delete({ id: vm.allSubGoals[value.index].id }, function () { });
            }
            vm.allSubGoals.splice(value.index, 1);
            vm.totalOfSubGoals = parseInt(vm.eachGoalPercentage / vm.allSubGoals.length);
            //  break;
            // }
            // }
            vm.modalInstance.close();
        }
        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }

        vm.selectColors = ['#ff4852', '#63e762', '#ff8c3f'];


        vm.metricTypes = [
            {
                "name": "Currency",
                "values": ["USD", "Euro"]
            },
            {
                "name": "Weight",
                "values": ["lbs", "kgs", "oz"]
            },
            {
                "name": "Distance",
                "values": ["mm", "m", "km", "mile", "feet", "yard"]
            },
            {
                "name": "Speed",
                "values": ["miles/hr", "km/hr"]
            },
            {
                "name": "Time",
                "values": ["sec", "hrs", "days", "months", "years"]
            },
            {
                "name": "Frequency",
                "values": ["daily", "weekly", "monthly", "yearly"]
            },
            {
                "name": "Count",
                "values": ["reps", "sets", "laps"]
            },
            {
                "name": "Status",
                "values": ["% Complete"]
            },
            {
                "name": "Others",
                "values": [""]
            }
        ];

        function changeMetricSubTypeForGoal(type) {
            for (var i = 0; i < vm.metricTypes.length; i++) {

                if (vm.metricTypes[i].name == vm.goal.metricType) {
                    vm.subMetricValuesForGoal = vm.metricTypes[i].values;
                    if (vm.metricTypes[i].name != "Others") {
                        vm.goal.metricSubType = vm.metricTypes[i].values[0]
                    }
                    if (vm.metricTypes[i].name == "Status") {
                        vm.goal.metricTarget = 100
                    }
                }
            }
        }
        vm.generateColor = generateColor;
        function generateColor() {
            var goal = vm.subGoal;
            goal.metricAmount = parseInt(goal.metricAmount)
            goal.metricTarget = parseInt(goal.metricTarget)
            var m1 = (goal.metricAmount / goal.metricTarget * 100);
            goal.goalpercent = isNaN(m1) ? 0 : m1;
            var color = "";
            if (goal.goalpercent <= 25) {
                color = "#ff4852";
            }
            else if (goal.goalpercent <= 50) {
                //color = "#F19447";
                color = "#F59547";
            }
            else if (goal.goalpercent <= 75) {
                // color = "#F2E375";
                color = "#F3DC58"
            }
            else if (goal.goalpercent <= 100) {
                color = "#87E35A";
            }
            return color;
        }
        //Open popup modal to edit sub goal
        function createSubGoal(val1, val2, val3) {
            if (vm.allSubGoals.length >= 6 && val2 == 'no') {
                var data = "max 6 sub goal";
                vm.showAlert(data)
            }
            else {
                vm.subGoal = {};
                vm.showLabel = 'no';
                vm.subGoal.metricType = 'Status'
                if (val2 == 'yes') {
                    vm.showLabel = 'yes';
                    // for (var i = 0; i < vm.allSubGoals.length; i++) {
                    // vm.allSubGoals[val3].index = val3;
                    // if (vm.allSubGoals[i].index == val3) {
                    vm.subGoal = angular.copy(vm.allSubGoals[val3]);
                    vm.subGoal.index = val3;
                    vm.subGoal.editPercentage = vm.subGoal.goalpercent;
                    // }
                    // }
                }

                vm.changeMetricSubType = changeMetricSubType;
                function changeMetricSubType() {
                    for (var i = 0; i < vm.metricTypes.length; i++) {
                        if (vm.metricTypes[i].name == vm.subGoal.metricType) {
                            vm.subMetricValues = vm.metricTypes[i].values;
                            if (vm.metricTypes[i].name != "Others") {
                                vm.subGoal.metricSubType = vm.metricTypes[i].values[0]
                            }
                            if (vm.metricTypes[i].name == "Status") {
                                vm.subGoal.metricTarget = 100
                            }
                        }
                    }
                }
                vm.modalInstance = $uibModal.open({
                    templateUrl: 'app/entities/goal/goal-sub-goals.html'
                    , size: 'md'
                    , scope: $scope
                    , controller: ['$scope', '$uibModal', 'SubGoalPostsById', 'SubGoalPost', SubGoalInstanceController]
                });
            }
        }


        vm.close = close;
        function close() {
            vm.modalInstance.close();
        }

        function SubGoalInstanceController($scope, $uibModalInstance, SubGoalPostsById, SubGoalPost) {
            vm.today = new Date();
            if (vm.subGoal.finishTime == '' || vm.subGoal.finishTime == null || vm.subGoal.finishTime == undefined) {
                vm.subGoal.finishTime = vm.today;
            }
            vm.newLineToBR = newLineToBR;
            function newLineToBR(input, name) {
                if (name == 'description') {
                    if (!input) return 'Write your goal description';
                }
                else if (name == 'subdescription') {
                    if (!input) return 'Write your sub goal description';
                }
                else {
                    if (!input) return input;
                }
                var output = input
                    //replace possible line breaks.
                    .replace(/(\r\n|\r|\n)/g, '<br/>')
                    //replace tabs
                    .replace(/\t/g, '&nbsp;&nbsp;&nbsp;')
                    //replace spaces.
                    .replace(/ /g, '&nbsp;');
                return output;
            };
            $scope.$watchCollection('[vm.subGoal.goalpercent, vm.subGoal.metricAmount, vm.subGoal.metricTarget]', function (newVal, oldVal) {

                if (newVal) {
                    if (vm.subGoal.metricTarget && vm.subGoal.metricAmount > vm.subGoal.metricTarget) {
                        vm.subGoal.metricAmount = parseInt(vm.subGoal.metricTarget);
                    } else {
                        var m4 = parseInt((vm.subGoal.metricAmount / vm.subGoal.metricTarget) * 100) || 0;
                        vm.subGoal.goalpercent = isNaN(m4) ? 0 : m4;
                        if (vm.subGoal.goalpercent) {
                            if (vm.subgoalmetricStatus == 'yes') {
                                vm.subgoalmetricStatus = 'no'
                            } else {
                                vm.subgoalmetricStatus = 'yes'
                            }
                        }
                        else {
                            if (vm.subgoalmetricStatus == 'yes') {
                                vm.subgoalmetricStatus = 'no'
                            } else {
                                vm.subgoalmetricStatus = 'yes'
                            }
                        }

                    }
                }
            });

            vm.editSubGoal = editSubGoal;
            vm.subGoal.post = {};
            $scope.cancel = function () {
                vm.modalInstance.close();

            };
            vm.deletesubpost = deletesubpost;
            function deletesubpost(id) {

                SubGoalPost.delete({ id: id },
                    function () {
                        getSubPosts();
                    });
            }
            vm.getSubPosts = getSubPosts;
            getSubPosts();
            function getSubPosts() {
                SubGoalPostsById.query({ id: vm.subGoal.id }, function (result) {
                    vm.subGoal.posts = result;

                }).$promise;
            }
            $scope.save = function () {
                $uibModalInstance.close($scope.theThingIWantToSave);

            };
            $scope.opened = {};

            $scope.open = function ($event, elementOpened) {
                $event.preventDefault();
                $event.stopPropagation();
                alert("ffas");
                $scope.opened[elementOpened] = !$scope.opened[elementOpened];
            };


            //Update sub goal with new data
            function editSubGoal(subGoal) {
                if (subGoal.id != null) {
                    vm.eachGoalPercentage -= subGoal.editPercentage;
                    var val = parseInt((subGoal.metricAmount / subGoal.metricTarget) * 100);
                    subGoal.goalpercent = isNaN(val) ? 0 : val;
                    vm.eachGoalPercentage += subGoal.goalpercent;
                    vm.allSubGoals[subGoal.index] = subGoal;
                    var val2 = parseInt(vm.eachGoalPercentage / vm.allSubGoals.length);
                    // vm.totalOfSubGoals = isNaN(val2) ? 0 : val2;
                    // vm.subGoal = {}; (client chnage 20/11/2017)
                    //vm.modalInstance.close();
                    // SubGoal.update(subGoal, onSaveSuccess, onSaveError);
                    vm.goal.subGoals = vm.allSubGoals;
                    // vm.modalInstance.close(); (client chnage 20/11/2017)
                    Goal.update(vm.goal, onSaveSuccess, onMetricSaveError);
                }
                else {

                    subGoal.id = null;

                    if (subGoal.metricAmount == undefined || subGoal.metricTarget == undefined) {
                        subGoal.metricAmount = 0;
                        subGoal.metricTarget = 0;
                        subGoal.goalpercent = 0;
                    } else {
                        var val = parseInt((subGoal.metricAmount / subGoal.metricTarget) * 100);
                        subGoal.goalpercent = isNaN(val) ? 0 : val;
                    }
                    subGoal.color = vm.selectColors[1];
                    subGoal.startTime = new Date();
                    subGoal.goalId = vm.goalId;
                    vm.eachGoalPercentage += subGoal.goalpercent;
                    if (subGoal.index) {                                //5-8-17 //vmc
                        vm.allSubGoals[subGoal.index] = subGoal;
                    } else {
                        vm.allSubGoals.push(subGoal);
                    }

                    // vm.totalOfSubGoals = parseInt(vm.eachGoalPercentage / vm.allSubGoals.length);
                    vm.subGoalMetricType = vm.metricTypes[0];
                    vm.subGoalMetricSubType = vm.subGoalMetricType.values[0];
                    vm.subGoalMetricSubTypeOther = '';
                    // vm.modalInstance.close();
                    // SubGoal.save(subGoal, onSaveSuccess, onSaveError); //commented on 5-8-17 //To check subgoal duplicates
                    vm.goal.subGoals = vm.allSubGoals;
                    // vm.modalInstance.close();
                    Goal.update(vm.goal, onSaveSuccess, onMetricSaveError);
                }
            }
            function onSaveSuccess(result) {
                // vm.subGoal = {};
                $scope.$emit('leApp:subGoalUpdate', result);
                // vm.modalInstance.close();
                var data = "Your sub goal saved successfully" //(client chnage 20/11/2017)
                vm.showSubAlert(data) //(client chnage 20/11/2017)
                vm.allSubGoals.forEach(function (user) {
                    result.subGoals.forEach(function (result) {
                        if (user.id) {
                            if (result.id == null || result.id == undefined) {
                                vm.subGoal = result;
                            }
                        }
                    })
                })
                // $state.reload(); (client chnage 20/11/2017)
                // vm.subGoal = result;
                // getSubPosts();
            }

            function onSaveError() {

            }

            vm.saveSubPost = saveSubPost;
            vm.saveSubComment = saveSubComment;
            function saveSubPost(type) {
                vm.isSaving = true;
                if (type == 'update') {
                    SubGoalPost.update(vm.subGoal.post, onPostSubSaveSuccess, onPostSubSaveError);
                } else {
                    //vm.subGoal.post.creatTime = new Date();
                    vm.subGoal.post.subGoalId = vm.subGoal.id;
                    vm.subGoal.post.userId = vm.loginUser.id;
                    vm.subGoal.post.firstName = vm.loginUser.firstName;
                    // alert("save: " + JSON.stringify(vm.post))
                    SubGoalPost.save(vm.subGoal.post, onPostSubSaveSuccess, onPostSubSaveError);
                }
            }
            function onPostSubSaveSuccess(result) {
                vm.subGoal.post = {};
                getSubPosts();
            }

            function onPostSubSaveError() {
                //vm.isSaving = false;
            }
            function saveSubComment(type, postId, comment) {
                if (type == 'update') {
                    if (vm.comment.comment !== null) {
                        vm.comment.id = comment.id;
                        //  vm.comment.creatTime = new Date();
                        vm.comment.comment = comment.comment;
                        vm.comment.subGoalPostId = postId;
                        vm.comment.userId = vm.loginUser.id;
                        // alert("save: " + JSON.stringify(vm.comment))
                        SubPostComment.update(vm.comment, onCommentSubSaveSuccess, onCommentSubSaveError);
                    }
                } else {
                    if (vm.comment.comment !== null) {
                        // vm.comment.creatTime = new Date();
                        vm.comment.comment = comment;
                        vm.comment.subGoalPostId = postId;
                        vm.comment.userId = vm.loginUser.id;
                        vm.comment.firstName = vm.loginUser.firstName;
                        // alert("save: " + JSON.stringify(vm.comment))
                        SubPostComment.save(vm.comment, onCommentSubSaveSuccess, onCommentSubSaveError);
                    }
                }
            }
            function onCommentSubSaveSuccess(result) {
                vm.isSaving = false;
                vm.comment = {};
                getSubPosts();
            }

            function onCommentSubSaveError() {
                vm.isSaving = false;
            }
            vm.deleteSubComment = deleteSubComment;
            function deleteSubComment(id) {
                SubPostComment.delete({ id: id },
                    function () {
                        //$uibModalInstance.close(true);
                        getSubPosts();
                    });
            }
            vm.deletesubGoal = deletesubGoal;
            function deletesubGoal(subGoal) {
                //alert(JSON.stringify(subGoal))
                // vm.dimensionName = subGoal.dimensionName;
                // vm.goal.goalText = subGoal.subgoalText;

                // // vm.dimensionId = vm.dimension.id;
                // // vm.modalInstance = $uibModal.open({
                // //     templateUrl: 'app/entities/goal/goal-delete-dialog.html'
                // //     , size: 'md'
                // //     , scope: $scope
                // //     , controller: ['$scope', '$uibModalInstance', DeleteController]
                // // });
                // // }
                // // function DeleteController($scope, $uibModalInstance) {
                // // vm.clear = clear;
                // // vm.confirmDelete = confirmDelete;


                // function clear() {
                //     $uibModalInstance.dismiss('cancel');
                // }
                // function confirmDelete() {

                //     SubGoal.delete({ id: vm.subGoal.id },
                //         function () {
                //             $uibModalInstance.close(true);
                //             //$state.go('goal-detail', { id: vm.maingoalId })
                //             $state.reload();
                //             // $state.go(vm.previousState.name, { id: vm.previousState.params.id })
                //         });
                // }
                SubGoal.delete({ id: subGoal.id },
                    function () {
                        vm.modalInstance.close();
                        //$state.go('goal-detail', { id: vm.maingoalId })
                        $state.reload();
                        // $state.go(vm.previousState.name, { id: vm.previousState.params.id })
                    });
            }

        }
        //uploadCollections
        vm.uploadCollections = uploadCollections;
        function uploadCollections() {
            vm.modalInstance = $uibModal.open({
                // templateUrl: 'app/entities/goal/goal-dialog-upload.html'
                templateUrl: 'app/entities/goal/download-collections.html'
                , size: 'sm'
                , scope: $scope
                , controller: ['$scope', '$uibModal', ModalInstanceController]
            });
        };
        $scope.$watchCollection('[vm.goal.metricTarget, vm.goal.metricAmount, vm.totalOfSubGoals]', function (newVal, oldVal) {

            if (newVal) {
                // if (parseInt(vm.goal.metricTarget) && parseInt(vm.goal.metricAmount) > parseInt(vm.goal.metricTarget)) {
                if (parseInt(vm.goal.metricAmount) > parseInt(vm.goal.metricTarget)) {
                    //alert('Current value should be less than the Target value')
                    // vm.goal.metricAmount = 0;
                    vm.goal.metricAmount = parseInt(vm.goal.metricTarget);
                } else {
                    var m4 = parseInt((vm.goal.metricAmount / vm.goal.metricTarget) * 100) || 0;
                    vm.mainGoal = isNaN(m4) ? 0 : m4;
                    if (vm.mainGoal) {
                        if (vm.metricStatus == 'yes') {
                            vm.metricStatus = 'no'
                        } else {
                            vm.metricStatus = 'yes'
                        }
                    }
                    else {
                        if (vm.metricStatus == 'yes') {
                            vm.metricStatus = 'no'
                        } else {
                            vm.metricStatus = 'yes'
                        }
                    }
                }
            }
        });

        if (vm.goal.subGoals && vm.goal.subGoals.length > 0) {
            console.log("vm.goal.subGoals: " + JSON.stringify(vm.goal.subGoals))
            vm.goal.subGoals.map(function (subGoal) {
                var m1 = parseInt((subGoal.metricAmount / subGoal.metricTarget) * 100);
                subGoal.goalpercent = isNaN(m1) ? 0 : m1;
                vm.eachGoalPercentage += subGoal.goalpercent;
                if (subGoal.goalpercent <= 25) {
                    subGoal.color = "#ff4852";
                }
                else if (subGoal.goalpercent <= 50) {
                    subGoal.color = "#F19447";
                }
                else if (subGoal.goalpercent <= 75) {
                    subGoal.color = "#F2E375";
                }
                else if (subGoal.goalpercent <= 100) {
                    subGoal.color = "#87E35A";
                }
                vm.allSubGoals.push(subGoal);
            })

            vm.totalOfSubGoals = parseInt(vm.eachGoalPercentage / vm.allSubGoals.length)
        }
        function deleteGoal(goalId) {
            vm.dimensionName = vm.dimension.name;
            vm.dimensionId = vm.dimension.id;
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/goal/goal-delete-dialog.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModalInstance', DeleteController]
            });
        }
        function DeleteController($scope, $uibModalInstance) {
            vm.clear = clear;
            vm.confirmDelete = confirmDelete;

            function clear() {
                $uibModalInstance.dismiss('cancel');
            }
            function confirmDelete(id) {
                Goal.delete({ id: id },
                    function () {
                        $uibModalInstance.close(true);
                        $state.go('goal')
                    });
            }
        }


        function showAlert(message) {
            vm.message = message;
            vm.modalInstance123 = $uibModal.open({
                templateUrl: 'app/entities/goal/alert-modal.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModalInstance', ShowAlertController]
            });
        }
        function ShowAlertController($scope, $uibModalInstance) {
            $timeout(function () {
                vm.modalInstance123.close();
            }, 1300)
        }
        vm.showSubAlert = showSubAlert;
        function showSubAlert(message) {
            vm.message = message;
            vm.modalInstance123 = $uibModal.open({
                templateUrl: 'app/entities/goal/alertsub-modal.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModalInstance', showSubAlertController]
            });
        }
        function showSubAlertController($scope, $uibModalInstance) {
            $timeout(function () {
                vm.modalInstance123.close();
            }, 1300)
        }

        function savePost(type) {

            vm.isSaving = true;
            if (type == 'update') {
                Post.update(vm.post, onPostSaveSuccess, onPostSaveError);
            } else {
                // vm.post.creatTime = new Date();
                vm.post.goalId = vm.goal.id;
                vm.post.userId = vm.loginUser.id;
                vm.post.firstName = vm.loginUser.firstName;
                // alert("save: " + JSON.stringify(vm.post))
                Post.save(vm.post, onPostSaveSuccess, onPostSaveError);
            }
        }
        function onPostSaveSuccess(result) {
            $scope.$emit('leApp:postUpdate', result);
            //$uibModalInstance.close(result);
            // vm.isSaving = false;
            vm.post = {};
            var data = "Post saved successfully"
            vm.showAlert(data)
            //  vm.goal= Goal.get({ id: $stateParams.id }).$promise;
            vm.FileNamePost = "";
            //vm.openTitle = false;
            // vm.openDescription = false;
            getPosts();
        }

        function onPostSaveError() {
            //vm.isSaving = false;
        }
        vm.deletepost = deletepost;
        function deletepost(id) {

            Post.delete({ id: id },
                function () {
                    getPosts();
                });
        }
        function saveComment(type, postId, comment) {
            if (type == 'update') {
                if (vm.comment.comment !== null) {
                    //vm.comment.creatTime = new Date();
                    vm.comment.id = comment.id;
                    vm.comment.comment = comment.comment;
                    vm.comment.postId = postId;
                    vm.comment.userId = vm.loginUser.id;
                    Comment.update(vm.comment, onCommentSaveSuccess, onCommentSaveError);
                }
            } else {
                if (vm.comment.comment !== null) {
                    // vm.comment.creatTime = new Date();
                    vm.comment.comment = comment;
                    vm.comment.postId = postId;
                    vm.comment.userId = vm.loginUser.id;
                    vm.comment.firstName = vm.loginUser.firstName;
                    // alert("save: " + JSON.stringify(vm.comment))
                    Comment.save(vm.comment, onCommentSaveSuccess, onCommentSaveError);
                }
            }
        }
        function onCommentSaveSuccess(result) {
            $scope.$emit('leApp:commentUpdate', result);
            // $uibModalInstance.close(result);
            vm.isSaving = false;
            vm.comment = {};
            getPosts();
        }

        function onCommentSaveError() {
            vm.isSaving = false;
        }
        vm.deleteComment = deleteComment;
        function deleteComment(id) {

            Comment.delete({ id: id },
                function () {
                    //$uibModalInstance.close(true);
                    getPosts();
                });
        }
        vm.metricStatus = 'no';
        vm.subgoalmetricStatus = 'no';

        vm.saveMetric = saveMetric;
        function saveMetric() {
            Goal.update(vm.goal, onMetricSaveSuccess, onMetricSaveError);
        }
        function onMetricSaveSuccess(result) {
            //  $scope.$emit('leApp:goalUpdate', result);
            vm.isSaving = false;
            var data = "Your goal updated successfully"
            vm.showAlert(data)
            //  vm.goal= Goal.get({ id: $stateParams.id }).$promise;
            $timeout(function () {
                $state.reload()
            }, 600)

            //vm.getGoalDetails(result)
        }
        function onSubGoalAddSaveSuccess(result) {
            //  $scope.$emit('leApp:goalUpdate', result);
            vm.isSaving = false;
            var data = "Your subgoal saved successfully"
            vm.showAlert(data)
            //  vm.goal= Goal.get({ id: $stateParams.id }).$promise;
            $timeout(function () {
                $state.reload()
            }, 600)

            //vm.getGoalDetails(result)
        }
        function onMetricSaveSuccess1(result) {
            //  $scope.$emit('leApp:goalUpdate', result);
            vm.isSaving = false;
            var data = "Your goal saved successfully"
            vm.showAlert(data)
            //  vm.goal= Goal.get({ id: $stateParams.id }).$promise;
            //$state.reload()
            //vm.getGoalDetails(result)
        }
        function onMetricSaveError() {
            vm.isSaving = false;
        }

    }
})();
