(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('PostController', PostController);

    PostController.$inject = ['$scope', '$state', 'Post', 'PostSearch'];

    function PostController ($scope, $state, Post, PostSearch) {
        var vm = this;
        
        vm.posts = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Post.query(function(result) {
                vm.posts = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            PostSearch.query({query: vm.searchQuery}, function(result) {
                vm.posts = result;
            });
        }    }
})();
