(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('NotificationController', NotificationController);

    NotificationController.$inject = ['$scope', '$state', 'Notification', 'NotificationSearch', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants', 'Principal', 'NotificationsAll', 'ForApproveRejectConnection', 'DeleteNotification'];

    function NotificationController($scope, $state, Notification, NotificationSearch, ParseLinks, AlertService, pagingParams, paginationConstants, Principal, NotificationsAll, ForApproveRejectConnection, DeleteNotification) {
        var vm = this;

        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;
        vm.searchQuery = pagingParams.search;
        vm.currentSearch = pagingParams.search;
        Principal.identity().then(function (u) {
            vm.loginUser = u;
            loadAll();
        });
        vm.connectionRequest = connectionRequest;
        vm.deleteNotification = deleteNotification;


        function loadAll() {
            // if (pagingParams.search) {
            //     NotificationSearch.query({
            //         id:vm.loginUser.id,
            //         query: pagingParams.search,
            //         page: pagingParams.page - 1,
            //         size: vm.itemsPerPage,
            //         sort: sort()
            //     }, onSuccess, onError);
            // } else {
            NotificationsAll.query({
                id: vm.loginUser.id
            }, onSuccess, onError);
            // }
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                // vm.links = ParseLinks.parse(headers('link'));
                // vm.totalItems = headers('X-Total-Count');
                // vm.queryCount = vm.totalItems;
                vm.notificationLen = data.length;
                vm.notifications = data;

                // alert(JSON.stringify(vm.notifications))
                //  vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }
        function connectionRequest(notificationId, status) {

            ForApproveRejectConnection.query({ notificationId: notificationId, status: status }, function (result) {
                alert(result)
                // $state.reload()
            }).$promise;
            $state.reload()
        }
        function deleteNotification(notificationId) {

            DeleteNotification.query({ id: notificationId }, function (result) {
                // alert(JSON.stringify(result))
                $state.reload()
            })
            //$state.reload()
        }
        function search(searchQuery) {
            if (!searchQuery) {
                return vm.clear();
            }
            vm.links = null;
            vm.page = 1;
            vm.predicate = '_score';
            vm.reverse = false;
            vm.currentSearch = searchQuery;
            vm.transition();
        }

        function clear() {
            vm.links = null;
            vm.page = 1;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.currentSearch = null;
            vm.transition();
        }
    }
})();
