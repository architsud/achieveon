(function () {
    'use strict';
    angular
        .module('leApp')
        .factory('Notification', Notification)
        .factory('NotificationsAll', NotificationsAll)
        .factory('ForApproveRejectConnection', ForApproveRejectConnection)
         .factory('DeleteNotification', DeleteNotification);

    Notification.$inject = ['$resource', 'DateUtils'];

    function Notification($resource, DateUtils) {
        var resourceUrl = 'api/notifications/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createdOn = DateUtils.convertLocalDateFromServer(data.createdOn);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.createdOn = DateUtils.convertLocalDateToServer(copy.createdOn);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.createdOn = DateUtils.convertLocalDateToServer(copy.createdOn);
                    return angular.toJson(copy);
                }
            }
        });
    }
    //http://localhost:8080/api/notifications-all/1001
})();
NotificationsAll.$inject = ['$resource', 'DateUtils'];
function NotificationsAll($resource, DateUtils) {
    var resourceUrl = 'api/notifications-all/:id';

    return $resource(resourceUrl, {}, {
        'query': { method: 'GET', isArray: true },
        'get': {
            method: 'GET',
            transformResponse: function (data) {

                if (data) {
                    data = angular.fromJson(data);
                    console.log("typeof: " + JSON.stringify(data))
                }
                return data;
            }
        },
    })
}
ForApproveRejectConnection.$inject = ['$resource', 'DateUtils'];
function ForApproveRejectConnection($resource, DateUtils) {
    var resourceUrl = 'api/approve-reject-connection/:id';
    return $resource(resourceUrl, {}, {
        'query': { method: 'PUT', isArray: false },
        'update': {
            method: 'PUT',
            transformResponse: function (data) {
                // if (data) {
                //     data = angular.fromJson(data);
                //     console.log("typeof: " + JSON.stringify(data))
                // }
                return data;
            }
        },
    })
}
DeleteNotification.$inject = ['$resource', 'DateUtils'];
function DeleteNotification($resource, DateUtils) {
    var resourceUrl = 'api/notifications/:id';
    return $resource(resourceUrl, {}, {
        'query': { method: 'DELETE', isArray: false },
        'delete': {
            method: 'DELETE',
            transformResponse: function (data) {
                // if (data) {
                //     data = angular.fromJson(data);
                //     console.log("typeof: " + JSON.stringify(data))
                // }
                return data;
            }
        },
    })
}

