(function() {
    'use strict';

    angular
        .module('leApp')
        .factory('DimensionSearch', DimensionSearch);

    DimensionSearch.$inject = ['$resource'];

    function DimensionSearch($resource) {
        var resourceUrl =  'api/_search/dimensions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
