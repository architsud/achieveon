(function () {
	'use strict';

	angular
		.module('leApp')
		.controller('DimensionDetailController', DimensionDetailController);

	DimensionDetailController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$uibModal', '$uibModalStack', 'previousState', 'entity', 'Dimension', 'User', 'Goal', 's3client', '$http'];

	function DimensionDetailController($scope, $rootScope, $state, $stateParams, $uibModal, $uibModalStack, previousState, entity, Dimension, User, Goal, s3client, $http) {
		var vm = this;
		vm.dimension = entity;
	
		vm.today = new Date();
		vm.previousState = previousState.name;
		vm.deleteGoal = deleteGoal;
		vm.modalInstance = "";
		vm.previousStateFun = previousStateFun;
		vm.previousState = previousState;
		vm.selectAvatar = selectAvatar;
		vm.logoPath = "https://s3-us-west-1.amazonaws.com/life-edge/dimensions/" + vm.dimension.imageGuid + ".jpg";

		function selectAvatar() {
			vm.modalInstance2 = $uibModal.open({
				templateUrl: 'app/entities/dimension/dimension-upload-dialog.html'
				, size: 'md'
				, scope: $scope
				, controller: ['$scope', '$uibModal', '$http', UploadDialogController]
			});
		}

		function UploadDialogController($scope, $uibModalInstance, $http) {

			vm.clear = clear;
			vm.upload = upload;
			vm.chooseAvatar = chooseAvatar;
			//vm.logoPath = "/content/images/blank_img.png";

			$scope.uploadme = {};
			$scope.uploadme.src = "";

			vm.cancel = function () { $uibModalInstance.dismiss('cancel'); }

			function clear() {
				vm.modalInstance2.close();
			}

			function save() {
				vm.isSaving = true;
			}

			function onSaveSuccess(result) {
				$uibModalInstance2.close(result);
				vm.isSaving = false;
			}

			function onSaveError() {
				vm.isSaving = false;
			}

			function chooseAvatar(id, avatarName) {
				var logoKey = "dimensions/" + id + ".jpg";
				var keySource = "avatars/dimensions/" + avatarName;
				var apiUrl = "/s3/copy?source=" + keySource + "&destination=" + logoKey;
				$http.get(apiUrl)
					.then(function successCallback(response) {
						vm.logoPath = "https://s3-us-west-1.amazonaws.com/life-edge/dimensions/" + vm.dimension.imageGuid + ".jpg?" + (new Date()).toDateString();
						vm.clear();
					}, function errorCallback(response) {
						alert(response);
						vm.clear();
					});
			}

			function upload() {
				//var files = angular.element('#fileElem');
				if ($scope.uploadme.src != "") {
					vm.waitingForUpload = true;
					var s3Prefix = "https://s3-us-west-1.amazonaws.com/life-edge/dimensions/" + vm.dimension.imageGuid + ".jpg";
					var apiUrl = "/s3/geturl?key=dimensions/" + vm.dimension.imageGuid + ".jpg";
					$http.get(apiUrl)
						.then(function (response) {
							var url = response.data;
							console.debug(url);
							$http.put(url, $scope.uploadme.src, {
								withCredentials: false,
								headers: {
									'Content-Type': 'binary/octet-stream'
								},
								transformRequest: angular.identity
							})
								.success(function (data) {
									vm.waitingForUpload = false;
									vm.Upload = false;
									//console.log(s3Prefix);
									vm.logoPath = s3Prefix + (new Date()).toDateString();;
									vm.clear();
								})
								.error(function (data) {
									vm.waitingForUpload = false;
									vm.Upload = false;
									//console.log(data);
									vm.clear();
								});
						});
				}
			}
		}

		function previousStateFun() {
			// alert(JSON.stringify(vm.previousState))
			$state.go(vm.previousState.name, { id: vm.previousState.params.id })
		}
		var unsubscribe = $rootScope.$on('leApp:dimensionUpdate', function (event, result) {
			vm.dimension = result;
		});
		$scope.$on('$destroy', unsubscribe);
		function deleteGoal(goal, dimension) {
			vm.goal = goal;
			vm.dimensionName = dimension.name;
			vm.modalInstance = $uibModal.open({
				templateUrl: 'app/entities/goal/goal-delete-dialog.html'
				, size: 'md'
				, scope: $scope
				, controller: ['$scope', '$uibModalInstance', DeleteController]
			});
		}
		function DeleteController($scope, $uibModalInstance) {
			vm.clear = clear;
			vm.confirmDelete = confirmDelete;

			function clear() {
				$uibModalInstance.dismiss('cancel');
			}
			function confirmDelete(id, redirectId) {
				Goal.delete({ id: id },
					function () {
						$uibModalInstance.close(true);
						$state.go('dimension-detail', { id: redirectId })
					});
			}
		}
	}
})();
