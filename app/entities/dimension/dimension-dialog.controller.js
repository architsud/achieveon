(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('DimensionDialogController', DimensionDialogController);

    DimensionDialogController.$inject = ['$rootScope', '$window', '$timeout', '$scope', '$state', '$stateParams', '$uibModal', 'entity', 'Dimension', 'User', 'Goal', 'Principal', 'InspiredGoal', '$http', 'previousState', 'FavouriteGoal', 'UnfavouriteGoal'];

    function DimensionDialogController($rootScope, $window, $timeout, $scope, $state, $stateParams, $uibModal, entity, Dimension, User, Goal, Principal, InspiredGoal, $http, previousState, FavouriteGoal, UnfavouriteGoal) {
        // $uibModalInstance,

        var vm = this;
        vm.today = new Date();
        vm.isSelected = false;
        vm.dimension = entity;
        $scope.hideinSmallDesktop = true;
        $scope.hideinMobile = true;
        $scope.hideinBigDesktop = true;
        $scope.hideinTablet = true;
        vm.waitingForUpload = false;
        $scope.windowWidth = $window.innerWidth;
        if ($scope.windowWidth > 767 && $scope.windowWidth < 991) {

            $scope.hideinTablet = false;
            vm.status = 2;
        }
        else if ($scope.windowWidth > 992 && $scope.windowWidth < 1281) {

            $scope.hideinSmallDesktop = false;
            vm.status = 2;
        }
        else if ($scope.windowWidth < 767) {

            $scope.hideinMobile = false;
            vm.status = 0;
        }
        else if ($scope.windowWidth > 1281) {

            $scope.hideinBigDesktop = false;
            vm.status = 2;
        }
        $(window).resize(function () {

            $scope.windowWidth = $window.innerWidth;
            if ($scope.windowWidth > 767 && $scope.windowWidth < 991) {
                $scope.hideinTablet = false;
                vm.status = 2;
            }
            else if ($scope.windowWidth > 992 && $scope.windowWidth < 1281) {
                $scope.hideinSmallDesktop = false;
                vm.status = 2;
            }
            else if ($scope.windowWidth < 767) {
                $scope.hideinMobile = false;
                vm.status = 0;
            }
            else if ($scope.windowWidth > 1281) {
                $scope.hideinBigDesktop = false;
                vm.status = 2;
            }
        });

        vm.change = change;
        function change() {
            console.log(vm.status)
            if (vm.status == 1) {
                vm.status = 0;
            }
            else {
                vm.status = 1;
            }
        }


        if (vm.dimension.imageGuid == null || vm.dimension.imageGuid == undefined || vm.dimension.imageGuid == '') {
            vm.dimension.imageGuid = UUID.generate();
        }
        vm.editPage = false;
        vm.modalInstance = "";
        Dimension.query(function (result) {
            vm.DIMENSIONS = result;
        }).$promise;

        if (entity.newDimension == true) {
            vm.editPage = true;      // vm.editPage = true then it is new page
        } else {
            vm.editPage = false;
        }
        Principal.identity().then(function (u) {
            vm.loginUser = u;
        });
        vm.modalInstance = "";
        vm.getGoalsAndDimensions = getGoalsAndDimensions;
        vm.getGoalsAndDimensions();
        function getGoalsAndDimensions() {
            Dimension.query(function (dimensions) {
                vm.dimensions = dimensions;
                loadData();
            });
        }
        vm.previousStateFun = previousStateFun;
        vm.previousState = previousState;
        function previousStateFun() {

            $state.go(vm.previousState.name, { id: vm.previousState.params.id })
        }
        vm.selectAvatar = selectAvatar;
        vm.logoPath = "https://s3-us-west-1.amazonaws.com/life-edge/dimensions/" + vm.dimension.imageGuid + ".jpg";
        //for Favourite goal
        vm.markFavourite = markFavourite;
        vm.unMarkFavourite = unMarkFavourite;
        function markFavourite(goalId) {
            FavouriteGoal.query({ "userId": vm.loginUser.id, "id": goalId, "firstName": vm.loginUser.firstName });
            $timeout(function () {
                $state.reload();
            }, 600)

        }
        function unMarkFavourite(goalId) {
            UnfavouriteGoal.query({ "userId": vm.loginUser.id, "id": goalId, "firstName": vm.loginUser.firstName });
            $timeout(function () {
                $state.reload();
            }, 600)
        }

        function selectAvatar() {
            vm.modalInstance2 = $uibModal.open({
                templateUrl: 'app/entities/dimension/dimension-upload-dialog.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', UploadDialogController]
            });
        }
        function UploadDialogController($scope, $uibModalInstance) {
            $scope.myImage = '';
            vm.process = 0;
            $scope.myCroppedImage = ''; // in this variable you will have dataUrl of cropped area.
            $scope.$watch('myCroppedImage', function (newVal, oldVal) {
                $scope.myCroppedImage = newVal;

            })
            $scope.handleFileSelect = function () {
                var input = document.getElementById('file-chooser');
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        // bind new Image to Component
                        $scope.$apply(function () {
                            $scope.myImage = e.target.result;
                        });
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $scope.blockingObject = { block: true };

            function dataURItoBlob(dataURI) {
                // convert base64/URLEncoded data component to raw binary data held in a string
                var binary = atob(dataURI.split(',')[1]);
                var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

                var array = [];
                for (var i = 0; i < binary.length; i++) {
                    array.push(binary.charCodeAt(i));
                }
                return new Blob([new Uint8Array(array)], { type: mimeString });
            }
            $scope.blockingObject.callback = function (dataURL) {

                $scope.myCroppedImage = dataURL;
            }
            vm.clear = clear;
            vm.upload = upload;
            vm.chooseAvatar = chooseAvatar;
            //vm.logoPath = "/content/images/blank_img.png";
            $scope.image = "";
            $scope.uploadme = {};
            $scope.uploadme.src = "";

            vm.cancel = function () { $uibModalInstance.dismiss('cancel'); }

            function clear() {
                vm.modalInstance2.close();
            }

            function save() {
                vm.isSaving = true;
            }

            function onSaveSuccess(result) {
                $uibModalInstance2.close(result);
                vm.isSaving = false;
            }

            function onSaveError() {
                vm.isSaving = false;
            }

            function chooseAvatar(id, avatarName) {
                if (vm.dimension.defaultImage) {
                    vm.dimension.imageGuid = UUID.generate();
                }
                var logoKey = "dimensions/" + vm.dimension.imageGuid + ".jpg";
                var keySource = "avatars/dimensions/" + avatarName;
                var apiUrl = "/s3/copy?source=" + keySource + "&destination=" + logoKey;
                $http.get(apiUrl)
                    .then(function successCallback(response) {
                        vm.logoPath = "https://s3-us-west-1.amazonaws.com/life-edge/dimensions/" + vm.dimension.imageGuid + ".jpg"; /*+ (new Date()).toDateString()*/;

                        document.getElementById("imgForGoal").src = vm.logoPath;
                        if (vm.dimension.defaultImage) {
                            Dimension.update(vm.dimension)
                        }
                        vm.clear();
                        var data = "Image added successfully";
                        vm.showAlert(data, 'minTime')
                        // $timeout(function () {
                        //     $state.reload();
                        // }, 2000)
                    }, function errorCallback(response) {
                        // alert(response);
                        vm.clear();
                    });
            }

            function upload() {
                // var fileChooser = document.getElementById('file-chooser');
                // var file = fileChooser.files[0];
                if (vm.dimension.defaultImage) {
                    vm.dimension.imageGuid = UUID.generate();
                }
                if ($scope.myCroppedImage == '') {
                    $scope.blockingObject.render(function (dataURL) {
                        console.log('via render');
                        console.log(dataURL.length);
                        $scope.myCroppedImage = dataURL;
                    });
                }
                var blob = dataURItoBlob($scope.myCroppedImage)
                var file = new File([blob], "goallogo.jpg");

                if (file) {
                    vm.waitingForUpload = true;
                    var s3Prefix = "https://s3-us-west-1.amazonaws.com/life-edge/dimensions/" + vm.dimension.imageGuid + ".jpg?";
                    var apiUrl = "/s3/geturl?key=dimensions/" + vm.dimension.imageGuid + ".jpg";
                    $http.get(apiUrl)
                        .then(function (response) {
                            var url = response.data;
                            console.debug(url);
                            $http.put(url, file, {
                                withCredentials: false,
                                headers: {
                                    'Content-Type': 'binary/octet-stream'
                                }, eventHandlers: {
                                    progress: function (c) {
                                        console.log('Progress -> ' + c);
                                        console.log(c);
                                    }
                                },
                                uploadEventHandlers: {
                                    progress: function (e) {
                                        vm.process = parseInt((e.loaded * 100) / e.total);
                                    }
                                },
                                transformRequest: angular.identity
                            })
                                .success(function (data) {
                                    vm.waitingForUpload = false;
                                    vm.Upload = false;
                                    vm.logoPath = s3Prefix;
                                    document.getElementById("imgForGoal").src = vm.logoPath;
                                    vm.clear();

                                    if (vm.dimension.defaultImage) {
                                        Dimension.update(vm.dimension)
                                    }
                                    var data = "Image added successfully";
                                    vm.showAlert(data, 'minTime');
                                    // document.getElementById("imgForGoal").src = vm.logoPath; //vamc 29-10-17
                                    $scope.setInterval();  //vamc 29-10-17

                                })
                                .error(function (data) {
                                    vm.waitingForUpload = false;
                                    vm.Upload = false;
                                    //console.log(data);
                                    vm.clear();
                                });
                        });
                }
            }
            $scope.setInterval = function () {
                // $scope.timeInterval = $interval(function () {
                //     vm.logoPath = "https://s3-us-west-1.amazonaws.com/life-edge/goal/" + vm.goal.imageGuid + '/logo/goallogo.jpg';
                //     document.getElementById("imgForGoal").src = vm.logoPath;
                //     console.log(vm.logoPath)
                // }, 2000);
                $timeout(function () {
                    //  $interval.cancel($scope.timeInterval);
                    document.getElementById("imgForGoal").src = vm.logoPath;
                }, 1200)
            }
        }
        //for sharing
        vm.share = share;
        function share(goal) {
            $scope.share = {
                templateUrl: 'shareInfo.html',
                goalURL: 'https://www.achieveon.com/#/goal/' + goal.id,
                goal: goal,
                goalimg: 'https://s3-us-west-1.amazonaws.com/life-edge/goal/' + goal.imageGuid + '/logo/goallogo.jpg'
            };
        }
        $scope.shareDialog = function (goal) {

            var ogUrl = 'https://www.achieveon.com/#/goal/' + goal.id;
            //'http://localhost:8080/#/goal/'+vm.goal.id;
            FB.ui({
                method: 'share_open_graph',
                action_type: 'og.shares',
                action_properties: JSON.stringify({
                    object: {
                        'og:url': ogUrl,
                        'og:href': ogUrl,
                        'og:title': goal.goalText,
                        'og:description': goal.description,
                        'og:image': 'https://s3-us-west-1.amazonaws.com/life-edge/goal/' + goal.imageGuid + '/logo/goallogo.jpg'
                    }
                })
            },
                // callback
                function (response) {
                    if (response && !response.error_message) {
                        // then get post content
                        //  alert('successfully posted. Status id : '+JSON.stringify(response));
                    } else {
                        // alert('Something went error.');
                    }
                });
        }
        vm.InspiredGoalFun = InspiredGoalFun;
        // for inspier goal
        function InspiredGoalFun(goal) {

            var goal = goal
            goal.parentGoalId = goal.id;
            goal.goalText = "Copy of " + goal.goalText;
            goal.id = "";
            goal.userCollaborators = [];
            for (var i = 0; i < vm.DIMENSIONS.length; i++) {
                if (vm.DIMENSIONS[i].name == "DEFAULT") {
                    goal.dimensionId = vm.DIMENSIONS[i].id;
                    goal.dimensionName = vm.DIMENSIONS[i].name;
                }
            }
            vm.goal.imageGuid = "";
            for (var i = 0; i < goal.subGoals.length; i++) {
                goal.subGoals[i].id = "";
                // vm.goal.subGoals[i].goalId = "";
            }
            goal.userId = vm.loginUser.id;
            InspiredGoal.query(goal, onInspiredGoalSuccess, onInspiredGoalError);
        }

        function onInspiredGoalSuccess(result) {
            // alert(JSON.stringify(result))
            var data = "Your Copied Goal was added"
            vm.showAlert(data, 'minTime')
            $timeout(function () {
                $state.reload();
            }, 1000)
            //$state.go('goal-detail', { id: result.id })

        }
        function onInspiredGoalError() {
            alert("An error occured while adding Your Copied Goal")
        }


        function loadData() {

            vm.orderedData = [];
            // vm.currentDimensionGoals = [];
            vm.addToCurrentDimension = [];
            for (var i = 0; i < vm.dimensions.length; i++) {
                var dimensionName = vm.dimensions[i].name;
                for (var j = 0; j < vm.dimensions[i].goals.length; j++) {
                    vm.dimensions[i].goals[j].isCurrentDimension = false;
                    if (vm.dimensions[i].goals[j].dimensionId == entity.id) {
                        vm.dimensions[i].goals[j].isCurrentDimension = true;
                    }
                    vm.dimensions[i].goals[j].userCollaborators.forEach(function (user) {
                        if (user.networkUserId == vm.loginUser.id && user.role == 'COLLABORATOR') {
                            vm.dimensions[i].goals[j].goalStatus = 'COLLABORATOR';
                        } else if (user.networkUserId == vm.loginUser.id && user.role == 'SUPPORTER') {
                            vm.dimensions[i].goals[j].goalStatus = 'SUPPORTER';
                        }
                    })
                    if (vm.dimensions[i].goals[j].favouriteUsers != null) {
                        vm.dimensions[i].goals[j].favouriteUsers.forEach(function (user) {
                            if (vm.loginUser.id == user) {
                                vm.dimensions[i].goals[j].favourite = true;
                            }
                        })
                    }


                    // var obj = {
                    //     goalId: vm.dimensions[i].goals[j].id,
                    //     goalName: vm.dimensions[i].goals[j].goalText,
                    //     dimensionId: vm.dimensions[i].id,
                    //     dimensionName: vm.dimensions[i].name,
                    //     isCurrentDimension: isCurrentDimension
                    // }

                    if (vm.dimensions[i].goals[j].isCurrentDimension) {
                        vm.addToCurrentDimension.push(vm.dimensions[i].goals[j]);
                    }
                    vm.orderedData.push(vm.dimensions[i].goals[j]);
                }
            }
        }

        vm.addToCurrentDimension = [];
        function addGoalToDimension(goal, check, index) {
            if (check == false) {
                vm.addToCurrentDimension.push(goal);
                var data = "Your goal was added successfully into this folder. Please click on the save button to update the changes"
                vm.showAlert(data, 'maxTime')
                // vm.currentDimensionGoals.push(goal)
            } else {
                for (var i = 0; i < vm.addToCurrentDimension.length; i++) {
                    if (vm.addToCurrentDimension[i].id == goal.id) {
                        vm.addToCurrentDimension.splice(i, 1);
                        break;
                    }
                }
                // for (var j = 0; j < vm.currentDimensionGoals.length; j++) {
                //   w90  if (vm.currentDimensionGoals[j].id == goal.id) {
                //          vm.currentDimensionGoals.splice(j, 1);
                //         break;
                //     }
                // }
            }
        }
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();
        vm.addGoalToDimension = addGoalToDimension;
        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        // function clear() {
        //     $uibModalInstance.dismiss('cancel');
        // }

        function save() {
            if (vm.addToCurrentDimension) {
                vm.addToCurrentDimension = vm.addToCurrentDimension.map(function (goal) {
                    return goal.id;
                })
            }
            vm.dimension.goalIds = vm.addToCurrentDimension;
            vm.isSaving = true;

            if (vm.dimension.id !== null) {
                Dimension.update(vm.dimension, onSaveSuccess, onSaveError);

            } else {
                vm.dimension.userId = vm.loginUser.id;
                //alert(JSON.stringify(vm.dimension))
                Dimension.save(vm.dimension, onSaveSuccess, onSaveError);

            }
        }
        // $rootScope.$on("deleteDimension", function () {
        //     $uibModalInstance.close(true);
        // });
        function onSaveSuccess(result) {
            // $scope.$emit('leApp:dimensionUpdate', result);
            // $uibModalInstance.close(result);
            var data = "Folder saved successfully"
            vm.showAlert(data, 'minTime')
            $timeout(function () {
                if (vm.dimension.id !== null) {
                    $state.reload();
                }
                else {
                    $state.go('dimension.edit', { id: result.id })
                }

            }, 600)
        }

        function onSaveError() {
            vm.isSaving = false;
        }
        vm.showMessage = showMessage;
        function showMessage() {
            $uibModal.open({
                template: '<h3><span>PLEASE DELETE ALL THE GOALS IN THIS DIMENSION</span></h3>'
                , size: 'md'
                , controller: ['$scope', '$uibModal', ModalInstanceController]
            });
        }
        function ModalInstanceController($scope, $uibModalInstance) {
            $scope.close = function () {
                $uibModalInstance.close(true);
            };
        }

        vm.deleteDimension = deleteDimension;
        function deleteDimension() {
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/dimension/dimension-delete-dialog.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', ModalInstanceController]
            });
        }
        vm.clear = clear;
        function clear() {
            //$uibModalInstance.dismiss('cancel');
            vm.modalInstance.close();
        }
        vm.confirmDelete = confirmDelete;
        function confirmDelete() {
            Dimension.delete({ id: vm.dimension.id },
                function () {
                    vm.modalInstance.close();
                    $state.go('dimension')
                    //  $scope.$emit('deleteDimension');
                });
        }

        function DimensionController($scope, $uibModalInstance) {
            $scope.close = function () {
                vm.modalInstance.close();
            };
        }
        vm.showAlert = showAlert;
        function showAlert(message, time) {
            vm.message = message;
            $scope.time = time;

            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/goal/alert-modal.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModalInstance', ShowAlertController]
            });
        }
        function ShowAlertController($scope, $uibModalInstance) {
            if ($scope.time == 'minTime') {
                vm.time = 1500
            } else {
                vm.time = 5000;
            }
            $timeout(function () {
                $uibModalInstance.close();
            }, vm.time)
        }

    }
})();
