(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('DimensionDeleteController', DimensionDeleteController);

    DimensionDeleteController.$inject = ['$scope', '$state', '$uibModalInstance', '$uibModalStack', 'entity', 'Dimension'];

    function DimensionDeleteController($scope, $state, $uibModalInstance, $uibModalStack, entity, Dimension) {
        var vm = this;

        vm.dimension = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(id) {
            Dimension.delete({ id: id },
                function () {
                    $uibModalInstance.close(true);
                    $scope.$emit('deleteDimension');
                });
        }
    }
})();
