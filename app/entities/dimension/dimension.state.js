(function () {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('dimension', {
                parent: 'entity',
                url: '/dimensions',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Dimensions'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/dimension/dimensions.html',
                        controller: 'DimensionController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'dimension',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('dimension-detail', {
                parent: 'entity',
                url: '/dimension/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Dimension'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/dimension/dimension-detail.html',
                        controller: 'DimensionDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Dimension', function ($stateParams, Dimension) {
                        return Dimension.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'dimension',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('dimension-detail.edit', {
                parent: 'dimension-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        // templateUrl: 'app/entities/dimension/dimension-dialog.html',  create-dimension //13-7-2017
                        templateUrl: 'app/entities/dimension/create-dimension.html',
                        controller: 'DimensionDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'sm',
                        resolve: {
                            entity: ['Dimension', function (Dimension) {
                                return Dimension.get({ id: $stateParams.id }).$promise;
                            }],
                            previousState: ["$state", function ($state) {
                                var currentStateData = {
                                    name: $state.current.name || 'dimension',
                                    params: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }]
                        }
                    })
                        .result.then(function () {
                            $state.go('^', {}, { reload: false });
                        }, function () {
                            $state.go('^');
                        });
                }]
            })
            .state('dimension.new', {
                parent: 'dimension',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/dimension/create-dimension.html',
                        controller: 'DimensionDialogController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: ['$stateParams', '$state', '$uibModal',function ($stateParams, $state, $uibModal) {
                        return {
                            name: null,
                            imageGuid: UUID.generate(),
                            defaultImage: false,
                            details: null,
                            id: null,
                            newDimension: true
                    };
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'dimension',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
                // onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                //     $uibModal.open({
                //       //  templateUrl: 'app/entities/dimension/dimension-dialog.html', //13-7-2017
                //         templateUrl: 'app/entities/dimension/create-dimension.html',
                //         controller: 'DimensionDialogController',
                //         controllerAs: 'vm',
                //         backdrop: 'static',
                //         size: 'lg',
                //         resolve: {
                //             entity: function () {
                //                 return {
                //                     name: null,
                //                     imageGuid: null,
                //                     details: null,
                //                     id: null,
                //                     newDimension: true
                //                 };
                //             },
                //             previousState: ["$state", function ($state) {
                //                 var currentStateData = {
                //                     name: $state.current.name || 'dimension',
                //                     params: $state.params,
                //                     url: $state.href($state.current.name, $state.params)
                //                 };
                //                 return currentStateData;
                //             }]
                //         }
                //     }).result.then(function () {
                //         $state.go('dimension', null, { reload: 'dimension' });
                //     }, function () {
                //         $state.go('dimension');
                //     });
                // }]

            })
            .state('dimension.edit', {
                parent: 'dimension',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/dimension/create-dimension.html',
                        controller: 'DimensionDialogController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: ['Dimension', '$stateParams', '$state', '$uibModal', function (Dimension, $stateParams, $state, $uibModal) {
                        var Dimension = Dimension.get({ id: $stateParams.id }).$promise;
                        Dimension.newDimension = false;
                        return Dimension;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'dimension',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
                // onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                //     $uibModal.open({
                //         templateUrl: 'app/entities/dimension/dimension-dialog.html',
                //         controller: 'DimensionDialogController',
                //         controllerAs: 'vm',
                //         backdrop: 'static',
                //         size: 'lg',
                //         resolve: {
                //             entity: ['Dimension', function (Dimension) {
                //                 var Dimension = Dimension.get({ id: $stateParams.id }).$promise;
                //                 Dimension.newDimension = false;
                //                 return Dimension;
                //             }],
                //             previousState: ["$state", function ($state) {
                //                 var currentStateData = {
                //                     name: $state.current.name || 'dimension',
                //                     params: $state.params,
                //                     url: $state.href($state.current.name, $state.params)
                //                 };
                //                 return currentStateData;
                //             }]
                //         }
                //     })
                //     .result.then(function () {
                //         $state.go('dimension-detail', { id: $stateParams.id })
                //          //alert("aaaaa")
                //         //$state.go('dimension', null, { reload: 'dimension' });
                //     }, function () {
                //        // $state.go('^');
                //        $state.go('dimension-detail', { id: $stateParams.id })
                //     });
                // }]
            })
            .state('dimension.delete', {
                parent: 'dimension',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/dimension/dimension-delete-dialog.html',
                        controller: 'DimensionDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Dimension', function (Dimension) {
                                return Dimension.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('dimension', null, { reload: 'dimension' });
                    }, function () {
                        $state.go('^');
                    });
                }]
            });
    }

})();
