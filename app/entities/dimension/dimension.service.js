(function() {
    'use strict';
    angular
        .module('leApp')
        .factory('Dimension', Dimension);

    Dimension.$inject = ['$resource'];

    function Dimension ($resource) {
        var resourceUrl =  'api/dimensions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
