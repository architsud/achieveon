(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('DimensionController', DimensionController);

    DimensionController.$inject = ['$scope', '$state', 'Dimension', 'DimensionSearch'];

    function DimensionController($scope, $state, Dimension, DimensionSearch) {
        var vm = this;

        vm.dimensions = [];
        vm.orderedData = [];
        vm.search = search;
        vm.loadAll = loadAll;
        vm.changeDimension = changeDimension;
        loadAll();

        function loadAll() {
            Dimension.query(function (result) {
                vm.dimensions = result;
                for (var i = 0; i < vm.dimensions.length; i++) {
                    vm.dimensions[i].completed = 0;
                    vm.dimensions[i].pending = 0;
                     vm.dimensions[i].dimensionGraph=0;
                    for (var j = 0; j < vm.dimensions[i].goals.length; j++) {
                        if (vm.dimensions[i].goals[j].metricAmount!=0 && vm.dimensions[i].goals[j].metricAmount == vm.dimensions[i].goals[j].metricTarget) {
                            vm.dimensions[i].completed += 1;
                        } else {
                           vm.dimensions[i].pending += 1;
                        }
                    }
                   // if(i==vm.dimensions.length-1){
                      // console.log("vm.dimensions[i].completed: "+vm.dimensions[i].completed);
                       //console.log("vm.dimensions[i].pending: "+vm.dimensions[i].pending);
                        vm.dimensions[i].dimensionGraph = parseInt((vm.dimensions[i].completed / vm.dimensions[i].goals.length) * 100) || 0;
                   // }
                }
            });
        }


        function changeDimension() {
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/dimension/dimension-dialog.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModalInstance', ChangeController]
            });
        }
        function ChangeController($scope, $uibModalInstance) {
            vm.clear = clear;
            vm.confirmDelete = confirmDelete;

            function clear() {
                $uibModalInstance.dismiss('cancel');
            }
            // function confirmDelete(id) {
            //     Goal.delete({ id: id },
            //         function () {
            //             $uibModalInstance.close(true);
            //             $state.go('goal')
            //         });
            // }
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            DimensionSearch.query({ query: vm.searchQuery }, function (result) {
                vm.dimensions = result;
            });
        }
    }
})();
