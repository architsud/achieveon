(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('GoalLikesController', GoalLikesController);

    GoalLikesController.$inject = ['$scope', '$state', 'GoalLikes', 'GoalLikesSearch'];

    function GoalLikesController ($scope, $state, GoalLikes, GoalLikesSearch) {
        var vm = this;
        
        vm.goalLikes = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            GoalLikes.query(function(result) {
                vm.goalLikes = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            GoalLikesSearch.query({query: vm.searchQuery}, function(result) {
                vm.goalLikes = result;
            });
        }    }
})();
