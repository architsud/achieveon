(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('GoalLikesDialogController', GoalLikesDialogController);

    GoalLikesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'GoalLikes', 'Goal', 'User'];

    function GoalLikesDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, GoalLikes, Goal, User) {
        var vm = this;

        vm.goalLikes = entity;
        vm.clear = clear;
        vm.save = save;
        vm.goals = Goal.query();
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.goalLikes.id !== null) {
                GoalLikes.update(vm.goalLikes, onSaveSuccess, onSaveError);
            } else {
                GoalLikes.save(vm.goalLikes, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('leApp:goalLikesUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
