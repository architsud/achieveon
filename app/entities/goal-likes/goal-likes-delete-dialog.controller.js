(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('GoalLikesDeleteController',GoalLikesDeleteController);

    GoalLikesDeleteController.$inject = ['$uibModalInstance', 'entity', 'GoalLikes'];

    function GoalLikesDeleteController($uibModalInstance, entity, GoalLikes) {
        var vm = this;

        vm.goalLikes = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            GoalLikes.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
