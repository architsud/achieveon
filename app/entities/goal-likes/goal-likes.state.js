(function() {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('goal-likes', {
            parent: 'entity',
            url: '/goal-likes',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'GoalLikes'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/goal-likes/goal-likes.html',
                    controller: 'GoalLikesController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('goal-likes-detail', {
            parent: 'entity',
            url: '/goal-likes/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'GoalLikes'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/goal-likes/goal-likes-detail.html',
                    controller: 'GoalLikesDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'GoalLikes', function($stateParams, GoalLikes) {
                    return GoalLikes.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'goal-likes',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('goal-likes-detail.edit', {
            parent: 'goal-likes-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/goal-likes/goal-likes-dialog.html',
                    controller: 'GoalLikesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GoalLikes', function(GoalLikes) {
                            return GoalLikes.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('goal-likes.new', {
            parent: 'goal-likes',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/goal-likes/goal-likes-dialog.html',
                    controller: 'GoalLikesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                type: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('goal-likes', null, { reload: 'goal-likes' });
                }, function() {
                    $state.go('goal-likes');
                });
            }]
        })
        .state('goal-likes.edit', {
            parent: 'goal-likes',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/goal-likes/goal-likes-dialog.html',
                    controller: 'GoalLikesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GoalLikes', function(GoalLikes) {
                            return GoalLikes.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('goal-likes', null, { reload: 'goal-likes' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('goal-likes.delete', {
            parent: 'goal-likes',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/goal-likes/goal-likes-delete-dialog.html',
                    controller: 'GoalLikesDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['GoalLikes', function(GoalLikes) {
                            return GoalLikes.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('goal-likes', null, { reload: 'goal-likes' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
