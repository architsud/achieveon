(function() {
    'use strict';

    angular
        .module('leApp')
        .factory('GoalLikesSearch', GoalLikesSearch);

    GoalLikesSearch.$inject = ['$resource'];

    function GoalLikesSearch($resource) {
        var resourceUrl =  'api/_search/goal-likes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
