(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('GoalLikesDetailController', GoalLikesDetailController);

    GoalLikesDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'GoalLikes', 'Goal', 'User'];

    function GoalLikesDetailController($scope, $rootScope, $stateParams, previousState, entity, GoalLikes, Goal, User) {
        var vm = this;

        vm.goalLikes = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('leApp:goalLikesUpdate', function(event, result) {
            vm.goalLikes = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
