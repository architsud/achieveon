(function () {
    'use strict';

    angular
        .module('leApp')
        .factory('UserPersonaSearch', UserPersonaSearch)
        .factory('UserFriendsSearch', UserFriendsSearch);

    UserPersonaSearch.$inject = ['$resource'];

    function UserPersonaSearch($resource) {
        var resourceUrl = 'api/_search/user-personas/:id';
        return $resource(resourceUrl, {}, {
            'query': { method: 'POST', isArray: true }
        });
    }

    //   function UserPersonaSearch($resource) {
    //         var resourceUrl =  'api/user-personas/find/:id';
    //         return $resource(resourceUrl, {}, {
    //             'query': { method: 'GET', isArray: true}
    //         });
    //     }
    UserFriendsSearch.$inject = ['$resource'];
    function UserFriendsSearch($resource) {
        var resourceUrl = 'api/_search/user-networks/:id';
        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true }
        });
    }
})();
