(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('UserPersonaDetailController', UserPersonaDetailController);

    UserPersonaDetailController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', 'previousState', 'entity', 'UserPersona', 'User', 'Goal', '$uibModal', 'UserNetwork', 'Principal', 'GoalsByUserId', 'GetFriendsByUserId', 'InspiredGoal', 'UserNetworkStatus', 'ForApproveRejectConnection', 'Dimension', 'FavouriteGoal', 'UnfavouriteGoal', '$timeout', 'testimonials', 'GetTestimonialsGiver', 'GetTestimonialsReceiver'];

    function UserPersonaDetailController($scope, $rootScope, $state, $stateParams, previousState, entity, UserPersona, User, Goal, $uibModal, UserNetwork, Principal, GoalsByUserId, GetFriendsByUserId, InspiredGoal, UserNetworkStatus, ForApproveRejectConnection, Dimension, FavouriteGoal, UnfavouriteGoal, $timeout, testimonials, GetTestimonialsGiver, GetTestimonialsReceiver) {
        var vm = this;
        vm.userPersonas = entity;
        vm.today = new Date();
        vm.identity = [];
        Principal.identity().then(function (u) {
            vm.identity = u;
            getLoginUserFriends();
            if (vm.userPersonas.id == vm.identity.id) {
                $state.go('user-persona-profile', { id: vm.identity.id })
            }
        });
        vm.totalrate = 0;
        vm.newLineToBR = newLineToBR;

        function newLineToBR(input, name) {

            if (!input) {
                return input;
            }
            var output = input
                //replace possible line breaks.
                .replace(/(\r\n|\r|\n)/g, '<br/>')
                //replace tabs
                .replace(/\t/g, '&nbsp;&nbsp;&nbsp;')
                //replace spaces.
                .replace(/ /g, '&nbsp;');
            return output;
        };
        Dimension.query(function (result) {
            vm.DIMENSIONS = result;

        }).$promise;
        vm.interests = vm.userPersonas.interests.map(function (tag) {
            return tag.id;
        }).join(', ')
        vm.skills = vm.userPersonas.skills.map(function (tag) {
            return tag.id;
        }).join(', ')
        if (vm.userPersonas.profileGuid == null || vm.userPersonas.profileGuid == undefined || vm.userPersonas.profileGuid == '') {
            vm.profilepic = "content/images/userprofiles/default.png"
        }
        else {
            vm.profilepic = "https://s3-us-west-1.amazonaws.com/life-edge/user/" + vm.userPersonas.profileGuid + "/profilepic.jpg";
        }
        if (vm.userPersonas.bannerGuid == null || vm.userPersonas.bannerGuid == undefined || vm.userPersonas.bannerGuid == '') {
            vm.bannerPath = "content/images/ban.jpg"
        }
        else {
            vm.bannerPath = "https://s3-us-west-1.amazonaws.com/life-edge/user/" + vm.userPersonas.bannerGuid + "/userbanner.jpg"
        }

        callFeedback();
        function callFeedback() {
            GetTestimonialsReceiver.query({ userId: vm.userPersonas.id }, function (result) {

                result.forEach(function (user) {
                    // UserPersona.get({ id: user.giverId }, function (data) {
                    //     user.firstname = data.firstname;
                    //     user.lastname = data.lastname;
                    //     user.occupation = data.occupation;
                    //     user.profileGuid = data.profileGuid;
                    // }).$promise;
                    vm.totalrate = vm.totalrate + user.rating;
                })
                vm.totalrate = (vm.totalrate / result.length) || 0.0;

                vm.testimonialReceiver = result;
              

            }).$promise;
        }
        vm.previousStateFun = previousStateFun;
        vm.previousState = previousState;

        vm.goals = [];
        vm.otherUserGoals = [];

        vm.userNetwork = {};
        vm.matchedUser = {};
        vm.InspiredGoalFun = InspiredGoalFun;
        vm.clear = clear;
        function clear() {
            vm.modalInstance.close();
        }
        // for inspiered goal
        function InspiredGoalFun(goal) {

            var goal = goal
            goal.parentGoalId = goal.id;
            goal.goalText = "Copy of " + goal.goalText;
            goal.id = "";
            goal.userCollaborators = [];
            for (var i = 0; i < vm.DIMENSIONS.length; i++) {
                if (vm.DIMENSIONS[i].name == "DEFAULT") {
                    goal.dimensionId = vm.DIMENSIONS[i].id;
                    goal.dimensionName = vm.DIMENSIONS[i].name;
                }
            }
            goal.imageGuid = "";
            for (var i = 0; i < goal.subGoals.length; i++) {
                goal.subGoals[i].id = "";
                // vm.goal.subGoals[i].goalId = "";
            }
            goal.userId = vm.identity.id;
            InspiredGoal.query(goal, onInspiredGoalSuccess, onInspiredGoalError);
        }

        function onInspiredGoalSuccess(result) {              // alert(JSON.stringify(result))
            $state.go('goal-detail', { id: result.id })
            alert("Your Copied Goal was added")
        }
        function onInspiredGoalError() {
            alert("An error occured while adding Your Copied Goal")
        }
        function previousStateFun() {
            $state.go(vm.previousState.name, { id: vm.previousState.params.id })
        }
        vm.openConnection = openConnection;
        vm.closeConnection = closeConnection;
        vm.modalInstance = '';

        vm.getLoginUserGoals = getLoginUserGoals;
        vm.getOtherUserGoals = getOtherUserGoals;
        vm.getLoginUserFriends = getLoginUserFriends;
        //Testimonials Start
        vm.tesmonialDialog = tesmonialDialog;
        function tesmonialDialog() {
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/user-persona/testimonials.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', '$uibModalInstance', TestimonialDialogController]
            });
        }

        function TestimonialDialogController() {
            vm.testimonial = 'Testimonials';
            vm.feedtext = '';
            vm.rating = 0;
            vm.rateFunction = function (rating) {
                vm.rating = rating;
            };
            // $scope.edit = false;

            GetTestimonialsGiver.query({ userId: vm.userPersonas.id }, function (result) {
                // result.forEach(function (user) {
                //     UserPersona.get({ id: user.receiverId }, function (data) {
                //         user.firstname = data.firstname;
                //         user.lastname = data.lastname;
                //         user.occupation = data.occupation;
                //         user.profileGuid = data.profileGuid;
                //     }).$promise;
                // })
                vm.testimonialGiven = result;
            }).$promise;
            vm.sendFeedback = sendFeedback;
            vm.cleartext = cleartext;
            function cleartext() {
                vm.feedtext = "";
                vm.rating = 0;
            }
            function sendFeedback() {

                var send =
                    {
                        "text": vm.feedtext,
                        "rating": vm.rating,
                        "receiverId": vm.userPersonas.id,
                        "giverId": vm.identity.id
                    }
                testimonials.query(send, function () {
                    callFeedback();
                    vm.feedtext = "";
                })
            }

        }
        //Testimonials End
        //for Favourite goal
        vm.markFavourite = markFavourite;
        vm.unMarkFavourite = unMarkFavourite;
        function markFavourite(goalId) {
            FavouriteGoal.query({ "userId": vm.identity.id, "id": goalId, "firstName": vm.identity.firstName });
            $timeout(function () {
                $state.reload();
            }, 600)

        }
        function unMarkFavourite(goalId) {
            UnfavouriteGoal.query({ "userId": vm.identity.id, "id": goalId, "firstName": vm.identity.firstName });
            $timeout(function () {
                $state.reload();
            }, 600)
        }

        vm.connectionRequest = connectionRequest;
        function connectionRequest(senderId, status) {

            ForApproveRejectConnection.query({ id: vm.networkUserId, senderId: senderId, userId: vm.identity.id, status: status }, function (result) {
                alert(result)
                // $state.reload()
            }).$promise;
            $state.reload()
        }
        getLoginUserGoals();
        function getLoginUserGoals() {
            Goal.query(function (result) {
                // vm.goals = result.sort(function (a, b) {
                //     return a.id - b.id
                // });
                vm.goals = vm.goals.filter(function (goal) {
                    return goal.privacy == 'PUBLIC';
                })

            }).$promise;
        }
        getOtherUserGoals();
        function getOtherUserGoals() {
            GoalsByUserId.query({ userId: vm.userPersonas.id }, function (result) {
                vm.otherUserGoals = result;
                vm.otherUserGoals = vm.otherUserGoals.filter(function (goal) {
                    return goal.privacy == 'PUBLIC';
                })
                for (var i = 0; i < vm.otherUserGoals.length; i++) {
                    vm.otherUserGoals[i].userCollaborators.forEach(function (user) {
                        if (user.networkUserId == vm.identity.id && user.role == 'COLLABORATOR' && user.status == "ACCEPTED") {
                            vm.otherUserGoals[i].goalStatus = 'COLLABORATOR';
                        } else if (user.networkUserId == vm.identity.id && user.role == 'SUPPORTER' && user.status == "ACCEPTED") {
                            vm.otherUserGoals[i].goalStatus = 'SUPPORTER';
                        }
                    })
                    if (vm.otherUserGoals[i].favouriteUsers != null) {
                        vm.otherUserGoals[i].favouriteUsers.forEach(function (user) {
                            if (vm.identity.id == user) {
                                vm.otherUserGoals[i].favourite = true;
                            }
                        })
                    }
                }
            }).$promise;
        }
        vm.userFriends = [];
        function getLoginUserFriends() {
            vm.isFriend = true;
            // GetFriendsByUserId.query({ userId: vm.identity.id }, function (result) {
            //   console.log("GetFriendsByUserId: "+JSON.stringify(result))
            UserNetworkStatus.query({ userId: vm.identity.id, status: 'ALL' }, function (result) {
                if (result.length > 0) {
                    result.forEach(function (user) {

                        if (user.networkUserId == vm.userPersonas.id) {
                            vm.matchedUser = user;
                            vm.isFriend = false;

                            if (user.senderId == vm.userPersonas.id && user.status == "PENDING") {
                                vm.status = 'APPROVE';
                                vm.senderId = user.senderId;
                                vm.networkUserId = user.id;
                            }
                            else if (user.status == "ACCEPTED") {
                                vm.status = 'ACCEPTED';
                                vm.userFriends.push(user);

                            }
                            else {
                                vm.status = 'REQUEST SENT';
                            }
                            return true
                        }
                    })
                } else {
                    vm.isFriend = true;
                }
            }).$promise;
        }
        vm.share = share;
        function share(goal) {
            $scope.share = {
                templateUrl: 'shareInfo.html',
                goalURL: 'https://www.achieveon.com/#/goal/' + goal.id,
                goal: goal,
                goalimg: 'https://s3-us-west-1.amazonaws.com/life-edge/goal/' + goal.imageGuid + '/logo/goallogo.jpg'
            };
        }
        $scope.shareDialog = function (goal) {

            var ogUrl = 'https://www.achieveon.com/#/goal/' + goal.id;
            //'http://localhost:8080/#/goal/'+vm.goal.id;
            FB.ui({
                method: 'share_open_graph',
                action_type: 'og.shares',
                action_properties: JSON.stringify({
                    object: {
                        'og:url': ogUrl,
                        'og:href': ogUrl,
                        'og:title': goal.goalText,
                        'og:description': goal.description,
                        'og:image': 'https://s3-us-west-1.amazonaws.com/life-edge/goal/' + goal.imageGuid + '/logo/goallogo.jpg'
                    }
                })
            },
                // callback
                function (response) {
                    if (response && !response.error_message) {
                        // then get post content
                        //  alert('successfully posted. Status id : '+JSON.stringify(response));
                    } else {
                        // alert('Something went error.');
                    }
                });
        }

        //Add Connection
        function openConnection() {
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/user-persona/add-connection.html'
                , size: 'sm'
                , scope: $scope
                , controller: ['$scope', '$uibModal', InviteController]
            });
        }
        function InviteController() {
            vm.addConnection = addConnection;   //Definining function
            vm.selectedGoal = selectedGoal;       //Definining function
            vm.selectedGoalData = '';
            vm.userNetwork.networkUserId = vm.userPersonas.id;
            vm.userNetwork.networkRelation = 'FRIEND';
            vm.userNetwork.userId = vm.identity.id;
            vm.userNetwork.firstName = vm.identity.firstName;
            vm.userNetwork.lastName = vm.identity.lastName;
            vm.collaborate = {
                "firstName": vm.userPersonas.firstname,
                "lastName": vm.userPersonas.lastname,
                "occupation": vm.userPersonas.occupation,
                "userId": vm.userPersonas.id
            }
            function selectedGoal(goal) {                    //Using function
                vm.collaborate.goalId = goal.id;
                vm.collaborate.goalName = goal.goalText;
            }
            function addConnection() {
                if (vm.collaborate.role) {
                    vm.userNetwork.collaborate = vm.collaborate;
                }
                // alert(JSON.stringify(vm.userNetwork.collaborate))
                // if (vm.userNetwork.id !== null) {
                //     UserNetwork.update(vm.userNetwork, onSaveSuccess, onSaveError);
                // } else {
                UserNetwork.save(vm.userNetwork, onSaveSuccess, onSaveError);
                // }
            }
            function onSaveSuccess(result) {
                getLoginUserFriends();
                vm.modalInstance.close();
            }

            function onSaveError() {
                vm.isSaving = false;
                vm.modalInstance.close();
            }
        }
        function closeConnection() {
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/user-persona/delete-connection.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', RemoveConnection]
            });
        }
        function RemoveConnection() {
            vm.removeConnection = removeConnection;
            function removeConnection() {
                vm.userNetwork = vm.matchedUser;
                UserNetwork.delete(vm.userNetwork, onRemoveSuccess, onRemoveError);
            }
            function onRemoveSuccess(result) {
                $scope.$emit('leApp:userNetworkUpdate', result);
                getLoginUserFriends();
                vm.modalInstance.close();
                $state.reload();
            }
            function onRemoveError() {
                vm.modalInstance.close();
            }

        }

        var unsubscribe = $rootScope.$on('leApp:userPersonaUpdate', function (event, result) {
            vm.userPersonas = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }

})();
