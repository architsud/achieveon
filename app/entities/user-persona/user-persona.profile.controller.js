(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('UserPersonaProfileController', UserPersonaProfileController);

    UserPersonaProfileController.$inject = ['$scope', '$rootScope', '$state', 'UserPersona', 'UserPersonaSearch', '$uibModal', 's3client', 'Goal', 'Principal', 'previousState', 'InspiredGoal', 'Dimension', 'FavouriteGoal', 'UnfavouriteGoal', '$timeout', '$http', 'OwnAndCollaboratedGoals', 'GetTestimonialsGiver', 'GetTestimonialsReceiver', 'UserNetworkStatus', 'Recommendation'];

    function UserPersonaProfileController($scope, $rootScope, $state, UserPersona, UserPersonaSearch, $uibModal, s3client, Goal, Principal, previousState, InspiredGoal, Dimension, FavouriteGoal, UnfavouriteGoal, $timeout, $http, OwnAndCollaboratedGoals, GetTestimonialsGiver, GetTestimonialsReceiver, UserNetworkStatus, Recommendation) {
        var vm = this;
        vm.today = new Date();
        vm.userPersonas = [];
        vm.search = search;
        vm.modalInstance = "";
        vm.loadAll = loadAll;
        vm.previousStateFun = previousStateFun;
        vm.previousState = previousState;
        vm.InspiredGoalFun = InspiredGoalFun;
        vm.profileGuid = '';
        vm.bannerGuid = '';
        vm.uploadImage = uploadImage;
        vm.selectAvatar = selectAvatar;
        vm.tesmonialDialog = tesmonialDialog;
        vm.totalrate = 0;
        vm.clear = clear;
        vm.newLineToBR = newLineToBR;
        function newLineToBR(input, name) {
            if (name == 'about') {
                if (!input) return 'Write about yourself';
            }
            else {
                if (!input) return input;
            }
            var output = input
                //replace possible line breaks.
                .replace(/(\r\n|\r|\n)/g, '<br/>')
                //replace tabs
                .replace(/\t/g, '&nbsp;&nbsp;&nbsp;')
                //replace spaces.
                .replace(/ /g, '&nbsp;');
            return output;
        };
        // upload  image 
        function selectAvatar() {
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/account/register/register-dialog.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', '$uibModalInstance', RegisterDialogController]
            });
        }

        function RegisterDialogController($scope, $uibModalInstance) {
            $scope.myImage = '';
            vm.process = 0;
            $scope.myCroppedImage = ''; // in this variable you will have dataUrl of cropped area.
            $scope.$watch('myCroppedImage', function (newVal, oldVal) {
                $scope.myCroppedImage = newVal;
            })
            $scope.handleFileSelect = function () {
                var input = document.getElementById('file-chooser');
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        // bind new Image to Component
                        $scope.$apply(function () {
                            $scope.myImage = e.target.result;
                        });
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $scope.blockingObject = { block: true };

            function dataURItoBlob(dataURI) {
                // convert base64/URLEncoded data component to raw binary data held in a string
                var binary = atob(dataURI.split(',')[1]);
                var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

                var array = [];
                for (var i = 0; i < binary.length; i++) {
                    array.push(binary.charCodeAt(i));
                }
                return new Blob([new Uint8Array(array)], { type: mimeString });
            }
            $scope.blockingObject.callback = function (dataURL) {

                $scope.myCroppedImage = dataURL;
            }
            vm.clear = clear;
            vm.upload = upload;
            $scope.image = "";
            vm.chooseAvatar = chooseAvatar;
            vm.profilepic1 = "content/images/avatars/male3-128.png";

            $scope.uploadme = {};
            $scope.uploadme.src = "";

            vm.cancel = function () { $uibModalInstance.dismiss('cancel'); }

            function clear() {
                vm.modalInstance.close();
            }

            function save() {
                vm.isSaving = true;
            }

            function onSaveSuccess(result) {
                $uibModalInstance.close(result);
                vm.isSaving = false;
            }

            function onSaveError() {
                vm.isSaving = false;
            }

            function chooseAvatar(avatarName) {
                var keySource = "avatars/" + avatarName;
                vm.profilePic = "http://s3-us-west-1.amazonaws.com/life-edge/" + keySource;
                vm.clear();
                var destination = '';
                if (vm.userPersonas.profileGuid == null || vm.userPersonas.profileGuid == undefined || vm.userPersonas.profileGuid == '') {
                    var destination = 'user/' + vm.profileGuid + "/profilepic.jpg";
                }
                else {
                    var destination = 'user/' + vm.userPersonas.profileGuid + "/profilepic.jpg";
                }
                //var destination = "user/" + vm.identity.id + "/profilepic.jpg";
                var apiUrl = "/s3/copy?source=" + keySource + "&destination=" + destination;
                $http.get(apiUrl)
                    .then(function successCallback(response) {

                        $scope.$apply(function () {
                            vm.profilePic = "https://s3-us-west-1.amazonaws.com/life-edge/" + destination + "?" + (new Date()).getMilliseconds();
                        })

                        if (vm.userPersonas.profileGuid == null || vm.userPersonas.profileGuid == undefined || vm.userPersonas.profileGuid == '') {
                            vm.userPersonas.profileGuid = vm.profileGuid;

                            UserPersona.update(vm.userPersonas);
                            // alert(vm.userPersonas.profileGuid)
                        }
                        vm.clear();
                        $timeout(function () {
                            $state.reload();
                        }, 2000)
                    }, function errorCallback(response) {
                        alert(response);
                        vm.clear();
                    });
            }

            function upload() {
                // var fileChooser = document.getElementById('file-chooser');
                // var file = fileChooser.files[0];
                if ($scope.myCroppedImage == '') {
                    $scope.blockingObject.render(function (dataURL) {
                        console.log('via render');
                        console.log(dataURL.length);
                        $scope.myCroppedImage = dataURL;
                    });
                }
                var blob = dataURItoBlob($scope.myCroppedImage)
                var file = new File([blob], "profilepic.jpg");
                if (file) {
                    vm.waitingForUpload = true;
                    var destination = '';
                    if (vm.userPersonas.profileGuid == null || vm.userPersonas.profileGuid == undefined || vm.userPersonas.profileGuid == '') {
                        var destination = 'user/' + vm.profileGuid + "/profilepic.jpg";
                    }
                    else {
                        var destination = 'user/' + vm.userPersonas.profileGuid + "/profilepic.jpg";
                    }
                    // var destination = "user/" + vm.identity.id + "/profilepic.jpg";
                    var s3Prefix = "https://s3-us-west-1.amazonaws.com/life-edge/" + destination;
                    var apiUrl = "/s3/geturl?key=" + destination;
                    console.log(apiUrl);
                    $http.get(apiUrl)
                        .then(function (response) {
                            var url = response.data;
                            $http.put(url, file, {
                                withCredentials: false,
                                headers: {
                                    'Content-Type': 'binary/octet-stream'
                                },
                                eventHandlers: {
                                    progress: function (c) {
                                        console.log('Progress -> ' + c);
                                        console.log(c);
                                    }
                                },
                                uploadEventHandlers: {
                                    progress: function (e) {
                                        vm.process = parseInt((e.loaded * 100) / e.total);
                                    }
                                },
                                transformRequest: angular.identity
                            })
                                .success(function (data) {
                                    //  console.log(response.data);
                                    vm.waitingForUpload = false;
                                    vm.Upload = false;

                                    // $scope.$apply(function () {
                                    vm.profilePic = s3Prefix + "?" + (new Date()).getMilliseconds();
                                    //  alert(vm.profilePic)
                                    // })
                                    if (vm.userPersonas.profileGuid == null || vm.userPersonas.profileGuid == undefined || vm.userPersonas.profileGuid == '') {
                                        vm.userPersonas.profileGuid = vm.profileGuid;
                                        // alert(vm.userPersonas.bannerGuid)
                                        // alert(vm.userPersonas.profileGuid)
                                        UserPersona.update(vm.userPersonas);
                                    }

                                    vm.clear();

                                    $timeout(function () {
                                        $state.reload();
                                    }, 2000)
                                })
                                .error(function (data) {
                                    vm.waitingForUpload = false;
                                    vm.Upload = false;
                                    console.log(data);
                                    vm.clear();
                                });
                        });
                }


            }
        }
        function uploadImage() {
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/user-persona/user-persona-bannerUpload.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', ModalInstanceController]
            });
        };
        function clear() {
            // $uibModalInstance.dismiss('cancel');
            vm.modalInstance.close();

        }

        //Testimonials Start
        function tesmonialDialog() {
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/user-persona/testimonials.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', '$uibModalInstance', TestimonialDialogController]
            });
        }

        function TestimonialDialogController() {
            vm.testimonial = 'Testimonials';
            $scope.edit = false;
            GetTestimonialsGiver.query({ userId: vm.identity.id }, function (result) {
                vm.testimonialGiven = result;
            }).$promise;
            vm.connections = [];
            vm.removeConnections = removeConnections;
            function removeConnections(index) {
                vm.connections.splice(index, 1);

            }
            vm.sendRecommendation = sendRecommendation;
            function sendRecommendation() {
                Recommendation.query({ "connections": vm.connections, "receiverId": vm.identity.id, "receiverLastName": vm.identity.lastName, "receiverFirstName": vm.identity.firstName }, function () {
                    vm.modalInstance.close();
                    var data = "Your recommendation message sent successfully"
                    $timeout(function () {
                        vm.showAlert(data)
                    }, 600)

                })
            }
            vm.addConnections = addConnections;
            function addConnections(id) {
                vm.connections.push(id)
            }
        }
        //Testimonials End
        function ModalInstanceController($scope, $uibModalInstance) {
            $scope.myBannerImage = '';
            
            $scope.myCroppedBannerImage = ''; // in this variable you will have dataUrl of cropped area.
            $scope.$watch('myCroppedBannerImage', function (newVal, oldVal) {
                $scope.myCroppedBannerImage = newVal;

            })
            $scope.handleFileBannerSelect = function () {
                var input = document.getElementById('file-chooser');
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        // bind new Image to Component

                        var image = new Image();
                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;
                        image.onload = function () {

                            var height = this.height;
                            var width = this.width;
                            if (width >= height) {
                                $scope.$apply(function () {
                                    $scope.myBannerImage = e.target.result;
                                });
                            } else if (width < height) {
                                vm.modalInstance.close();
                                var data = "Please Upload Landscape For Cover Picture"
                                vm.showAlert(data)

                                // $("#file-chooser").val('');
                            }

                        }
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $scope.blockingObject = { block: true };

            function dataURItoBlob(dataURI) {
                // convert base64/URLEncoded data component to raw binary data held in a string
                var binary = atob(dataURI.split(',')[1]);
                var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

                var array = [];
                for (var i = 0; i < binary.length; i++) {
                    array.push(binary.charCodeAt(i));
                }
                return new Blob([new Uint8Array(array)], { type: mimeString });
            }
            $scope.blockingObject.callback = function (dataURL) {

                $scope.myCroppedBannerImage = dataURL;
            }
            //$scope.vm = vm;
            vm.process = 0;
            $scope.image = "";
            if (vm.userPersonas.bannerGuid == null || vm.userPersonas.bannerGuid == undefined || vm.userPersonas.bannerGuid == '') {
                $scope.keyPrefix = 'user/' + vm.bannerGuid;
            }
            else {
                $scope.keyPrefix = 'user/' + vm.userPersonas.bannerGuid;
            }


            $scope.save = function () {
                $uibModalInstance.close($scope.theThingIWantToSave);
            };
            $scope.cancel = function () {
                vm.modalInstance.close();
            };

            $scope.uploadS3File = function (isLogo) {
                // var fileChooser = document.getElementById('file-chooser');
                // var file = fileChooser.files[0];
                if ($scope.myCroppedBannerImage == '') {
                    $scope.blockingObject.render(function (dataURL) {
                        console.log('via render');
                        console.log(dataURL.length);
                        $scope.myCroppedBannerImage = dataURL;
                    });
                }
                var blob = dataURItoBlob($scope.myCroppedBannerImage)
                var file = new File([blob], "userbanner.jpg");

                if (file) {

                    var fileName = 'userbanner.jpg';
                    var params = {
                        Key: $scope.keyPrefix + '/' + fileName
                        , ContentType: file.type
                        , Body: file
                    };
                    var bucket = s3client.getS3bucket();
                    bucket.upload(params).on('httpUploadProgress', function (evt) {
                        $scope.$apply(function () {
                            vm.process = parseInt((evt.loaded * 100) / evt.total);
                        })
                    }).send(function (err, data) {
                        if (isLogo) {
                            $scope.$apply(function () {
                                vm.bannerPath = "https://s3-us-west-1.amazonaws.com/life-edge/" + $scope.keyPrefix + "/userbanner.jpg"//? + new Date().getTime();
                            })
                            if (vm.userPersonas.bannerGuid == null || vm.userPersonas.bannerGuid == undefined || vm.userPersonas.bannerGuid == '') {
                                vm.userPersonas.bannerGuid = vm.bannerGuid;
                                // alert(vm.userPersonas.bannerGuid)
                                UserPersona.update(vm.userPersonas);
                            }
                            if (data) {
                                console.log("JSON: " + JSON.stringify(data))
                                $timeout(function () {
                                    $state.reload();
                                }, 2000)
                            }

                        }
                        else {
                            var collectionsPromise = s3client.getCollections(vm.userPersonas.bannerGuid);
                            collectionsPromise.then(function (cols) {
                                vm.collections = cols;
                            }, function (reason) {
                                console.log('Failed to update collections :' + reason);
                            });
                        }
                        $scope.cancel();
                    });
                }
            }

            $scope.vm.chooseAvatar = function (id, avatarName) {
                var logoKey = $scope.keyPrefix + '/userbanner.jpg';
                var keySource = "avatars/dimensions/" + avatarName;
                var apiUrl = "/s3/copy?source=" + keySource + "&destination=" + logoKey;
                $http.get(apiUrl)
                    .then(function successCallback(response) {

                        vm.bannerPath = "https://s3-us-west-1.amazonaws.com/life-edge/" + logoKey;

                        if (vm.userPersonas.bannerGuid == null || vm.userPersonas.bannerGuid == undefined || vm.userPersonas.bannerGuid == '') {
                            vm.userPersonas.bannerGuid = vm.bannerGuid;
                            UserPersona.update(vm.userPersonas);
                        }
                        $scope.cancel();
                        $timeout(function () {
                            $state.reload();
                        }, 2000)
                    }, function errorCallback(response) {
                        alert("An unexpected error occurred: " + response);
                        $scope.cancel();
                    });
            }
        }

        vm.updateSettings = updateSettings;
        function updateSettings() {
            vm.userPersonas.skills = [];
            if (vm.skills) {
                var skills = vm.skills.split(',');
                skills.forEach(function (tag) {
                    vm.userPersonas.skills.push({
                        'id': tag
                    })
                })

            }
            if (vm.interests) {
                vm.userPersonas.interests = [];
                var interests = vm.interests.split(',');
                interests.forEach(function (tag) {
                    vm.userPersonas.interests.push({
                        'id': tag
                    })
                })

            }


            UserPersona.update(vm.userPersonas, onSaveSuccess, onSaveError);
        }
        function onSaveSuccess(result) {
            console.log("Settings updated");
            $rootScope.$emit('authenticated');
            var data = "Your information updated successfully"
            vm.showAlert(data)
            $timeout(function () {
                $state.reload();
            }, 600)

        }

        function onSaveError() {
            console.log("Error in updating settings")
        }
        function clear() {
            vm.modalInstance.close();
        }
        //Show alert
        vm.showAlert = showAlert;
        function showAlert(message) {
            vm.message = message;
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/goal/alert-modal.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModalInstance', ShowAlertController]
            });
        }
        function ShowAlertController($scope, $uibModalInstance) {
            $timeout(function () {
                $uibModalInstance.close();
            }, 1300)
        }
        function previousStateFun() {
            $state.go(vm.previousState.name, { id: vm.previousState.params.id })
        }
        loadAll();
        function loadAll() { }

        Principal.identity().then(function (u) {
            vm.identity = u;
            if (u != null) {
                UserPersona.get({ id: vm.identity.id }).$promise.then(function (v) {
                    vm.userPersonas = v;
                    if (vm.userPersonas.bannerGuid == null || vm.userPersonas.bannerGuid == undefined || vm.userPersonas.bannerGuid == '') {
                        vm.bannerGuid = UUID.generate();
                        // alert(vm.bannerGuid)
                        vm.bannerPath = "content/images/ban.jpg"
                    }
                    else {
                        vm.bannerPath = "https://s3-us-west-1.amazonaws.com/life-edge/user/" + vm.userPersonas.bannerGuid + "/userbanner.jpg"
                    }

                    vm.interests = vm.userPersonas.interests.map(function (tag) {
                        return tag.id;
                    }).join(', ')
                    vm.skills = vm.userPersonas.skills.map(function (tag) {
                        return tag.id;
                    }).join(', ')
                    //vm.interests=vm.userPersonas
                    if (vm.userPersonas.profileGuid == null || vm.userPersonas.profileGuid == undefined || vm.userPersonas.profileGuid == '') {
                        vm.profileGuid = UUID.generate();

                        vm.profilepic = "/content/images/dimension-new-edit/Photo.png"
                        //alert(vm.profileGuid)
                    }
                    else {
                        vm.profilepic = "https://s3-us-west-1.amazonaws.com/life-edge/user/" + vm.userPersonas.profileGuid + "/profilepic.jpg";

                    }


                });
                callFeedback();
                function callFeedback() {
                    GetTestimonialsReceiver.query({ userId: vm.identity.id }, function (result) {

                        result.forEach(function (user) {
                            // UserPersona.get({ id: user.giverId }, function (data) {
                            //     user.firstname = data.firstname;
                            //     user.lastname = data.lastname;
                            //     user.occupation = data.occupation;
                            //     user.profileGuid = data.profileGuid;
                            // }).$promise;
                            vm.totalrate = vm.totalrate + user.rating;
                        })
                        vm.totalrate = (vm.totalrate / result.length) || 0.0;
                        vm.testimonialReceiver = result;
                    }).$promise;
                }
                UserNetworkStatus.query({ userId: vm.identity.id, status: 'ACCEPTED' }, function (result) {
                    vm.userFriends = result;

                }).$promise;
            }
        });
        Dimension.query(function (result) {
            vm.DIMENSIONS = result;

        }).$promise;
        vm.share = share;
        function share(goal) {
            $scope.share = {
                templateUrl: 'shareInfo.html',
                goalURL: 'https://www.achieveon.com/#/goal/' + goal.id,
                goal: goal,
                goalimg: 'https://s3-us-west-1.amazonaws.com/life-edge/goal/' + goal.imageGuid + '/logo/goallogo.jpg'
            };
        }
        $scope.shareDialog = function (goal) {

            var ogUrl = 'https://www.achieveon.com/#/goal/' + goal.id;
            //'http://localhost:8080/#/goal/'+vm.goal.id;
            FB.ui({
                method: 'share_open_graph',
                action_type: 'og.shares',
                action_properties: JSON.stringify({
                    object: {
                        'og:url': ogUrl,
                        'og:href': ogUrl,
                        'og:title': goal.goalText,
                        'og:description': goal.description,
                        'og:image': 'https://s3-us-west-1.amazonaws.com/life-edge/goal/' + goal.imageGuid + '/logo/goallogo.jpg'
                    }
                })
            },
                // callback
                function (response) {
                    if (response && !response.error_message) {
                        // then get post content
                        //  alert('successfully posted. Status id : '+JSON.stringify(response));
                    } else {
                        // alert('Something went error.');
                    }
                });
        }
        function InspiredGoalFun(goal) {
            var goal = goal
            goal.parentGoalId = goal.id;
            goal.goalText = "Copy of " + goal.goalText;
            goal.id = "";
            goal.userCollaborators = [];
            for (var i = 0; i < vm.DIMENSIONS.length; i++) {
                if (vm.DIMENSIONS[i].name == "DEFAULT") {
                    goal.dimensionId = vm.DIMENSIONS[i].id;
                    goal.dimensionName = vm.DIMENSIONS[i].name;
                }
            }
            goal.imageGuid = "";
            for (var i = 0; i < goal.subGoals.length; i++) {
                goal.subGoals[i].id = "";
                // vm.goal.subGoals[i].goalId = "";
            }
            goal.userId = vm.identity.id;
            InspiredGoal.query(goal, onInspiredGoalSuccess, onInspiredGoalError);
        }

        function onInspiredGoalSuccess(result) {              // alert(JSON.stringify(result))
            $state.go('goal-detail', { id: result.id })
            alert("Your Copied Goal was added")
        }
        function onInspiredGoalError() {
            alert("An error occured while adding Your Copied Goal")
        }
        //for Favourite goal
        vm.markFavourite = markFavourite;
        vm.unMarkFavourite = unMarkFavourite;
        function markFavourite(goalId) {
            FavouriteGoal.query({ "userId": vm.identity.id, "id": goalId, "firstName": vm.identity.firstName });
            $timeout(function () {
                $state.reload();
            }, 600)

        }
        function unMarkFavourite(goalId) {
            UnfavouriteGoal.query({ "userId": vm.identity.id, "id": goalId, "firstName": vm.identity.firstName });
            $timeout(function () {
                $state.reload();
            }, 600)
        }
        vm.goals = [];
        // Goal.query(function (result) {   //11-8-17
        Principal.identity().then(function (u) {
            vm.identity = u;
            OwnAndCollaboratedGoals.query({ userId: vm.identity.id }, function (result) {
                vm.goals = result
                // vm.goals = result.sort(function (a, b) {
                //     return a.id - b.id
                // });
                for (var i = 0; i < vm.goals.length; i++) {
                    vm.goals[i].userCollaborators.forEach(function (user) {
                        if (user.networkUserId == vm.identity.id && user.role == 'COLLABORATOR') {
                            vm.goals[i].goalStatus = 'COLLABORATOR';
                        } else if (user.networkUserId == vm.identity.id && user.role == 'SUPPORTER') {
                            vm.goals[i].goalStatus = 'SUPPORTER';
                        }
                    })
                    if (vm.goals[i].favouriteUsers != null) {
                        vm.goals[i].favouriteUsers.forEach(function (user) {
                            if (vm.identity.id == user) {
                                vm.goals[i].favourite = true;
                            }
                        })
                    }
                }
                vm.goals = vm.goals.filter(function (goal) {
                    return goal.privacy == 'PUBLIC';
                })
            }).$promise;
        }).$promise;

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            UserPersonaSearch.query({ query: vm.searchQuery }, function (result) {
                vm.userPersonas = result;
            });
        }


    }
})();
