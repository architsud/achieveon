(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('UserPersonaController', UserPersonaController);

    UserPersonaController.$inject = ['$scope', '$state', 'UserPersona', 'UserPersonaSearch', '$uibModal', 'Goal', 'Principal', 'previousState', 'GetFriendsByUserId', 'CollaboratorsByUser','UserNetworkStatus'];

    function UserPersonaController($scope, $state, UserPersona, UserPersonaSearch, $uibModal, Goal, Principal, previousState, GetFriendsByUserId, CollaboratorsByUser,UserNetworkStatus) {
        var vm = this;
        vm.today = new Date();
        vm.userPersonas = [];
        vm.supporters = [];
        vm.collaborators = [];
        vm.allUsers = [];
        vm.search = search;
        vm.modalInstance = "";
        vm.loadAll = loadAll;
        vm.addConnection = addConnection;
        vm.inviteUsers = inviteUsers;
        vm.clear = clear;
        vm.switchUsers = switchUsers;
        vm.chkBox1 = false;
        vm.chkBox2 = false;
        vm.chkBox3 = true;
        vm.previousStateFun = previousStateFun;
        vm.previousState = previousState;
        vm.userFriends = [];
        function previousStateFun() {
            $state.go(vm.previousState.name, { id: vm.previousState.params.id })
        }
        loadAll();
        //Remove duplicates from any array
        function removeDuplicates(originalArray, prop) {
            var newArray = [];
            var lookupObject = {};

            for (var i in originalArray) {
                lookupObject[originalArray[i][prop]] = originalArray[i];
            }

            for (i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
            return newArray;
        }
        function loadAll() {
            Principal.identity().then(function (u) {
                vm.identity = u;
                //GetFriendsByUserId.query({ userId: vm.identity.id }, function (result) {
                    UserNetworkStatus.query({ userId: vm.identity.id , status :'ACCEPTED'}, function (result) {
                    vm.userFriends = result;
                    vm.userPersonas = vm.userFriends;

                }).$promise;
                CollaboratorsByUser.query({ userId: vm.identity.id }, function (result) {
                    for (var i = 0, resultLen = result.length; i < resultLen; i++) {
                        if (result[i].role == 'SUPPORTER' && result[i].status == "ACCEPTED") {
                            vm.supporters.push(result[i])
                        }else{
                          if(result[i].status == "ACCEPTED"){
                              vm.collaborators.push(result[i])
                          }                            
                        }                       
                    }
                    vm.supporters = removeDuplicates(vm.supporters, "networkUserId");
                    vm.collaborators = removeDuplicates(vm.collaborators, "networkUserId");
                }).$promise;
            });
       
        }


        vm.breadcrumbsList = [
            { 'name': 'Home', 'sref': 'home' }
        ];
        function switchUsers(type) {
            if (type == 'collaborators') {
                vm.chkBox1 = true;
                vm.chkBox2 = false;
                vm.chkBox3 = false;
                vm.userPersonas = vm.collaborators;
            } else if (type == 'supporters') {
                vm.chkBox2 = true;
                vm.chkBox1 = false;
                vm.chkBox3 = false;
                vm.userPersonas = vm.supporters;
            } else {
                vm.chkBox3 = true;
                vm.chkBox2 = false;
                vm.chkBox1 = false;
                vm.userPersonas = vm.userFriends;
            }
        }
        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            UserPersonaSearch.query({ query: vm.searchQuery }, function (result) {
                vm.userPersonas = result;

            });
        }
        function inviteUsers() {
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/user-persona/invite-users.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', InviteController]
            });
        }
        function InviteController() {

        }
        function clear() {
            vm.modalInstance.close();
        }
        function addConnection() {
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/user-persona/add-connection.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', InviteController]
            });
        }

        //Get Friends List
        // function getLoginUserFriends() {
        //     // vm.isFriend = true;
        //     GetFriendsByUserId.query({ userId: vm.identity.id }, function (result) {
        //         vm.userFriends = result;
        //         vm.userPersonas = vm.userFriends;
        //     }).$promise;
        // }

    }
})();
