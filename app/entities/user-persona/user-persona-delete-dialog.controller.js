(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserPersonaDeleteController',UserPersonaDeleteController);

    UserPersonaDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserPersona'];

    function UserPersonaDeleteController($uibModalInstance, entity, UserPersona) {
        var vm = this;

        vm.userPersona = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserPersona.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
