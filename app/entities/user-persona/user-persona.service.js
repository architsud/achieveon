(function () {
    'use strict';
    angular
        .module('leApp')
        .factory('UserPersona', UserPersona)
        .factory('GetFriendsByUserId', GetFriendsByUserId)
        .factory('EmailInvitation', EmailInvitation)
        .factory('testimonials', testimonials)
        .factory('GetTestimonialsGiver', GetTestimonialsGiver)
        .factory('GetTestimonialsReceiver', GetTestimonialsReceiver)
        .factory('Recommendation', Recommendation)
    UserPersona.$inject = ['$resource', 'DateUtils'];

    function UserPersona($resource, DateUtils) {
        var resourceUrl = 'api/user-personas/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.birthdate = DateUtils.convertLocalDateFromServer(data.birthdate);
                        //data.lastlogin = DateUtils.convertLocalDateFromServer(data.lastlogin);
                        if (data && data.userEducations) {
                            for (var i = 0; i < data.userEducations.length; i++) {
                                data.userEducations[i].completion = DateUtils.convertLocalDateFromServer(data.userEducations[i].completion);
                            }
                        }
                        if (data && data.userWorkPlaces) {
                            for (var i = 0; i < data.userWorkPlaces.length; i++) {
                                data.userWorkPlaces[i].fromDate = DateUtils.convertLocalDateFromServer(data.userWorkPlaces[i].fromDate);
                                data.userWorkPlaces[i].endDate = DateUtils.convertLocalDateFromServer(data.userWorkPlaces[i].endDate);
                            }
                        }
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    // console.log(JSON.stringify(copy))
                    copy.birthdate = DateUtils.convertLocalDateToServer(copy.birthdate);
                    //copy.lastlogin = DateUtils.convertLocalDateToServer(copy.lastlogin);
                    for (var i = 0; i < copy.userEducations.length; i++) {
                        copy.userEducations[i].completion = DateUtils.convertLocalDateToServer(copy.userEducations[i].completion);
                    }
                    for (var i = 0; i < data.userWorkPlaces.length; i++) {
                        copy.userWorkPlaces[i].fromDate = DateUtils.convertLocalDateToServer(copy.userWorkPlaces[i].fromDate);
                        copy.userWorkPlaces[i].endDate = DateUtils.convertLocalDateToServer(copy.userWorkPlaces[i].endDate);
                    }
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.birthdate = DateUtils.convertLocalDateToServer(copy.birthdate);
                    copy.lastlogin = DateUtils.convertLocalDateToServer(copy.lastlogin);
                    for (var i = 0; i < copy.userEducations.length; i++) {
                        copy.userEducations[i].completion = DateUtils.convertLocalDateToServer(copy.userEducations[i].completion);
                    }
                    for (var i = 0; i < data.userWorkPlaces.length; i++) {
                        copy.userWorkPlaces[i].fromDate = DateUtils.convertLocalDateToServer(copy.userWorkPlaces[i].fromDate);
                        copy.userWorkPlaces[i].endDate = DateUtils.convertLocalDateToServer(copy.userWorkPlaces[i].endDate);
                    }
                    return angular.toJson(copy);
                }
            }
        });
    }
    //get Friends By Id       
    GetFriendsByUserId.$inject = ['$resource', 'DateUtils'];
    function GetFriendsByUserId($resource, DateUtils) {
        var resourceUrl = '/api/user-networks-user/:userId';
        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            }
        });
    }
    //get Friends By Id       
    EmailInvitation.$inject = ['$resource', 'DateUtils'];
    function EmailInvitation($resource, DateUtils) {
        var resourceUrl = '/api/email-invitation/:userId';
        return $resource(resourceUrl, {}, {
            'query': { method: 'POST', isArray: true },
            'post': {
                method: 'POST',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            }
        });
    }
    testimonials.$inject = ['$resource', 'DateUtils'];
    function testimonials($resource, DateUtils) {
        var resourceUrl = '/api/testimonials:userId';
        return $resource(resourceUrl, {}, {
            'query': { method: 'POST', isArray: false },
            'post': {
                method: 'POST',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            }
        });
    }

    GetTestimonialsReceiver.$inject = ['$resource', 'DateUtils'];
    function GetTestimonialsReceiver($resource, DateUtils) {
        var resourceUrl = '/api/testimonials/receiver/:userId';
        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            }
        });
    }
    GetTestimonialsGiver.$inject = ['$resource', 'DateUtils'];
    function GetTestimonialsGiver($resource, DateUtils) {
        var resourceUrl = '/api/testimonials/giver/:userId';
        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            }
        });
    }
    Recommendation.$inject = ['$resource', 'DateUtils'];
    function Recommendation($resource, DateUtils) {
        var resourceUrl = '/api/testimonials/recommendation/:userId';
        return $resource(resourceUrl, {}, {
            'query': { method: 'POST', isArray: true },
            'post': {
                method: 'POST',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            }
        });
    }
})();
