(function() {
    'use strict';

    angular
        .module('leApp')
        .controller('UserPersonaDialogController', UserPersonaDialogController);

    UserPersonaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'UserPersona', 'User'];

    function UserPersonaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, UserPersona, User) {
        var vm = this;

        vm.userPersona = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userPersona.id !== null) {
                UserPersona.update(vm.userPersona, onSaveSuccess, onSaveError);
            } else {
                UserPersona.save(vm.userPersona, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('leApp:userPersonaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.birthdate = false;
        vm.datePickerOpenStatus.lastlogin = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
