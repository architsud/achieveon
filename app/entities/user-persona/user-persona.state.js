(function () {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('user-persona', {
                parent: 'entity',
                url: '/user-persona',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'UserPersonas'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/user-persona/user-personas.html',
                        controller: 'UserPersonaController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'user-persona',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('user-persona-detail', {
                parent: 'entity',
                url: '/user-persona/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'UserPersona'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/user-persona/user-persona-detail.html',
                        controller: 'UserPersonaDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'UserPersona', function ($stateParams, UserPersona) {
                        return UserPersona.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'user-persona',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('user-persona-detail.edit', {
                parent: 'user-persona-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/user-persona/user-persona-dialog.html',
                        controller: 'UserPersonaDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['UserPersona', function (UserPersona) {
                                return UserPersona.get({ id: $stateParams.id }).$promise;
                            }],
                            previousState: ["$state", function ($state) {
                                var currentStateData = {
                                    name: $state.current.name || 'user-persona',
                                    params: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('^', {}, { reload: false });
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('user-persona.new', {
                parent: 'user-persona',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/user-persona/user-persona-dialog.html',
                        controller: 'UserPersonaDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    firstname: null,
                                    lastname: null,
                                    occupation: null,
                                    experties: null,
                                    experience: null,
                                    interests: null,
                                    interestedIn: null,
                                    birthdate: null,
                                    country: null,
                                    location: null,
                                    gender: null,
                                    notebook: null,
                                    lastlogin: null,
                                    birthdayVisibility: null,
                                    goalVisibility: null,
                                    connectionVisibility: null,
                                    accountType: null,
                                    exportData: null,
                                    payment: null,
                                    cardType: null,
                                    cardNumber: null,
                                    company: null,
                                    id: null
                                };
                            },
                            previousState: ["$state", function ($state) {
                                var currentStateData = {
                                    name: $state.current.name || 'user-persona',
                                    params: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('user-persona', null, { reload: 'user-persona' });
                    }, function () {
                        $state.go('user-persona');
                    });
                }]
            })
            .state('user-persona.edit', {
                parent: 'user-persona',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/user-persona/user-persona-dialog.html',
                        controller: 'UserPersonaDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['UserPersona', function (UserPersona) {
                                return UserPersona.get({ id: $stateParams.id }).$promise;
                            }],
                            previousState: ["$state", function ($state) {
                                var currentStateData = {
                                    name: $state.current.name || 'user-persona',
                                    params: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('user-persona', null, { reload: 'user-persona' });
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('user-persona.delete', {
                parent: 'user-persona',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/user-persona/user-persona-delete-dialog.html',
                        controller: 'UserPersonaDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['UserPersona', function (UserPersona) {
                                return UserPersona.get({ id: $stateParams.id }).$promise;
                            }],
                            previousState: ["$state", function ($state) {
                                var currentStateData = {
                                    name: $state.current.name || 'user-persona',
                                    params: $state.params,
                                    url: $state.href($state.current.name, $state.params)
                                };
                                return currentStateData;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('user-persona', null, { reload: 'user-persona' });
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('user-persona-profile', {
                parent: 'entity',
                url: '/user-persona-profile',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'UserPersonas'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/user-persona/user-persona-profile.html',
                        controller: 'UserPersonaProfileController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'user-persona',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })          
            .state('add-connection-goal', {
                parent: 'entity',
                url: '/add-connection-goal',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'add collaborators'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/user-persona/add-connection-goal.html',
                        controller: 'UserPersonaController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'goal',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
    }

})();
