(function () {
  'use strict';

  angular
    .module('leApp')
    .directive('goalprogress', function () {

      var linkFunction = function (scope, element, attributes) {
        scope.goalPercent = attributes["goalPercent"];
        scope.subgoalPercent = attributes["subgoalPercent"];
        scope.size = attributes["size"];
        scope.target = attributes["target"];
        scope.init();
      };

      var controller = ['$scope', function ($scope) {
        $scope.init = function init() {

          $scope.target = $scope.target;
          $scope.goalPercent = $scope.goalPercent;
          $scope.goalPercent = parseInt($scope.goalPercent);;
          $scope.subgoalPercent = $scope.subgoalPercent;
          $scope.subgoalPercent = parseInt($scope.subgoalPercent);
          console.log("$scope.goalPercent: " + $scope.goalPercent)
          console.log("$scope.subgoalPercent: " + $scope.subgoalPercent)

          var c = document.getElementById($scope.target);
          var width = parseInt($scope.size);  //commeneted to test 12-7-17
          var outerLineWidth;
          var circleSize;
          if (width == 210) {
            outerLineWidth = 15;
            circleSize = width / 2.2;
          } else if (width == 100) {
            outerLineWidth = 5;
            circleSize = width / 2.2;
          } else {
            outerLineWidth = 15;
            circleSize = width / 2.2;
          }

          c.width = width;
          c.height = width;
          var completedColor = '';
          if ($scope.goalPercent <= 25) {
            completedColor = "#ff4852";
          }
          else if ($scope.goalPercent <= 50) {
            //completedColor = "#F19447";
            completedColor = "#F59547";
          }
          else if ($scope.goalPercent <= 75) {
            //completedColor = "#F2E375";
            completedColor = "#F3DC58"
          }
          else if ($scope.goalPercent <= 100) {
            completedColor = "#87E35A";
          }
          $scope.$watchCollection('[$scope.goalPercent]', function (newVal, oldVal) {


            var ctx = c.getContext("2d");

            createCircle(ctx, '#f5f8fa', 100, width / 2, circleSize, outerLineWidth);//Main Goal pending
            createCircle(ctx, completedColor, $scope.goalPercent, width / 2, circleSize, outerLineWidth, true);//Main Goal completed

            // createCircle(ctx, '#FFFFFF', 100, width / 2, width / 2.3, 10);//Sub Goals pending
            // createCircle(ctx, '#E7EFF1', $scope.subgoalPercent, width / 2, width / 2.3, 10);//Sub Goals completed

            writeCircleInnerText(ctx, $scope.goalPercent, $scope.subgoalPercent, width / 2);
            function convertToRadians(x) {
              return ((x / 100) * (2) - 0.5);
            }

            function writeCircleInnerText(ctx, goalPercent, subgoalPercent, boundingBox) {
              ctx.textAlign = "center";
              ctx.fillStyle = completedColor;
              if (width == 100) {
                ctx.font = "15px Arial";
                if ($scope.subgoalPercent > 0) {
                  ctx.fillText(goalPercent + "%", boundingBox, boundingBox + 10);
                } else {
                  ctx.fillText(goalPercent + "%", boundingBox, boundingBox + 10);
                }
              }
              else {
                ctx.font = "30px Arial";
                if ($scope.subgoalPercent > 0) {
                  ctx.fillText(goalPercent + "%", boundingBox, boundingBox + 15);
                } else {
                  ctx.fillText(goalPercent + "%", boundingBox, boundingBox + 15);
                }
              }

              // ctx.font = "13px Arial";
              // if ($scope.subgoalPercent > 0) {
              //   ctx.fillText("Overall Achievement", boundingBox, boundingBox + 15);
              // }
              // ctx.fillStyle = "#1AB3DB";
              // ctx.font = "13px Arial";
              // if ($scope.subgoalPercent > 0) {
              //   ctx.fillText(subgoalPercent + "% Subgoals", boundingBox, boundingBox + 40);
              //   ctx.fillText("Progress", boundingBox, boundingBox + 60);
              // }

            }

            function createCircle(ctx, color, goalPercent, boundingBox, width, outerLineWidth, status) {
              var startingRadians = -0.5 * Math.PI;
              var radians = convertToRadians(goalPercent);

              ctx.strokeStyle = color;
              // ctx.lineWidth = outerLineWidth;
              ctx.lineWidth = 10;
              ctx.beginPath();
              if (status && parseInt(goalPercent) > 0) {
                ctx.lineCap = "round";
              }

              ctx.arc(boundingBox, boundingBox, width, startingRadians, radians * Math.PI);
              ctx.stroke();
            }
          })
        }
      }]

      return {
        restrict: 'E',
        scope: { goalProgress: '@', subgoalProgress: '@', size: '@', target: '@' },
        controller: controller,
        link: linkFunction,
        templateUrl: 'app/directives/progress-directive-template.html'
      };
    })

    .directive('allgoalsprogress', function () {

      var linkFunction = function (scope, element, attributes) {
        scope.goalPercent = attributes["goalPercent"];
        scope.size = attributes["size"];
        scope.target = attributes["target"];
        scope.init();
      };

      var controller = ['$scope', function ($scope) {
        $scope.init = function init() {

          $scope.target = $scope.target;
          var c = document.getElementById($scope.target);
          var width = parseInt($scope.size);
          var goalPercent = parseInt($scope.goalPercent);
          c.width = width;
          c.height = width;
          var outerLineWidth;
          var circleSize;
          if (width == 180) {
            outerLineWidth = 15;
            circleSize = width / 2.2;
          } else if (width == 120) {
            outerLineWidth = 10;
            //circleSize = 8;
            circleSize = width / 2.2;
          } else {
            outerLineWidth = 15;
            circleSize = width / 2.2;
          }
          $scope.$watchCollection('[$scope.target, $scope.goalPercent]', function (value) {
            var ctx = c.getContext("2d");
            // createCircle(current circle, color, goalPercent, boundingBox,width, outerLineWidth)
            createCircle(ctx, '#f3dc5c', 100, width / 2, circleSize, outerLineWidth);//Main Goal pending

            // createCircle(ctx, '#fbde58', 40, width / 2, width / 2.2, 12);

            //  createCircle(ctx, '#ff8c3f', 60, width / 2, width / 2.2, 12);
            createCircle(ctx, '#63e762', goalPercent, width / 2, circleSize, outerLineWidth, true);//Main Goal completed

            writeCircleInnerText(ctx, goalPercent, width / 2);
          })
        }


        function convertToRadians(x) {
          return ((x / 100) * (2) - 0.5);
        }

        function writeCircleInnerText(ctx, goalPercent, boundingBox) {
          ctx.textAlign = "center";
          ctx.fillStyle = "#214658";
          var sentence = "OVERALL GOAL STATUS"
          sentence = sentence.split('\n');
          ctx.font = "15px Arial";
          // ctx.fillText(sentence, boundingBox, boundingBox );

        }

        function createCircle(ctx, color, goalPercent, boundingBox, width, outerLineWidth, status) {
          var startingRadians = -0.5 * Math.PI;
          var radians = convertToRadians(goalPercent);

          ctx.strokeStyle = color;
          ctx.lineWidth = outerLineWidth;

          ctx.beginPath();
          if (status && parseInt(goalPercent) > 0) {
            ctx.lineCap = "round";
          }
          ctx.arc(boundingBox, boundingBox, width, startingRadians, radians * Math.PI);

          ctx.stroke();
        }


      }]

      return {
        restrict: 'E',
        scope: { goalProgress: '@', size: '@', target: '@' },
        controller: controller,
        link: linkFunction,
        templateUrl: 'app/directives/progress-directive-template.html'
      };
    })

    .directive('idleGoals', function () {
      var linkFunction = function (scope, element, attributes) {
        console.log("$scope.goalPercent: ")
        scope.goalPercent = attributes["goalPercent"];
        console.log("$scope.goalPercent: " + scope.goalPercent)
        scope.size = attributes["size"];
        scope.target = attributes["target"];
        scope.init();
      };

      var controller = ['$scope', function ($scope) {
        $scope.init = function init() {

          $scope.target = $scope.target;
          var c = document.getElementById($scope.target);
          var width = parseInt($scope.size);
          var goalPercent = parseInt($scope.goalPercent);

          c.width = width;
          c.height = width;
          $scope.$watchCollection('[$scope.target, $scope.goalPercent]', function (value) {
            var ctx = c.getContext("2d");
            // createCircle(current circle, color, goalPercent, boundingBox,width, outerLineWidth)
            createCircle(ctx, '#f3dc5c', 100, width / 2, width / 2.2, 15);//Main Goal pending

            // createCircle(ctx, '#fbde58', 40, width / 2, width / 2.2, 12);

            //  createCircle(ctx, '#ff8c3f', 60, width / 2, width / 2.2, 12);
            createCircle(ctx, '#63e762', goalPercent, width / 2, width / 2.2, 15, true);//Main Goal completed

            writeCircleInnerText(ctx, goalPercent, width / 2);
          })
        }


        function convertToRadians(x) {
          return ((x / 100) * (2) - 0.5);
        }

        function writeCircleInnerText(ctx, goalPercent, boundingBox) {
          ctx.textAlign = "center";
          ctx.fillStyle = "#214658";
          var sentence = "OVERALL GOAL STATUS"
          sentence = sentence.split('\n');
          ctx.font = "15px Arial";
          //  ctx.fillText(sentence, boundingBox, boundingBox + 10);

        }

        function createCircle(ctx, color, goalPercent, boundingBox, width, outerLineWidth, status) {
          var startingRadians = -0.5 * Math.PI;
          var radians = convertToRadians(goalPercent);

          ctx.strokeStyle = color;
          ctx.lineWidth = outerLineWidth;
          ctx.beginPath();
          if (status && parseInt(goalPercent) > 0) {
            ctx.lineCap = "round";
          }

          ctx.arc(boundingBox, boundingBox, width, startingRadians, radians * Math.PI);

          ctx.stroke();
        }


      }]

      return {
        restrict: 'E',
        scope: { goalProgress: '@', size: '@', target: '@' },
        controller: controller,
        link: linkFunction,
        templateUrl: 'app/directives/progress-directive-template.html'
      };
    })

    .directive('subgoalprogress', function () {

      var linkFunction = function (scope, element, attributes) {
        scope.goalPercent = attributes["goalPercent"];
        // scope.subgoalPercent = attributes["subgoalPercent"];
        scope.size = attributes["size"];
        scope.target = attributes["target"];
        scope.color = attributes["color"];
        scope.innerTextSize = attributes["innerTextSize"];
        scope.init();

        if (scope.goalPercent <= 25) {
          scope.color = "#ff4852";
        }
        else if (scope.goalPercent <= 50) {
          scope.color = "#F59547";
        }
        else if (scope.goalPercent <= 75) {
          scope.color = "#F3DC58"
        }
        else if (scope.goalPercent <= 100) {
          scope.color = "#87E35A";
        }
      };

      var controller = ['$scope', function ($scope) {
        $scope.init = function init() {

          $scope.target = $scope.target;
          $scope.color = $scope.color;
          $scope.innerTextSize = $scope.innerTextSize
          $scope.$watchCollection('[$scope.target, $scope.color, $scope.goalPercent]', function (value) {
            var c = document.getElementById($scope.target);
            var width = parseInt($scope.size);
            var goalPercent = parseInt($scope.goalPercent);
            //var subgoalPercent = parseInt($scope.subgoalPercent);
            c.width = width;
            c.height = width;
            var ctx = c.getContext("2d");
            var outerLineWidth = 0;
            var circleSize = 0;
            if (width == 40) {
              outerLineWidth = 5;
              circleSize = 13
            } else if (width == 20) {
              outerLineWidth = 3;
              circleSize = 8;
            } else if (width == 30) {
              outerLineWidth = 4;
              circleSize = 10;
            } else if (width == 50) {
              outerLineWidth = 5;
              circleSize = 10;
            } else {
              outerLineWidth = 8;
              circleSize = width / 2.5;
            }

            createCircle(ctx, '#f5f8fa', 100, width / 2, circleSize, outerLineWidth);
            // createCircle(ctx, '#92FA5D', goalPercent, width/2, width/2.5, 12);
            createCircle(ctx, $scope.color, goalPercent, width / 2, circleSize, outerLineWidth, true);//Main Goal completed

            //writeCircleInnerText(ctx, goalPercent, width / 2);
          })
        }

        function convertToRadians(x) {
          return ((x / 100) * (2) - 0.5);
        }

        function writeCircleInnerText(ctx, goalPercent, boundingBox) {
          ctx.textAlign = "center";
          ctx.fillStyle = "#717982";
          ctx.font = "14px Arial";
          if (boundingBox == 20) {
            // ctx.fillText(boundingBox, boundingBox + 10);
          } else if (boundingBox == 15) {
            //ctx.fillText(goalPercent + "%", boundingBox, boundingBox + 6);
          } else {
            ctx.fillText(goalPercent + "%", boundingBox, boundingBox + 6);
          }
        }

        function createCircle(ctx, color, goalPercent, boundingBox, width, outerLineWidth, status) {
          var startingRadians = -0.5 * Math.PI;
          var radians = convertToRadians(goalPercent);

          ctx.strokeStyle = color;
          ctx.lineWidth = outerLineWidth;
          ctx.beginPath();
          if (status && parseInt(goalPercent) > 0) {
            ctx.lineCap = "round";
          }
          ctx.arc(boundingBox, boundingBox, width, startingRadians, radians * Math.PI);
          ctx.stroke();
        }


      }]

      return {
        restrict: 'E',
        scope: { goalProgress: '@', size: '@', target: '@' },
        controller: controller,
        link: linkFunction,
        templateUrl: 'app/directives/progress-directive-template.html'
      };
    })

    .directive('subgoalprogressinedit', function () {

      var linkFunction = function (scope, element, attributes) {
        scope.goalPercent = attributes["goalPercent"];
        scope.target = attributes["target"];
        scope.color = attributes["color"];
        scope.init();
      };

      var controller = ['$scope', function ($scope) {
        $scope.init = function init() {
          $scope.target = $scope.target; // div id
          $scope.color = $scope.color;    //line color
          $scope.goalPercent = $scope.goalPercent;
          if (!$scope.color) {
            $scope.color = "#f5f8fa"
          }
          $scope.$watchCollection('[$scope.target, $scope.color, $scope.goalPercent]', function (value) {

            var c = document.getElementById($scope.target);
            var maxWidth = c.width;
            var maxHeight = c.height;
            var perc = parseInt($scope.goalPercent) / 100;
            var context = c.getContext('2d');
            context.beginPath();
            context.lineCap = "round";
            context.rect(0, 0, maxWidth, maxHeight);
            context.fillStyle = '#f5f8fa';
            context.fill();
            // fill
            context.beginPath();
            context.lineCap = "round";
            context.fillStyle = $scope.color;
            context.rect(0, 0, maxWidth * perc, maxHeight);
            context.fill();
          });
        }
      }]

      return {
        restrict: 'E',
        scope: { goalProgress: '@', target: '@' },
        controller: controller,
        link: linkFunction,
        templateUrl: 'app/directives/progress-directive-template.html'
      };
    });

})();
