(function () {
    'use strict';

    angular
        .module('leApp')
        .directive("fileread", [function () {
            return {
                scope: {
                    fileread: "="
                },
                link: function (scope, element, attributes) {
                    element.bind("change", function (changeEvent) {
                        scope.$apply(function () {
                            scope.fileread = changeEvent.target.files[0];
                            // or all selected files:
                            // scope.fileread = changeEvent.target.files;
                        });
                    });
                }
            }
        }
        ])
        .directive('myUpload', [function () {
            return {
                restrict: 'A',
                link: function (scope, elem, attrs) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        scope.image = e.target.result;

                        scope.$apply();
                    }

                    elem.on('change', function () {
                        reader.readAsDataURL(elem[0].files[0]);
                        scope.fileName = elem[0].files[0].name;
                    });
                }
            };
        }])
        .directive('starRating', starRating)
    function starRating() {
        return {
            restrict: 'EA',
            template:
            '  <ul style="list-style-type: none;padding: 0; width: 100%;" class="fontsize">' +
            '  <li   style="display: inline" ng-repeat="star in stars"  ng-click="toggle($index)">' +
            ' <span ng-show="star.filled" class="glyphicon testimonial-staroff glyphicon-star yellow"></span>' + // or &#9733
            ' <span ng-hide="star.filled" class="glyphicon testimonial-staroff glyphicon-star glyphicon-star-empty"></span>' +
            '  </li>' +
            '</ul>',
            scope: {
                ratingValue: '=ngModel',
                max: '=?', // optional (default is 5)
                onRatingSelect: '&?',
                readonly: '=?'
            },
            link: function (scope, element, attributes) {
                if (scope.max == undefined) {
                    scope.max = 5;
                }
                function updateStars() {
                    scope.stars = [];
                    for (var i = 0; i < scope.max; i++) {
                        scope.stars.push({
                            filled: i < scope.ratingValue
                        });
                    }
                    // alert(JSON.stringify(scope.stars))
                };
                scope.toggle = function (index) {
                    if (scope.readonly == undefined || scope.readonly === false) {
                        scope.ratingValue = index + 1;
                        scope.onRatingSelect({
                            rating: index + 1
                        });
                    }
                };
                scope.$watch('ratingValue', function (oldValue, newValue) {
                    if (newValue || newValue === 0) {
                        updateStars();
                    }
                });
            }
        };
    }
})();