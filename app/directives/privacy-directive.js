(function() {
    'use strict';

    angular
        .module('leApp')
        .directive('privacy', function() {
      
        var linkFunction = function(scope, element, attributes) {
            scope.isPublic = attributes["isPublic"]=='1';
            scope.init();
          };
      
          var controller = ['$scope', function ($scope) {
            $scope.init = function init() {
              console.log('privacy = ' + $scope.isPublic);
              if ($scope.isPublic)
              {
                $scope.privacy_icon = "icon_privacy_public.png";
                console.log('privacy = ' + $scope.privacy_icon);
                $scope.parentPrivacy = 'PUBLIC';
              }
              else
              {
                $scope.privacy_icon = "icon_privacy_private.png";
                console.log('privacy = ' + $scope.privacy_icon);
                $scope.parentPrivacy = 'PRIVATE';
              }
            }
            
            $scope.togglePrivacy = function togglePrivacy(){
              console.log('privacy = ' + $scope.isPublic);
              $scope.isPublic = !$scope.isPublic;
              if ($scope.isPublic)
              {
                $scope.privacy_icon = "icon_privacy_public.png";
                console.log('privacy = ' + $scope.privacy_icon);
                $scope.parentPrivacy = 'PUBLIC';
              }
              else
              {
                $scope.privacy_icon = "icon_privacy_private.png";
                console.log('privacy = ' + $scope.privacy_icon);
                $scope.parentPrivacy = 'PRIVATE';
              }
              return $scope.privacy_icon;
            }

          }]
              
          return {
            restrict: 'E',
            replace: 'true',
            scope: { isPublic : '@', parentPrivacy : '='},
            controller: controller,
            link: linkFunction,
            templateUrl: 'app/directives/privacy-directive-template.html'
          };
        });
})();
