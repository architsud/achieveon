(function () {
  'use strict';

  angular
    .module('leApp')
    .directive('breadcrumbs', function () {

      var controller = ['$scope', function ($scope) {

        function init() {
          if ($scope.breadcrumbsList == null) {
            $scope.breadcrumbsList = [
              { 'name': 'Home', 'sref': 'home' }
            ];
          }
        }

        init();
      }]

      return {
        restrict: 'E',
        replace: 'true',
        scope: false,
        controller: controller,
        templateUrl: 'app/directives/breadcrumbs-directive-template.html'
      };
    })
})();
