(function () {
  'use strict';

  angular
    .module('leApp')
    .directive('noCacheSrc', function () {
      return {
        priority: 99,
        link: function (scope, element, attrs) {
          attrs.$observe('noCacheSrc', function (noCacheSrc) {
            noCacheSrc += '?' + (new Date()).getTime();
            attrs.$set('src', noCacheSrc);
          });
        }
      }
    })
    .directive('newpost', function () {


      var controller = ['$scope', function ($scope) {

      }]

      return {
        restrict: 'E',
        scope: {},
        controller: controller,
        templateUrl: 'app/directives/newpost-directive-template.html'
      };
    });
})();
