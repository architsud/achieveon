var AcieveOn = AcieveOn || {}

AcieveOn.elementsPosition = new function(){
    var self = this;
    this.init = function(){
        self.svgPositions();
        document.getElementsByTagName("BODY")[0].onresize = function() {
            self.svgPositions();
        };
        self.circularProgressbar();
        self.circularProgressbarOnCards();


    };
    this.circularProgressbar = function(){
        var bar = new ProgressBar.Circle('#circularProgressbar', {
            color: '#00ff77',
            trailColor: '#eee',
            strokeWidth: 4,
            trailWidth: 4,
            easing: 'bounce',
            duration: 800,
            from: { color: '#aaa', width: 4 },
            to: { color: '#00ff77', width: 4 },
            step: function(state, circle) {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);

                var value = Math.round(circle.value() * 100);
                if (value === 0) {
                    circle.setText('');
                } else {
                    circle.setText(value+" %");
                }

            }
        });
        bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
        bar.text.style.fontSize = '2rem';
        $(window).scroll(function() {
            var top_of_element = $('#circularProgressbar').offset().top;
            var bottom_of_element = $('#circularProgressbar').offset().top + $('#circularProgressbar').outerHeight();
            var bottom_of_screen = $(window).scrollTop() + $(window).height();
            var top_of_screen = $(window).scrollTop();
            if((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
                bar.animate(0.72);
            }
            else {
                bar.animate(0);
            }
        });

    };
    this.circularProgressbarOnCards = function(){
        var arrayOfCircularProgressBar = [
            {"id":"#circular-progress-bar-card","color":"#FF556C","progress":"0.52"},
            {"id":"#circular-progress-bar-card-1","color":"#00ff77","progress":"0.91"},
            {"id":"#circular-progress-bar-card-2","color":"#00ff77","progress":"0.91"},
            {"id":"#circular-progress-bar-card-3","color":"#00ff77","progress":"0.91"},
            {"id":"#circular-progress-bar-card-4","color":"#00ff77","progress":"0.92"},
            {"id":"#circular-progress-bar-card-5","color":"#00ff77","progress":"0.92"},
        ]
        for(var k=0;k<arrayOfCircularProgressBar.length ;k ++){
            var bar = new ProgressBar.Circle(arrayOfCircularProgressBar[k]["id"], {
                color: arrayOfCircularProgressBar[k]["color"],
                trailColor: '#eee',
                strokeWidth: 8,
                trailWidth: 8,
                text: {
                    autoStyleContainer: false
                },
                easing: 'bounce',
                duration: 800,
                from: { color: '#aaa', width: 8 },
                to: { color: arrayOfCircularProgressBar[k]["color"] , width: 8 },
                step: function(state, circle) {
                    circle.path.setAttribute('stroke', state.color);
                    circle.path.setAttribute('stroke-width', state.width);

                    var value = Math.round(circle.value() * 100);
                    if (value === 0) {
                        circle.setText('');
                    } else {
                        circle.setText(value+"%");
                    }

                }
            });
            bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
            bar.text.style.left = "60%"
            bar.text.style.top = "56%"
            bar.text.style.color = "#aaa6b6"
            bar.text.style.fontSize = '20px';
            bar.animate(arrayOfCircularProgressBar[k]["progress"]);
        }

    };

    this.svgPositions = function(){
        $(".background-circles").width($(".parent-container").width()).height($(".parent-container").height()).css('opacity','.8');
        $(".MLG-bg-circles").width($(".MLG-row").width()).height($(".MLG-row").height());
        $(".GI-bg-circles").width($("#GetInspiration").width()).height($("#GetInspiration").height());
        $(".BUDT-vertical-line").height($(".BUDT-col-right").height());
        $(".RTC-vertical-line").height($(".RTC-col").height());
        $(".GI-vertical-line").height($(".GI-row").height());
        $(".CI-vertical-line").height($(".CI-row-3").height());
    };




}
