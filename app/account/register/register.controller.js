(function () {
	'use strict';

	angular
		.module('leApp')
		.controller('RegisterController', RegisterController);


	RegisterController.$inject = ['$scope', '$state', '$timeout', 'Auth', '$http', '$uibModal'];

	function RegisterController($scope, $state, $timeout, Auth, $http, $uibModal) {
		var vm = this;
		vm.name = 'testing2';
		vm.profilePic = "content/images/userprofiles/default.png";
		vm.doNotMatch = null;
		vm.error = null;
		vm.errorUserExists = null;
		vm.register = register;
		vm.registerAccount = {};
		if ($state.params.email) {
			vm.registerAccount.email = $state.params.email;
		}
		if ($state.params.password) {
			vm.confirmPassword = $state.params.password;
			vm.registerAccount.password = $state.params.password;
		}
		vm.success = null;
		vm.showAvatars = false;
		var d = new Date();
		vm.registerAccount.logo = "";
		vm.selectAvatar = selectAvatar;
		vm.profileGuid = '';
		vm.waitingForUpload = false;
		vm.displayUpload = false;
		$timeout(function () { angular.element('#login').focus(); });
		vm.modalInstance = '';

		function selectAvatar() {

			vm.modalInstance = $uibModal.open({
				templateUrl: 'app/account/register/register-dialog.html'
				, size: 'md'
				, scope: $scope
				, controller: ['$scope', '$uibModal', '$uibModalInstance', RegisterDialogController]
			});
		}
		//RegisterDialogController is implemented here
		function RegisterDialogController($scope, $uibModalInstance) {
			$scope.myImage = '';
			vm.process = 0;
			$scope.myCroppedImage = ''; // in this variable you will have dataUrl of cropped area.
			$scope.$watch('myCroppedImage', function (newVal, oldVal) {
				$scope.myCroppedImage = newVal;
			})
			$scope.handleFileSelect = function () {
				var input = document.getElementById('file-chooser');
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						// bind new Image to Component
						$scope.$apply(function () {
							$scope.myImage = e.target.result;
						});
					}
					reader.readAsDataURL(input.files[0]);
				}
			}
			$scope.blockingObject = { block: true };

			function dataURItoBlob(dataURI) {
				// convert base64/URLEncoded data component to raw binary data held in a string
				var binary = atob(dataURI.split(',')[1]);
				var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

				var array = [];
				for (var i = 0; i < binary.length; i++) {
					array.push(binary.charCodeAt(i));
				}
				return new Blob([new Uint8Array(array)], { type: mimeString });
			}
			$scope.blockingObject.callback = function (dataURL) {

				$scope.myCroppedImage = dataURL;
			}
			vm.clear = clear;
			vm.upload = upload;
			vm.chooseAvatar = chooseAvatar;
			vm.profilepic1 = "content/images/avatars/male3-128.png";

			$scope.uploadme = {};
			$scope.uploadme.src = "";

			vm.cancel = function () { $uibModalInstance.dismiss('cancel'); }

			function clear() {
				vm.modalInstance.close();
			}

			function save() {
				vm.isSaving = true;
			}

			function onSaveSuccess(result) {
				$uibModalInstance.close(result);
				vm.isSaving = false;
			}

			function onSaveError() {
				vm.isSaving = false;
			}

			function chooseAvatar(avatarName) {
				//	vm.registerAccount.logo = "avatars/" + avatarName;
				//	vm.profilePic = "http://s3-us-west-1.amazonaws.com/life-edge/avatars/" + avatarName;

				vm.profileGuid = UUID.generate();
				var keySource = "avatars/" + avatarName;
				vm.profilePic = "http://s3-us-west-1.amazonaws.com/life-edge/" + keySource;
				vm.clear();

				var destination = 'user/' + vm.profileGuid + "/profilepic.jpg";
				//var destination = "user/" + vm.identity.id + "/profilepic.jpg";
				var apiUrl = "/s3/copy?source=" + keySource + "&destination=" + destination;
				$http.get(apiUrl)
					.then(function successCallback(response) {
						vm.profilePic = "https://s3-us-west-1.amazonaws.com/life-edge/" + destination + "?" + (new Date()).getMilliseconds();
						vm.clear();
					}, function errorCallback(response) {
						//alert(response);
						vm.clear();
					});

				/*var keySource = "avatars/dimensions/" + avatarName;
				
				var apiUrl = "/s3/copy?source=" + keySource + "&destination=" + logoKey;
				$http.get(apiUrl)
				.then(function successCallback(response) {
					vm.logoPath = "https://s3-us-west-1.amazonaws.com/life-edge/dimensions/" + vm.dimension.imageGuid + ".jpg?" + (new Date()).toDateString();
					vm.clear();
				}, function errorCallback(response) {
					alert(response);
					vm.clear();
			    });*/
			}

			function upload1() {
				//var files = angular.element('#fileElem');
				if ($scope.uploadme.src != "") {
					vm.waitingForUpload = true;
					vm.registerAccount.logo = d.getTime() + ".jpg";
					var s3Prefix = "https://s3-us-west-1.amazonaws.com/life-edge/temp/" + vm.registerAccount.logo;
					var apiUrl = "/s3/geturl?key=temp/" + vm.registerAccount.logo;
					$http.get(apiUrl)
						.then(function (response) {
							var url = response.data;
							console.debug(url);
							$http.put(url, $scope.uploadme.src, {
								withCredentials: false,
								headers: {
									'Content-Type': 'binary/octet-stream'
								},
								transformRequest: angular.identity
							})
								.success(function (data) {
									vm.waitingForUpload = false;
									vm.Upload = false;
									console.log(s3Prefix);
									vm.profilePic = s3Prefix;
									vm.clear();
								})
								.error(function (data) {
									vm.waitingForUpload = false;
									vm.Upload = false;
									console.log(data);
									vm.clear();
								});
						});
				}
			}

			function upload() {
				vm.profileGuid = UUID.generate();
				// var fileChooser = document.getElementById('file-chooser');
				// var file = fileChooser.files[0];
				if ($scope.myCroppedImage == '') {
					$scope.blockingObject.render(function (dataURL) {
						console.log('via render');
						console.log(dataURL.length);
						$scope.myCroppedImage = dataURL;
					});
				}
				var blob = dataURItoBlob($scope.myCroppedImage)
				var file = new File([blob], "profilepic.jpg");
				if (file) {
					vm.waitingForUpload = true;
					var destination = '';
					//	vm.registerAccount.logo = d.getTime() + ".jpg";
					var destination = 'user/' + vm.profileGuid + "/profilepic.jpg";
					$scope.s3Prefix = "https://s3-us-west-1.amazonaws.com/life-edge/" + destination;
					var apiUrl = "/s3/geturl?key=" + destination;
					//var s3Prefix = "https://s3-us-west-1.amazonaws.com/life-edge/temp/" + vm.registerAccount.logo;
					//var apiUrl = "/s3/geturl?key=temp/" + vm.registerAccount.logo;
					console.log(apiUrl);
					$http.get(apiUrl)
						.then(function (response) {
							var url = response.data;
							console.debug(url);
							$http.put(url, file, {
								withCredentials: false,
								headers: {
									'Content-Type': 'binary/octet-stream'
								},
								eventHandlers: {
									progress: function (c) {
										console.log('Progress -> ' + c);
										console.log(c);
									}
								},
								uploadEventHandlers: {
									progress: function (e) {
										vm.process = parseInt((e.loaded * 100) / e.total);
									}
								},
								transformRequest: angular.identity
							})
								.success(function (data) {
									vm.waitingForUpload = false;
									vm.Upload = false;
									console.log($scope.s3Prefix);
									vm.profilePic = $scope.s3Prefix;
									vm.clear();
								})
								.error(function (data) {
									vm.waitingForUpload = false;
									vm.Upload = false;
									console.log(data);
									vm.clear();
								});
						});
				}


			}
		}

		function register() {
			if (vm.registerAccount.password !== vm.confirmPassword) {
				vm.doNotMatch = 'ERROR';
			} else {
				vm.registerAccount.langKey = 'en';
				vm.doNotMatch = null;
				vm.error = null;
				vm.errorUserExists = null;
				vm.errorEmailExists = null;
				vm.registerAccount.confirmPassword = vm.registerAccount.password;
				vm.registerAccount.login = vm.registerAccount.email;
				vm.registerAccount.profileGuid = vm.profileGuid;
				Auth.createAccount(vm.registerAccount).then(function () {
					vm.success = 'OK';
					$state.go("register-success")
				}).catch(function (response) {
					vm.success = null;
					if (response.status === 400 && response.data === 'login already in use') {
						vm.errorUserExists = 'ERROR';
					} else if (response.status === 400 && response.data === 'e-mail address already in use') {
						vm.errorEmailExists = 'ERROR';
					} else {
						vm.error = 'ERROR';
					}
				});
			}
		}
	}
})();
