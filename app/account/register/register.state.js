(function() {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('register', {
            parent: 'account',
            url: '/register',
            params:{email:null,password:null},
            data: {
                authorities: [],
                pageTitle: 'Registration'
            },
            views: {
                'navbar@': {
                    templateUrl: 'app/layouts/navbar/navbar.html',
                    controller: 'NavbarController',
                    controllerAs: 'vm'
                },
                'content@': {
                    templateUrl: 'app/account/register/register.html',
                    controller: 'RegisterController',
                    controllerAs: 'vm'
                }
            }
        });
          $stateProvider.state('register-success', {
            parent: 'account',
            //  url: '/register-success',
            url: '/home',
            data: {
                authorities: [],
                pageTitle: 'Registration Success'
            },
            views: {

                'content@': {
                    templateUrl: 'app/account/register/registerSuccess.html',

                }
            }
        });
    }
})();
