(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('AccountDeleteController', AccountDeleteController);

    AccountDeleteController.$inject = ['$uibModalInstance'];

    function AccountDeleteController($uibModalInstance) {
        var vm = this;    
        vm.clear = clear;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
