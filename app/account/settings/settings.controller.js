(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('SettingsController', SettingsController);

    SettingsController.$inject = ['Principal', 'Auth', '$stateParams', '$rootScope', '$scope', '$state', 'UserPersona', 'UserPersonaSearch', '$uibModal', '$http', '$timeout'];

    function SettingsController(Principal, Auth, $stateParams, $rootScope, $scope, $state, UserPersona, UserPersonaSearch, $uibModal, $http, $timeout) {
        var vm = this;
        vm.reload = reload;
        vm.profileGuid = '';
        Principal.identity().then(function (u) {
            vm.identity = u;

            if (u != null) {
                UserPersona.get({ id: vm.identity.id }).$promise.then(function (v) {

                    vm.userPersonas = v;
                    if (vm.userPersonas.profileGuid == null || vm.userPersonas.profileGuid == undefined || vm.userPersonas.profileGuid == '') {
                        vm.profileGuid = UUID.generate();
                        // alert(vm.profileGuid)
                        vm.profilepic = "/content/images/dimension-new-edit/Photo.png"
                        //alert(vm.profileGuid)
                    }
                    else {
                        vm.profilepic = "https://s3-us-west-1.amazonaws.com/life-edge/user/" + vm.userPersonas.profileGuid + "/profilepic.jpg";
                        //vm.profilepic = "https://s3-us-west-1.amazonaws.com/life-edge/user/" + vm.identity.id + "/profilepic.jpg";
                        // alert(vm.profilepic)
                    }


                    if (vm.userPersonas.userEducations.length <= 0) {
                        vm.userPersonas.userEducations = [
                            {
                                institution: 'NONE',
                                name: '',
                                degree: 'NONE',
                                completion: ''
                            }
                        ];
                    }
                    if (vm.userPersonas.userWorkPlaces.length <= 0) {
                        vm.userPersonas.userWorkPlaces = [
                            {
                                title: '',
                                companyName: '',
                                role: '',
                                fromDate: '',
                                endDate: ''
                            }
                        ];
                    }
                    //  alert(JSON.stringify(v))
                });
            }
        });
        vm.error = null;
        vm.save = save;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.updateSettings = updateSettings;
        vm.settingsAccount = null;
        vm.success = null;

        vm.addEducation = addEducation;
        vm.removeEducation = removeEducation;
        vm.addWorkPlaces = addWorkPlaces;
        vm.removeWorkPlace = removeWorkPlace;
        vm.clear = clear;
        vm.selectAvatar = selectAvatar;

        vm.collapseOne = true;
        vm.collapseTwo = true;
        vm.collapseThree = true;
        vm.collapseFour = true;
        vm.collapseFive = true;
        vm.collapseSix = true;
        vm.collapseSeven = true;
        vm.collapseEight = true;
        vm.collapseNine = true;
        /**
         * Store the "settings account" in a separate variable, and not in the shared "account" variable.
         */
        var copyAccount = function (account) {
            return {
                activated: account.activated,
                email: account.email,
                firstName: account.firstName,
                langKey: account.langKey,
                lastName: account.lastName,
                login: account.login
            };
        };

        function selectAvatar() {

            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/account/register/register-dialog.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', '$uibModalInstance', RegisterDialogController]
            });
        }

        function RegisterDialogController($scope, $uibModalInstance) {
            vm.process = 0;
            $scope.myImage = '';
            $scope.myCroppedImage = ''; // in this variable you will have dataUrl of cropped area.
            $scope.$watch('myCroppedImage', function (newVal, oldVal) {
                $scope.myCroppedImage = newVal;
            })
            $scope.handleFileSelect = function () {
                var input = document.getElementById('file-chooser');
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        // bind new Image to Component
                        $scope.$apply(function () {
                            $scope.myImage = e.target.result;
                        });
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $scope.blockingObject = { block: true };

            function dataURItoBlob(dataURI) {
                // convert base64/URLEncoded data component to raw binary data held in a string
                var binary = atob(dataURI.split(',')[1]);
                var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

                var array = [];
                for (var i = 0; i < binary.length; i++) {
                    array.push(binary.charCodeAt(i));
                }
                return new Blob([new Uint8Array(array)], { type: mimeString });
            }
            $scope.blockingObject.callback = function (dataURL) {

                $scope.myCroppedImage = dataURL;
            }
            vm.clear = clear;
            vm.upload = upload;
            vm.chooseAvatar = chooseAvatar;
            vm.profilepic1 = "content/images/avatars/male3-128.png";

            $scope.uploadme = {};
            $scope.uploadme.src = "";

            vm.cancel = function () { $uibModalInstance.dismiss('cancel'); }

            function clear() {
                vm.modalInstance.close();
            }

            function save() {
                vm.isSaving = true;
            }

            function onSaveSuccess(result) {
                $uibModalInstance.close(result);
                vm.isSaving = false;
            }

            function onSaveError() {
                vm.isSaving = false;
            }

            function chooseAvatar(avatarName) {
                var keySource = "avatars/" + avatarName;
                vm.profilePic = "http://s3-us-west-1.amazonaws.com/life-edge/" + keySource;
                vm.clear();
                var destination = '';
                if (vm.userPersonas.profileGuid == null || vm.userPersonas.profileGuid == undefined || vm.userPersonas.profileGuid == '') {
                    var destination = 'user/' + vm.profileGuid + "/profilepic.jpg";
                }
                else {
                    var destination = 'user/' + vm.userPersonas.profileGuid + "/profilepic.jpg";
                }
                //var destination = "user/" + vm.identity.id + "/profilepic.jpg";
                var apiUrl = "/s3/copy?source=" + keySource + "&destination=" + destination;
                $http.get(apiUrl)
                    .then(function successCallback(response) {

                        // $scope.$apply(function () {
                        vm.profilePic = "https://s3-us-west-1.amazonaws.com/life-edge/" + destination + "?" + (new Date()).getMilliseconds();
                        //  })
                        if (vm.userPersonas.profileGuid == null || vm.userPersonas.profileGuid == undefined || vm.userPersonas.profileGuid == '') {
                            vm.userPersonas.profileGuid = vm.profileGuid;

                            UserPersona.update(vm.userPersonas);
                            //alert(vm.userPersonas.profileGuid)
                        }

                        vm.clear();
                    }, function errorCallback(response) {
                        //alert(response);
                        vm.clear();
                    });
            }

            function upload() {

                // var fileChooser = document.getElementById('file-chooser');
                // var file = fileChooser.files[0];
                if ($scope.myCroppedImage == '') {
                    $scope.blockingObject.render(function (dataURL) {
                        console.log('via render');
                        console.log(dataURL.length);
                        $scope.myCroppedImage = dataURL;
                    });
                }
                var blob = dataURItoBlob($scope.myCroppedImage)
                var file = new File([blob], "profilepic.jpg");
                if (file) {
                    vm.waitingForUpload = true;
                    var destination = '';
                    if (vm.userPersonas.profileGuid == null || vm.userPersonas.profileGuid == undefined || vm.userPersonas.profileGuid == '') {
                        var destination = 'user/' + vm.profileGuid + "/profilepic.jpg";
                    }
                    else {
                        var destination = 'user/' + vm.userPersonas.profileGuid + "/profilepic.jpg";
                    }
                    // var destination = "user/" + vm.identity.id + "/profilepic.jpg";
                    var s3Prefix = "https://s3-us-west-1.amazonaws.com/life-edge/" + destination;
                    var apiUrl = "/s3/geturl?key=" + destination;
                    console.log(apiUrl);
                    $http.get(apiUrl)
                        .then(function (response) {
                            var url = response.data;
                            $http.put(url, file, {
                                withCredentials: false,
                                headers: {
                                    'Content-Type': 'binary/octet-stream'
                                },
                                eventHandlers: {
									progress: function (c) {
										console.log('Progress -> ' + c);
										console.log(c);
									}
								},
								uploadEventHandlers: {
									progress: function (e) {
										vm.process = parseInt((e.loaded * 100) / e.total);
									}
								},
                                transformRequest: angular.identity
                            })
                                .success(function (data) {
                                    vm.waitingForUpload = false;
                                    vm.Upload = false;

                                    // $scope.$apply(function () {
                                    vm.profilePic = s3Prefix + "?" + (new Date()).getMilliseconds();

                                    // })
                                    if (vm.userPersonas.profileGuid == null || vm.userPersonas.profileGuid == undefined || vm.userPersonas.profileGuid == '') {
                                        vm.userPersonas.profileGuid = vm.profileGuid;
                                        // alert(vm.userPersonas.bannerGuid)
                                        // alert(vm.userPersonas.profileGuid)
                                        UserPersona.update(vm.userPersonas);
                                    }

                                    vm.clear();
                                    $timeout(function () {
                                        $state.reload();
                                    }, 1000)
                                })
                                .error(function (data) {
                                    vm.waitingForUpload = false;
                                    vm.Upload = false;
                                    console.log(data);
                                    vm.clear();
                                });
                        });
                }


            }
        }
        $scope.dateOptions = {
            showWeeks: false,
            maxDate: new Date()
        };
        Principal.identity().then(function (account) {
            vm.settingsAccount = copyAccount(account);
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }
        vm.datePickerOpenStatus.birthdate = false;
        vm.datePickerOpenStatus.completion = false;
        vm.datePickerOpenStatus.fromDate = false;
        vm.datePickerOpenStatus.endDate = false;
        function openCalendar($event, type, dt) {
            $event.preventDefault();
            $event.stopPropagation();
            // vm.datePickerOpenStatus[dt] = true;
            if (type == 0) {
                vm.datePickerOpenStatus.birthdate = true;
            } else if (type == 1) {
                dt.opened = true;
            } else if (type == 2) {
                dt.fromDateOpened = true;
            } else {
                dt.endDateOpened = true;
            }
        }
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[1];
        function addEducation() {
            vm.userPersonas.userEducations.push({
                institution: 'NONE',
                name: '',
                degree: 'NONE',
                completion: ''
            })
        }
        function removeEducation(index) {
            vm.userPersonas.userEducations.splice(index, 1);
        }

        function addWorkPlaces() {
            vm.userPersonas.userWorkPlaces.push({
                title: '',
                companyName: '',
                role: '',
                fromDate: '',
                endDate: ''
            })
        }
        function removeWorkPlace(index) {
            vm.userPersonas.userWorkPlaces.splice(index, 1);
        }

        function updateSettings() {
            // alert(JSON.stringify(vm.userPersonas))
            UserPersona.update(vm.userPersonas, onSaveSuccess, onSaveError);
        }
        function onSaveSuccess(result) {
            console.log("Settings updated");
            $rootScope.$emit('authenticated');
            $state.go('goal');
        }

        function onSaveError() {
            console.log("Error in updating settings")
        }
        function save() {
            Auth.updateAccount(vm.settingsAccount).then(function () {
                vm.error = null;
                vm.success = 'OK';
                Principal.identity(true).then(function (account) {
                    vm.settingsAccount = copyAccount(account);
                });
            }).catch(function () {
                vm.success = null;
                vm.error = 'ERROR';
            });
        }

        function reload() {
            $state.go('settings', {}, { reload: true });
        }
    }

})();
