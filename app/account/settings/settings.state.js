(function () {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('settings', {
                parent: 'account',
                url: '/settings',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Settings'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/account/settings/settings.html',
                        controller: 'SettingsController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('account.delete', {
                parent: 'account',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/account/settings/account-delete-dialog.html',
                        controller: 'AccountDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {                           
                        }
                    })
                    .result.then(function () {
                        $state.go('settings', null, { reload: 'settings' });
                    }, function () {
                      //  $state.go('^');
                      $state.go('settings', null, { reload: 'settings' });
                    });
                }]
            })
    }
})();
