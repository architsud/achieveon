(function () {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('howItWorks', {
            parent: 'app',
            url: '/',
            data: {
                authorities: [],
                pageTitle: 'AchieveOn'
            },
            views: {
                'content@': {
                    templateUrl: 'app/home/landing-page1.html',
                    controller: 'HomeController',
                    controllerAs: 'vm'
                }
            }
        });
        $stateProvider.state('home', {
            parent: 'app',
            url: '/home',
            data: {
                authorities: [],
                pageTitle: 'home'
            },
            views: {
                'content@': {
                    templateUrl: 'app/home/home.html',
                    controller: 'HomeController',
                    controllerAs: 'vm'
                }
            }
        });
        $stateProvider.state('mobile_login', {
            parent: 'app',
            url: '/mobile_login',
            data: {
                authorities: [],
                pageTitle: 'mobile_login'
            },
            views: {
                'content@': {
                    templateUrl: 'app/home/mobile_login.html',
                    controller: 'HomeController',
                    controllerAs: 'vm'
                }
            }
        });
        $stateProvider.state('pricing', {
            parent: 'app',
            url: '/pricing',
            data: {
                authorities: [],
                pageTitle: 'pricing'
            },
            views: {
                'content@': {
                    templateUrl: 'app/home/pricing.html',
                    // controller: 'PricingController',
                    // controllerAs: 'vm'
                }
            }
        });
        $stateProvider.state('works', {
            parent: 'app',
            url: '/works',
            data: {
                authorities: [],
                pageTitle: 'how it works'
            },
            views: {
                'content@': {
                    templateUrl: 'app/home/works.html',
                    // controller: 'PricingController',
                    // controllerAs: 'vm'
                }
            }
        });
        $stateProvider.state('terms', {
            parent: 'app',
            url: '/terms',
            data: {
                authorities: [],
                pageTitle: 'terms & condition'
            },
            views: {
                'content@': {
                    templateUrl: 'app/home/terms.html',
                    // controller: 'PricingController',
                    // controllerAs: 'vm'
                }
            }
        });
        $stateProvider.state('contact', {
            parent: 'app',
            url: '/contact',
            data: {
                authorities: [],
                pageTitle: 'contact'
            },
            views: {
                'content@': {
                    templateUrl: 'app/home/contact.html',
                    controller: 'ContactController',
                    // controllerAs: 'vm'
                }
            }
        });
        $stateProvider.state('privacy', {
            parent: 'app',
            url: '/privacy',
            data: {
                authorities: [],
                pageTitle: 'privacy'
            },
            views: {
                'content@': {
                    templateUrl: 'app/home/privacy.html',
                    // controller: 'PricingController',
                    // controllerAs: 'vm'
                }
            }
        });
    }
})();
