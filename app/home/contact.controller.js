(function () {
	'use strict';

	angular
		.module('leApp')
		.controller('ContactController', ContactController);


	ContactController.$inject = ['$scope', '$state'];

	function ContactController($scope, $state) {
		var vm = this;
		
		vm.error = null;
		
		vm.register = register;
		vm.registerAccount = {};
		vm.success = false;

		function register() {		
			vm.success = true;
		}
	}
})();
