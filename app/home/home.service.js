(function () {
    'use strict';
    angular
        .module('leApp')
        .factory('notify', notify)
    notify.$inject = ['$resource', 'DateUtils'];
    function notify($resource, DateUtils) {
        var resourceUrl = '/api/register-user';
        return $resource(resourceUrl, {}, {
            'query': { method: 'POST', isArray: true },
           'send': {
                method: 'POST',
                transformRequest: function (data) {
                    console.log("dataaaa: "+JSON.stringify(data))
                    var copy = angular.copy(data);                   
                    return angular.toJson(copy);
                }
            }
        });
    }

})();
