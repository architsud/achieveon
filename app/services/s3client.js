(function () {
  'use strict';

  angular
    .module('leApp')
    .factory('s3client', ['$q', s3client]);

  function s3client($q) {

    var service = {

      docExtensions: ['doc', 'docx', 'pdf'],
      imageExtensions: ['png', 'jpg', 'jpeg', 'gif', 'tiff'],
      audioExtensions: ['mp3'],
      videoExtensions: ['mp4'],

      getFolderName: function (fileName, isLogo) {
        var extension = 'doc';//default
        var fileNameParts = fileName.split(".");
        if (fileNameParts.length > 1) {
          extension = fileNameParts[fileNameParts.length - 1];
        }
        if (isLogo) { extension = 'logo' }
        else if ($.inArray(extension, this.docExtensions) != -1) { extension = 'docs' }
        else if ($.inArray(extension, this.imageExtensions) != -1) { extension = 'images' }
        else if ($.inArray(extension, this.videoExtensions) != -1) { extension = 'videos' }
        else if ($.inArray(extension, this.audioExtensions) != -1) { extension = 'audios' }
        return extension;
      },

      getCollections: function (imageGuid) {

        var collectionsDeferred = $q.defer();

        var collections = { images: [], videos: [], audios: [], docs: [], links: [], logo: [] };
        var bucket = this.getS3bucket();
        var params = {
          Bucket: 'life-edge'
          , // required 
          Delimiter: ','
          , EncodingType: 'url'
          , MaxKeys: 10 //
          , Prefix: 'goal/' + imageGuid + '/'
        };
        bucket.listObjectsV2(params, function (err, data) {
          if (err) {
            console.log(err, err.stack); // an error occurred
            collectionsDeferred.reject(err);
          }
          else {
            data.Contents.forEach(function (bucketObject) {
              var key = bucketObject.Key;
              var keyArray = key.split("/");
              var item = { name: keyArray[keyArray.length - 1], key: key };
              console.log(keyArray[keyArray.length - 2]);
              if (keyArray[keyArray.length - 2] == 'docs') {
                collections.docs.push(item);
              }
              if (keyArray[keyArray.length - 2] == 'images') {
                collections.images.push(item);
              }
              if (keyArray[keyArray.length - 2] == 'videos') {
                collections.videos.push(item);
              }
              if (keyArray[keyArray.length - 2] == 'audios') {
                collections.audios.push(item);
              }
              if (keyArray[keyArray.length - 2] == 'logo') {
                collections.logo.push(item);
              }
            });
            collectionsDeferred.resolve(collections);
          }
        });
        return collectionsDeferred.promise;
        //return collections;
      },

      getS3bucket: function () {
        AWS.config.update({
          accessKeyId: 'AKIAI35BUKTGJ42L7KFQ',
          secretAccessKey: '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'
        });

        var bucket = new AWS.S3({ params: { Bucket: 'life-edge' } });
        return bucket;
      }
    };

    return service;
  }
})();

/*    function getPresignedUrl(key) {
      var urlPromise = $q.defer();
      $http.get('/s3/geturl?key=' + key).then(function (response) {
        urlPromise.resolve(response.data);
      }, function (error) {
        console.warn(error.statusText);
        urlPromise.reject(error.statusText);
      });
      return urlPromise.promise;
    }
    
    */