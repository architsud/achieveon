(function() {
    'use strict';

    angular
        .module('leApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
