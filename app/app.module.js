(function () {
    'use strict';

    angular
        .module('leApp', [
            'ngStorage',
            'ngResource',
            'ngCookies',
            'ngAria',
            'ngCacheBuster',
            'ngFileUpload',
            'ui.bootstrap',
            'ui.bootstrap.datetimepicker',
            'ui.router',
            'infinite-scroll',
            // jhipster-needle-angularjs-add-module JHipster will add new module here
            'angular-loading-bar',
            'ngTagsInput',
            'ngSanitize',
            'xeditable',
            'angularMoment',
            '720kb.socialshare',
            'ngMeta',
            'uiCropper'
            //'ngAnimate'
        ])

        .run(run);
    run.$inject = ['stateHandler', 'editableOptions', 'ngMeta'];

    function run(stateHandler, editableOptions, ngMeta) {
        stateHandler.initialize();
        ngMeta.init();
        editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
    }



})();

