(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['$rootScope', '$window', '$timeout', '$scope', '$state', 'Auth', 'Principal', 'ProfileService', 'UserPersona', 'Notification', 'AlertService', '$uibModal', 'PaginationUtil', 'NotificationsAll', 'ForApproveRejectConnection', 'DeleteNotification', 'EmailInvitation'];

    function NavbarController($rootScope, $window, $timeout, $scope, $state, Auth, Principal, ProfileService, UserPersona, Notification, AlertService, $uibModal, PaginationUtil, NotificationsAll, ForApproveRejectConnection, DeleteNotification, EmailInvitation) {
        var vm = this;
        vm.previousStateBeforeSearch = '';
        vm.isNavbarCollapsed = true;
        vm.recipient = "";
        vm.isAuthenticated = Principal.isAuthenticated;
        // Principal.identity().then(function (u) {
        //     vm.loginUser = u;
        //     getAllNotifications();
        // });
        $scope.openNotification = false;
        angular.element($window).on("click", function (event) {
            $scope.openInvite = false;
        });
        vm.getAuthenticatedUser = getAuthenticatedUser;
        vm.connectionRequest = connectionRequest;
        //  vm.inviteUsers = inviteUsers;
        vm.clear = clear;

        // function inviteUsers() {
        //     vm.modalInstance = $uibModal.open({
        //         templateUrl: 'app/entities/user-persona/invite-users.html'
        //         , size: 'md'
        //         , scope: $scope
        //         , controller: ['$scope', '$uibModal', InviteController]
        //     });
        // }
        // function InviteController() {
        vm.showAlert = showAlert;
        vm.Invite = Invite;
        function Invite() {
            // var recipients = vm.recipient.split(',');
            var recipients = vm.recipient;
            EmailInvitation.query({ recipients: recipients, content: vm.content, firstName: vm.persona.firstname }, onInviteSuccess, onInviteError);
        }
        function onInviteSuccess() {
            //  vm.modalInstance.close();
            var data = "Invitation sent successfully"
            vm.showAlert(data);
            vm.recipient = '';
            vm.content = '';
        }
        function onInviteError() {
            var data = "Some error occured. Please try again"
            vm.showAlert(data)
        }
        //}
        function clear() {
            vm.modalInstance.close();
        }

        vm.getpopup = getpopup;
        function getpopup() {
            if (vm.identity.isFirstLogin) {
                $scope.windowWidth = $window.innerWidth;
                if ($scope.windowWidth < 767) {
                    showmobileinfo();
                    vm.identity.isFirstLogin = false;
                }
                else {
                    showinfo();
                    vm.identity.isFirstLogin = false;
                }
            }

        }
        getAuthenticatedUser();

        function getAuthenticatedUser() {
            if (vm.isAuthenticated) {
                Principal.identity().then(function (u) {
                    vm.identity = u;
                    // console.debug("vm.identity(navbarCtrl) " + JSON.stringify(u));
                    if (u != null) {
                        UserPersona.get({ id: vm.identity.id }).$promise.then(function (v) {

                            vm.profilepic = "https://s3-us-west-1.amazonaws.com/life-edge/user/" + v.profileGuid + "/profilepic.jpg";
                            vm.persona = v;
                            getAllNotifications();

                            //console.debug("vm.persona(navbarCtrl) " + JSON.stringify(vm.persona));
                        });
                    }
                });
            }

        }

        setInterval(function () {
            getAllNotifications()
        }, 25000)
        //Get login user's notifications.
        function getAllNotifications() {
            if (vm.identity && vm.identity.id) {
                NotificationsAll.query({ id: vm.identity.id }, function (result) {
                    vm.userNotifications = result;
                    //alert(JSON.stringify(result))
                    vm.notificationLength = vm.userNotifications.length;
                    // alert(vm.notificationLength)
                }).$promise;
            }
        }
        $rootScope.$on("authenticated", function () {
            getAuthenticatedUser();
        });

        ProfileService.getProfileInfo().then(function (response) {
            vm.inProduction = response.inProduction;
            vm.swaggerEnabled = response.swaggerEnabled;
        });

        vm.login = login;
        vm.logout = logout;
        vm.toggleNavbar = toggleNavbar;
        vm.collapseNavbar = collapseNavbar;
        vm.searchPattern = searchPattern;
        vm.searchText = "";
        vm.$state = $state;
        vm.deleteNotification = deleteNotification;
        function login() {
            collapseNavbar();
            //alert('1LoginService.open();');TODO review
        }
        // for info
        vm.showinfo = showinfo;
        function showinfo() {

            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/layouts/navbar/app-info.html'
                , size: 'lg'
                , scope: $scope
                , controller: ['$scope', '$uibModal', ModalInstanceController]
            });

        }

        function ModalInstanceController($scope, $uibModalInstance) {
            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            $scope.slides = [
                {
                    image: 'content/images/slids/Slide0.png',
                    text: '',
                    id: 0
                },
                {
                    image: 'content/images/slids/Slide1.png',
                    text: '',
                    id: 1
                },
                {
                    image: 'content/images/slids/Slide2.png',
                    text: '',
                    id: 2
                },
                {
                    image: 'content/images/slids/Slide3.png',
                    text: '',
                    id: 3
                },
                {
                    image: 'content/images/slids/Slide4.png',
                    text: '',
                    id: 4
                },
                {
                    image: 'content/images/slids/Slide5.png',
                    text: '',
                    id: 5
                },
                {
                    image: 'content/images/slids/Slide6.png',
                    text: '',
                    id: 6
                },
                {
                    image: 'content/images/slids/Slide7.png',
                    text: '',
                    id: 7
                },
                {
                    image: 'content/images/slids/Slide8.png',
                    text: '',
                    id: 8
                },
                {
                    image: 'content/images/slids/Slide9.png',
                    text: '',
                    id: 9
                }
            ]



            var currIndex = 0;
            var slides = $scope.slides;

        }
        vm.showmobileinfo = showmobileinfo;
        function showmobileinfo() {
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/layouts/navbar/app-mobile-info.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', ModalInstanceControllerMobile]
            });
        }
        function clear() {
            vm.modalInstance.close();

        }
        function ModalInstanceControllerMobile($scope, $uibModalInstance) {

            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            $scope.slides = [
                {
                    image: 'content/images/slids/m-Slide1.png',
                    text: '',
                    id: 0
                },
                {
                    image: 'content/images/slids/m-Slide2.png',
                    text: '',
                    id: 1
                },
                {
                    image: 'content/images/slids/m-Slide3.png',
                    text: '',
                    id: 2
                },
                {
                    image: 'content/images/slids/m-Slide4.png',
                    text: '',
                    id: 3
                },
                {
                    image: 'content/images/slids/m-Slide5.png',
                    text: '',
                    id: 4
                },
                {
                    image: 'content/images/slids/m-Slide6.png',
                    text: '',
                    id: 5
                },
                {
                    image: 'content/images/slids/m-Slide7.png',
                    text: '',
                    id: 6
                },
                {
                    image: 'content/images/slids/m-Slide8.png',
                    text: '',
                    id: 7
                },
                {
                    image: 'content/images/slids/m-Slide9.png',
                    text: '',
                    id: 8
                },
                {
                    image: 'content/images/slids/m-Slide10.png',
                    text: '',
                    id: 9
                }
            ]

            var currIndex = 0;
            var slides = $scope.slides;

        }
        //end info
        function connectionRequest(notificationId, status) {

            ForApproveRejectConnection.query({ notificationId: notificationId, status: status }, function (result) {
                alert(result)
                // getAllNotifications()
            })
            //  getAllNotifications()
            $state.reload()
        }
        function deleteNotification(notificationId) {

            DeleteNotification.query({ id: notificationId }, function (result) {
                // alert(JSON.stringify(result))

            })
            //getAllNotifications()
            $state.reload()
        }
        function logout() {
            collapseNavbar();
            Auth.logout();
            $state.go('home');
        }
        vm.currentPage = true;
        $rootScope.$on("howItWorks", function () {
            vm.currentPage = false;
            if ($state.current.name == 'home') {
                vm.currentPage = true;
            }
        });
        function toggleNavbar() {
            vm.isNavbarCollapsed = !vm.isNavbarCollapsed;
        }

        function collapseNavbar() {
            vm.isNavbarCollapsed = true;
        }

        if (vm.isAuthenticated()) {
            if ($state.current.name == 'home') {
                $state.go('goal');
            }
        }

        function searchPattern() {
            if (vm.searchText == '') {
                if (vm.previousStateBeforeSearch != '') {
                    $state.go(vm.previousStateBeforeSearch);
                }
            }
            else {
                vm.previousStateBeforeSearch = $state.current.name;
                $state.go('search', { name: vm.searchText });
            }
        }

        vm.addConnection = addConnection;
        function addConnection() {
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/notification/notificationPopup.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModal', NotificationController]
            });
        }
        //   function NotificationController() {
        var params = {
            page: {
                value: '1',
                squash: true
            },
            sort: {
                value: 'id,asc',
                squash: true
            },
            search: null
        }
        //   var  page = PaginationUtil.parsePage(params.page)
        //   var  sort = params.sort
        //   var  predicate = PaginationUtil.parsePredicate(params.sort)
        //   var  ascending = PaginationUtil.parseAscending(params.sort)
        //   var  search = params.search
        /*Notification.query({
            page: 0,
            size: 5,
            sort: sort()
        }, onSuccess, onError);*/

        function sort() {
            var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
            if (vm.predicate !== 'id') {
                result.push('id');
            }
            return result;
        }
        function onSuccess(data, headers) {
            vm.links = ParseLinks.parse(headers('link'));
            vm.totalItems = headers('X-Total-Count');
            vm.queryCount = vm.totalItems;
            vm.notifications = data;
            vm.page = pagingParams.page;
        }
        function onError(error) {
            AlertService.error(error.data.message);
        }
        //  }
        function showAlert(message) {
            vm.message = message;
            vm.modalInstance = $uibModal.open({
                templateUrl: 'app/entities/goal/alert-modal.html'
                , size: 'md'
                , scope: $scope
                , controller: ['$scope', '$uibModalInstance', ShowAlertController]
            });
        }
        function ShowAlertController($scope, $uibModalInstance) {
            $timeout(function () {
                $uibModalInstance.close();
            }, 1300)
        }
    }
})();
