(function () {
    'use strict';

    angular
        .module('leApp')
        .config(stateConfig)
        

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('search', {
                parent: 'entity',
                url: '/search',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'search'
                },
                params: { 'name': null },
                views: {
                    'content@': {
                        templateUrl: 'app/layouts/navbar/search-goals.html',
                        controller: 'SearchController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    // name: ['$stateParams', function ($stateParams) {
                    //     return $stateParams.name;
                    // }]
                }
            })
    }
})();