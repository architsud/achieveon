(function () {
    'use strict';

    angular
        .module('leApp')
        .controller('SearchController', SearchController);

    SearchController.$inject = ['$scope', '$state', 'UserPersona', 'UserPersonaSearch', 'Principal', 'SearchGoals'];

    function SearchController($scope, $state, UserPersona, UserPersonaSearch, Principal, SearchGoals) {
        var vm = this;
        vm.today = new Date();
        // alert($state.params.name)
        vm.serchText = $state.params.name;
        vm.userPersonas = [];
        vm.serchedContext = [];
        // vm.search = search;
        vm.loadAll = loadAll;
        vm.switchUsers = switchUsers;
        vm.chkBox1 = true;
        vm.chkBox2 = false;
        vm.personData = [];
        vm.goalData = [];
        vm.allData = [];
        vm.filteredData = [];
        vm.personList = [];
        vm.goalList = [];

        loadAll();
        function loadAll() {
            //  if (!vm.serchText) {
            //     return vm.loadAll();
            // }


            SearchGoals.query({ query: vm.serchText }, function (result) {
                Principal.identity().then(function (u) {
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].id !== u.id) {
                            vm.goalList.push(result[i]);
                        }
                    }
                });
            });
            if (vm.serchText) {
                var recipients = vm.serchText.split(' ');

                UserPersonaSearch.query({ firstname: recipients[0], lastname: recipients[1] || recipients[0] }, function (result) {
                    Principal.identity().then(function (u) {
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].id !== u.id) {
                                if (result[i].profileGuid == null || result[i].profileGuid == "") {
                                    result[i].profileGuid = "undefine";
                                }
                                vm.personList.push(result[i]);
                            }
                        }
                        vm.userPersonas = vm.personList;
                    });
                });
            }
        }


        //     for (var i = 1; i < 11; i++) {
        //         personList = {
        //             "id": i,
        //             "firstname": "Asif" + i,
        //             "lastname": "Shiraz" + i,
        //             "occupation": "occupation" + i,
        //             "goals": ["Adventure", "trecking", "inventing", "ab"]
        //         };
        //         // vm.collaborators.push(personList)
        //         // res.push(personList);
        //         vm.serchedContext.push(personList);
        //     }
        //     for (var i = 11; i < 21; i++) {
        //         personList = {
        //             "id": i,
        //             "firstname": "Asif" + i,
        //             "lastname": "Shiraz" + i,
        //             "occupation": "occupation" + i,
        //             "goals": ["complete angular", "trecking", "inventing", "abc"]
        //         };
        //         // vm.collaborators.push(personList)
        //         // res.push(personList);                
        //         vm.serchedContext.push(personList);
        //     }
        // }
        // vm.breadcrumbsList = [
        //     { 'name': 'Home', 'sref': 'home' }
        // ]
        function switchUsers(type) {
            // vm.personData.push({ "id": personList.id, "firstname": personList.firstname, "lastname": personList.lastname, "occupation": personList.occupation })
            // vm.goalData.push({ "firstname": personList.firstname })
            if (type == 'people') {

                vm.chkBox1 = true;
                vm.chkBox2 = false;
                // //alert(JSON.stringify(vm.filteredContext))
                vm.userPersonas = vm.personList;
                vm.filteredData = vm.filteredContext;
                vm.serchedContext = vm.personData;

            } else if (type == 'goals') {
                vm.chkBox1 = false;
                vm.chkBox2 = true;
                vm.userPersonas = vm.goalList;
                vm.filteredData = vm.filteredContext;
                vm.serchedContext = vm.personData;

            }
            else {
                vm.chkBox1 = true;
                vm.chkBox2 = false;
                vm.userPersonas = vm.personList;
                // //alert(JSON.stringify(vm.filteredContext))
                vm.filteredData = vm.filteredContext;
                vm.serchedContext = vm.personData;

            }

        }
    }
})();
