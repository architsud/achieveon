(function () {
    'use strict';

    angular
        .module('leApp')
        .filter('dateRange', function () {
            var magicNumber = (1000 * 60 * 60 * 24);
            return function (toDate, fromDate) {
                if (toDate && fromDate) {
                    var dayDiff = Math.floor((toDate - fromDate) / magicNumber);
                    if (angular.isNumber(dayDiff)) {
                        var date = dayDiff + 1;
                        if (date < 0) {
                            date = 'Past Due';
                        } else if (date == 0) {
                            date = 'Today'
                        } else if (date == 1) {
                            date = date + ' Day left'
                        } else {
                            date = date + ' Days left'
                        }
                        return date;
                    }
                }
            };
        })
        .filter('ageFilter', function () {
            function calculateAge(birthday) {
                var ageDifMs = Date.now() - birthday.getTime();
                var ageDate = new Date(ageDifMs);
                return Math.abs(ageDate.getUTCFullYear() - 1970);
            }
            return function (birthdate) {
                return calculateAge(birthdate);
            };
        })
        .filter('goalPercentage', function () {
            return function (goal) {
                var m1 = parseInt((goal.metricAmount / goal.metricTarget) * 100) || 0;
                var percentage = isNaN(m1) ? 0 : m1;
                return percentage;
            }
        })
        .filter('convertIntoDate', function () {
            return function (date) {
                var d = date.finishTime;                
                var date = new Date(d);
                return date;
            }
        })
        .filter('applyColor', function () {
            return function (goal) {
                var m1 = parseInt((goal.metricAmount / goal.metricTarget) * 100);
                goal.goalpercent = isNaN(m1) ? 0 : m1;
                var color = "";
                if (goal.goalpercent <= 25) {
                    color = "#ff4852";

                }
                else if (goal.goalpercent <= 50) {
                    //color = "#F19447";
                    color = "#F59547";
                }
                else if (goal.goalpercent <= 75) {
                    // color = "#F2E375";
                    color = "#F3DC58"
                }
                else if (goal.goalpercent <= 100) {
                    color = "#87E35A";
                }
                return color;
            }
        })
        .filter('cut', function () {     //used to trim extra text and keep dots(...)
            return function (value, wordwise, max, tail) {
                if (!value) return '';

                max = parseInt(max, 10);
                if (!max) return value;
                if (value.length <= max) return value;

                value = value.substr(0, max);
                if (wordwise) {
                    var lastspace = value.lastIndexOf(' ');
                    if (lastspace !== -1) {
                        //Also remove . and , so its gives a cleaner result.
                        if (value.charAt(lastspace - 1) === '.' || value.charAt(lastspace - 1) === ',') {
                            lastspace = lastspace - 1;
                        }
                        value = value.substr(0, lastspace);
                    }
                }

                return value + (tail || ' …');
            };
        })
})();