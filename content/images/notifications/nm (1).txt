<li class="dropdown dropdown-notifications open"> <a href="#notifications-panel" class="dropdown-toggle"> <i data-count="2" class="glyphicon glyphicon-bell notification-icon"></i> </a>
            <div class="dropdown-container">
              <div class="dropdown-toolbar">
                <h3 class="dropdown-toolbar-title">Notifications (2)</h3>
              </div>
              <!-- /dropdown-toolbar -->
              <style>
.media-left, .media-right, .media-body {
display: table-cell;
vertical-align: middle;
}
</style>
              <ul class="dropdown-menu">
                <li class="notification">
                  <div class="media">
                    <div class="media-left">
                      <div class="media-object"> <img src="user_ic.png" class="img-circle" alt="Name" /> </div>
                    </div>
                    <div class="media-body">
                      <div class=" pull-left"> <strong class="notification-title pull-left"><a href="#">Mir Areeb Ali</a> added as friend </strong>
                        <div class="notification-meta"> <small class="timestamp">27. 11. 2015, 15:00</small> </div>
                      </div>
                      <div class="pull-right"><img src="close_ic.png"> <img src="right_ic.png"></div>
                    </div>
                  </div>
                </li>
                <li class="notification active">
                  <div class="media">
                    <div class="media-left">
                      <div class="media-object"> <img src="user_ic.png" class="img-circle" alt="Name" /> </div>
                    </div>
                    <div class="media-body">
                      <div class=" pull-left"> <strong class="notification-title pull-left"><a href="#">Ifrah Zaidi M.</a> assigned a sub goal </strong><br>
                        <span>23 Days Trekking & Caming - II</span>
                        <div class="notification-meta"> <small class="timestamp">27. 11. 2015, 15:00</small> </div>
                      </div>
                      <div class="pull-right"><img src="close_ic.png"> <img src="right_ic.png"></div>
                    </div>
                  </div>
                </li>
                <li class="notification">
                  <div class="media">
                    <div class="media-left">
                      <div class="media-object"> <img src="user_ic.png" class="img-circle" alt="Name" /> </div>
                    </div>
                    <div class="media-body">
                      <div class=" pull-left"> <strong class="notification-title pull-left"><a href="#">Mir Areeb Ali</a> added as friend </strong>
                        <div class="notification-meta"> <small class="timestamp">27. 11. 2015, 15:00</small> </div>
                      </div>
                      <div class="pull-right"><img src="close_ic.png"> <img src="right_ic.png"></div>
                    </div>
                  </div>
                </li>
              </ul>
              <div class="dropdown-footer text-center"> <a href="#">View All</a> </div>
              <!-- /dropdown-footer --> 
              
            </div>
            <!-- /dropdown-container --> 
          </li>